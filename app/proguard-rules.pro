# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Retrofit 2
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

-keep class com.ub.alibi.di.services.api.models.requests.** { *; }
-keep class com.ub.alibi.di.services.api.models.responses.** { *; }

# Okhhtp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontnote okhttp3.**

# Okio
-dontwarn okio.**

# dagger 2
-dontobfuscate
-dontoptimize
-optimizations !code/allocation/variable

# Log4j
-dontwarn org.apache.log4j.**
-dontnote org.apache.log4j.**

-dontwarn java.beans.PropertyDescriptor
-dontnote java.beans.PropertyDescriptor

# SIP
-dontwarn android.gov.nist.javax.sip.stack.sctp.**