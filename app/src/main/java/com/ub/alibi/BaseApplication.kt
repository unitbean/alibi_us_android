package com.ub.alibi

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics

import com.facebook.stetho.Stetho
import com.ub.alibi.di.components.AppComponent
import com.ub.alibi.di.components.DaggerAppComponent
import com.ub.alibi.di.modules.ContextModule
import com.ub.alibi.di.modules.DatabaseModule
import io.fabric.sdk.android.Fabric
import com.flurry.android.FlurryAgent
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.ub.alibi.di.services.api.ApiService.Companion.DEVICE_TOKEN
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Created by pozharov on 28.02.2018.
 */

class BaseApplication : MultiDexApplication() {

    companion object {

        @JvmStatic
        lateinit var appComponent: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(applicationContext)
        GlobalScope.launch(Dispatchers.IO) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                DEVICE_TOKEN = it.token
            }
        }

        Stetho.initializeWithDefaults(applicationContext)

        initDI(applicationContext)

        Fabric.with(this, Crashlytics())

        FlurryAgent.Builder()
            .withLogEnabled(true)
            .build(this, getString(R.string.flurry_key))

        AdvertisingIdClient.Info("UA-122474887-1", false) //Рекламный идентификатор приложения
    }

    private fun initDI(context: Context) {
        appComponent = DaggerAppComponent
            .builder()
            .contextModule(ContextModule(context))
            .databaseModule(DatabaseModule())
            .build()
    }
}
