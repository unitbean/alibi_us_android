package com.ub.alibi.di.modules

import com.ub.alibi.di.services.api.ApiService

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

/**
 * Created by pozharov on 01.03.2018.
 */

@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideApi(): ApiService {
        return ApiService()
    }
}
