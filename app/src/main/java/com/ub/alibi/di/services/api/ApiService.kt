package com.ub.alibi.di.services.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.ub.alibi.BuildConfig
import com.ub.alibi.R
import com.ub.alibi.di.services.api.interceptors.ApiInterceptor
import com.ub.alibi.di.services.api.models.requests.AuthInitRequest
import com.ub.alibi.di.services.api.models.requests.ChangeAlibiModeRequest
import com.ub.alibi.di.services.api.models.requests.LoginRequest
import com.ub.alibi.di.services.api.models.requests.RemoveMelodiesRequest
import com.ub.alibi.di.services.api.models.requests.SaveMelodiesRequest
import com.ub.alibi.di.services.api.models.requests.TransactionRequest
import com.ub.alibi.di.services.api.models.requests.VerifyCodeRequest
import com.ub.alibi.di.services.api.models.responses.BaseResponse
import com.ub.alibi.di.services.api.models.responses.LoginResponse
import com.ub.alibi.di.services.api.models.responses.MelodyResponse
import com.ub.alibi.di.services.api.models.responses.TariffResponse
import com.ub.alibi.di.services.api.models.responses.UpdateTokenResponse
import com.ub.alibi.di.services.api.models.responses.UserInfoResponse
import com.ub.alibi.di.services.api.models.responses.UssdResponse
import com.ub.alibi.di.services.api.models.responses.VerifyCodeResponse
import com.ub.alibi.di.services.api.models.responses.VoipResponse
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.PreferenceKeys.Companion.KEY_DEVICE_TOKEN
import com.ub.alibi.utils.PreferenceUtils
import java.util.concurrent.TimeUnit

import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by pozharov on 01.03.2018.
 */

class ApiService {

    private val stethoInterceptor = StethoInterceptor()

    val api: Api

    init {

        val httpClient = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(ApiInterceptor())
            .addInterceptor(HttpLoggingInterceptor().setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE))
            .addNetworkInterceptor(stethoInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER)
            .client(httpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(ApiService.Api::class.java)
    }

    interface Api {

        @get:GET("$API_V1/user/me")
        val userInfo: Observable<BaseResponse<UserInfoResponse>>

        @get:GET("$API_V1/melodies")
        val melodies: Observable<BaseResponse<List<MelodyResponse>>>

        @get:GET("$API_V1/favorite-melodies")
        val favMelodies: Observable<BaseResponse<List<MelodyResponse>>>

        @get:GET("$API_V1/package/all")
        val tariffs: Observable<BaseResponse<List<TariffResponse>>>

        @get:GET("$API_V1/voip-connector")
        val voipServers: Observable<BaseResponse<List<VoipResponse>>>

        @POST("$API_V1/user/init")
        fun authInit(@Body body: AuthInitRequest): Observable<BaseResponse<String>>

        @POST("$API_V1/user/me/android-tokens")
        fun initToken(): Observable<BaseResponse<UpdateTokenResponse>>

        @POST("$API_V1/user/verify")
        fun verifyCode(@Body body: VerifyCodeRequest): Observable<BaseResponse<VerifyCodeResponse>>

        @POST("$API_V1/user/login")
        fun login(@Body body: LoginRequest): Observable<BaseResponse<LoginResponse>>

        @POST("$API_V1/user/me/change-alibi-mode")
        fun changeAlibiMode(@Body body: ChangeAlibiModeRequest): Observable<BaseResponse<Any>>

        @GET("$API_V1/ussd")
        fun getUssdSettings(@Field("phone") phone: String): Observable<BaseResponse<UssdResponse>>

        @GET("$API_V1/melodies/{id}")
        fun getMelodyById(@Path("id") id: String): Observable<BaseResponse<MelodyResponse>>

        @POST("$API_V1/melodies/{id}")
        fun setCurrentMelody(@Path("id") id: String): Observable<BaseResponse<MelodyResponse>>

        @POST("$API_V1/favorite-melodies/remove")
        fun removeFavMelodies(@Body ids: RemoveMelodiesRequest): Observable<BaseResponse<List<MelodyResponse>>>

        @PATCH("$API_V1/favorite-melodies")
        fun saveMelodies(@Body ids: SaveMelodiesRequest): Observable<BaseResponse<List<MelodyResponse>>>

        @POST("$API_V1/transaction")
        fun createTransaction(@Body body: TransactionRequest): Observable<BaseResponse<String>>
    }

    companion object {

        var DEVICE_TOKEN: String?
            get() = PreferenceUtils.pref.getString(KEY_DEVICE_TOKEN, "")
            set(value) = PreferenceUtils.edit().putString(KEY_DEVICE_TOKEN, value).apply()

        const val API_V1 = "/api/v1"

        private const val FILES_URL = "${BuildConfig.SERVER}/files/"
        private const val PICS_URL = "${BuildConfig.SERVER}/pics/"

        const val MOCK_AVATAR = R.drawable.ic_vector_user

        fun retryAfterRefreshToken(api: ApiService, errors: Observable<out Throwable>): Observable<*> {
            return errors.flatMap { error ->
                if (error is HttpException && error.code() == 401) {
                    val refreshToken = PreferenceUtils.pref.getString(PreferenceKeys.KEY_SETTINGS_USER_REFRESH_TOKEN, "")

                    return@flatMap api
                        .api
                        .login(LoginRequest(refreshToken))
                        .flatMap({ loginResponse ->
                            val accessToken = loginResponse.result.accessToken
                            PreferenceUtils
                                .edit()
                                .putString(PreferenceKeys.KEY_SETTINGS_USER_ACCESS_TOKEN, accessToken.token)
                                .putString(PreferenceKeys.KEY_SETTINGS_USER_ACCESS_TOKEN_EXPIRED_IN, accessToken.expiredAt)
                                .putString(PreferenceKeys.KEY_SETTINGS_USER_ACCESS_TOKEN_TIMEZONE, accessToken.timeZone)
                                .apply()
                            Observable.just(1)
                        })
                }

                Observable.error<Throwable>(error)
            }
        }

        @JvmStatic
        fun getFileUrl(id: String): String {
            return "$FILES_URL$id"
        }

        @JvmStatic
        fun getPictureUrl(id: String): String {
            return "$PICS_URL$id"
        }
    }
}
