package com.ub.alibi.di.services.api.models.responses;

/**
 * Created by pozharov on 05.03.2018.
 */

public class MelodyResponse {

    private String id;
    private String title;
    private String description;
    private String fileId;
    private String picId;
    private boolean isCurrentMelody;
    private boolean isFavoriteMelody;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getFileId() {
        return fileId;
    }

    public String getPicId() {
        return picId;
    }

    public boolean isCurrentMelody() {
        return isCurrentMelody;
    }

    public boolean isFavoriteMelody() {
        return isFavoriteMelody;
    }
}
