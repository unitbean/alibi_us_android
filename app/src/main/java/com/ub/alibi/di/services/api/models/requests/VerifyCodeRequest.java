package com.ub.alibi.di.services.api.models.requests;

/**
 * Created by pozharov on 01.03.2018.
 */

public class VerifyCodeRequest {

    private String phone;
    private String code;

    public VerifyCodeRequest(String phone, String code){
        this.phone = phone;
        this.code = code;
    }
}
