package com.ub.alibi.di.components;

import android.content.Context;

import com.ub.alibi.di.modules.ApiModule;
import com.ub.alibi.di.modules.ContextModule;
import com.ub.alibi.di.modules.DatabaseModule;
import com.ub.alibi.di.modules.UserModule;
import com.ub.alibi.di.modules.WebRtcModule;
import com.ub.alibi.di.services.CoreServices;
import com.ub.alibi.ui.tariffs.reporitories.TariffRepository;
import com.ub.alibi.utils.webrtc.WebRtcService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pozharov on 01.03.2018.
 */

@Singleton
@Component(modules = {
        ContextModule.class,
        ApiModule.class,
        UserModule.class,
        DatabaseModule.class,
        WebRtcModule.class
})
public interface AppComponent{
    Context getContext();
    void inject(CoreServices coreServices);
    void inject(TariffRepository repository);

    void inject(WebRtcService webRtcService);
}
