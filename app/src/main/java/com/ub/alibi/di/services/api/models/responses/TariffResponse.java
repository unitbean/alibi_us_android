package com.ub.alibi.di.services.api.models.responses;

/**
 * Created by pozharov on 07.03.2018.
 */

public class TariffResponse {

    private String id;
    private String amount;
    private long millisec;
    private String title;
    private String description;
    private String picId;
    private String androidId;

    public String getId() {
        return id;
    }

    public String getAmount() {
        return amount;
    }

    public long getMillisec() {
        return millisec;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPicId() {
        return picId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setMillisec(long millisec) {
        this.millisec = millisec;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicId(String picId) {
        this.picId = picId;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }
}
