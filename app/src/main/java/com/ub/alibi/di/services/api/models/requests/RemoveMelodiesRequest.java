package com.ub.alibi.di.services.api.models.requests;

import java.util.HashSet;

/**
 * Created by pozharov on 06.03.2018.
 */

public class RemoveMelodiesRequest {

    private HashSet<String> ids;

    public RemoveMelodiesRequest(HashSet<String> ids){
        this.ids = ids;
    }
}
