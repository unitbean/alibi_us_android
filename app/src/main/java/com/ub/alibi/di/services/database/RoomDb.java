package com.ub.alibi.di.services.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import com.ub.alibi.di.services.database.models.CallModel;

/**
 * Created by VOLixanov on 22.03.2018.
 */

@Database(entities = {CallModel.class}, version = 1, exportSchema = false)
public abstract class RoomDb extends RoomDatabase {

    private static RoomDb INSTANCE;

    public abstract CallDao callDao();

    static RoomDb getRoomDb(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, RoomDb.class, "calls")
                .fallbackToDestructiveMigration()
                .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}