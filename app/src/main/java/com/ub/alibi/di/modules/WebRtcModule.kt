package com.ub.alibi.di.modules

import android.content.Context
import com.ub.alibi.di.services.UserService
import com.ub.alibi.di.services.database.DatabaseService
import com.ub.alibi.utils.webrtc.WebRtcService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class WebRtcModule {

    @Provides
    @Singleton
    fun provideWebRtcModule(context: Context, userService: UserService, databaseService: DatabaseService): WebRtcService {
        return WebRtcService(context, userService, databaseService)
    }

}