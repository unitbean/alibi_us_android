package com.ub.alibi.di.services.api.models.responses;

/**
 * Created by pozharov on 01.03.2018.
 */

public class TokenResponse {

    private String token;
    private String expiredAt;
    private String timeZone;

    public String getToken() {
        return token;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public String getTimeZone() {
        return timeZone;
    }
}
