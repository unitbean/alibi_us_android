package com.ub.alibi.di.services.api;

import androidx.annotation.Nullable;
import android.util.MalformedJsonException;

import com.google.gson.Gson;
import com.ub.alibi.R;
import com.ub.alibi.di.services.api.models.responses.ErrorResponse;
import com.ub.alibi.utils.LogUtils;
import com.ub.alibi.utils.Utils;

import java.io.IOException;
import java.net.NoRouteToHostException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.functions.Consumer;
import retrofit2.HttpException;

/**
 * Created by VOLixanov on 22.03.2018.
 */

public abstract class ApiException implements Consumer<Throwable> {

    private static final String TAG = ApiException.class.getSimpleName();

    private static final String PHONE_KEY = "phone";
    private static final String CODE_KEY = "code";
    private static final String REFRESH_KEY = "refreshToken";
    private static final String INVALID = "error.invalid";
    private static final String NOT_FOUND = "error.notFound";
    private static final String USER_BLOCKED = "error.user.blocked";
    private static final String USER_EXISTS = "error.userExists";
    private static final String USER_NOT_EXISTS = "error.userNotExists";
    private static final String SMS_TIMEOUT = "error.sms.timeout";
    private static final String LIMIT_REACHED = "error.limitReached";
    private static final String TOKEN_EXPIRED = "error.token.expired";
    private static final String REQUEST_ERROR = "error.request.error";
    private static final String REQUEST_MISSING_PARAM = "request.missingRequestParam";

    private String mTag;

    public ApiException(String tag) {
        this.mTag = tag;
    }

    @Override
    public void accept(Throwable throwable) {
        String message = buildMessage(throwable);

        if (message != null) {
            call(message);
        }

        LogUtils.e(mTag, throwable.getMessage(), throwable);
    }

    @Nullable
    private String buildMessage(Throwable throwable) {
        String message;
        if (throwable instanceof HttpException) {
            if (((HttpException) throwable).response().code() == 404
                ||((HttpException) throwable).response().code() == 500
                ||((HttpException) throwable).response().code() == 502) {
                message = Utils.getString(R.string.error_http_exception, ((HttpException) throwable).response().code());
            } else if (((HttpException) throwable).response().errorBody() != null
                && (((HttpException) throwable).response().code() == 400
                || ((HttpException) throwable).response().code() == 401)) {
                try {
                    ErrorResponse errorResponse = new Gson().fromJson(((HttpException) throwable).response().errorBody().string(), ErrorResponse.class);

                    if (errorResponse.getErrors().size() > 0) {
                        StringBuilder messageBuilder = new StringBuilder();

                        boolean isCode = false;

                        for (String keyError : errorResponse.getErrors().keySet()) {
                            for (String error : errorResponse.getErrors().get(keyError)) {
                                if (messageBuilder.length() == 0) {
                                    if (TOKEN_EXPIRED.equals(error) && REFRESH_KEY.equals(keyError)) {
                                        unauthorized();
                                        return null;
                                    } else if (INVALID.equals(error) && PHONE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.activity_login_phone_error_invalid));
                                    } else if (USER_BLOCKED.equals(error) && PHONE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.error_phone_user_blocked));
                                    } else if (USER_EXISTS.equals(error) && PHONE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.error_phone_user_exists));
                                    } else if (USER_NOT_EXISTS.equals(error) && PHONE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.error_phone_user_not_exists));
                                    } else if (INVALID.equals(error) && CODE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.error_code_invalid));
                                    } else if (REQUEST_ERROR.equals(error) && CODE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.error_request_error));
                                    } else if (SMS_TIMEOUT.equals(error) && CODE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.error_code_timed_out));
                                    } else if (NOT_FOUND.equals(error) && PHONE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.activity_login_phone_error_not_found));
                                    } else if (LIMIT_REACHED.equals(error) && PHONE_KEY.equals(keyError)) {
                                        messageBuilder.append(Utils.getString(R.string.activity_auth_limit_reached));
                                    } else {
                                        messageBuilder.append(keyError)
                                            .append(" ")
                                            .append(error);

                                        isCode = true;
                                    }
                                } else {
                                    messageBuilder
                                        .append(keyError)
                                        .append(" ")
                                        .append(error)
                                        .append("\n");

                                    isCode = true;
                                }
                            }
                        }

                        /*if (isCode) {
                            message = PreferenceUtils.getString(R.string.error_message_default_api, messageBuilder.toString());
                        } else {*/
                        message = messageBuilder.toString();
                        //}
                    } else if (((HttpException) throwable).response().code() == 401) {
                        message = Utils.getString(R.string.error_unauthorized_token);
                    } else {
                        message = errorResponse.getMessage();
                    }
                } catch (IOException e) {
                    message = Utils.getString(R.string.error_http_exception, ((HttpException) throwable).response().code());

                    LogUtils.e(TAG, e.getMessage(), e);
                }
            } else if (((HttpException) throwable).response().code() == 500
                || ((HttpException) throwable).response().code() == 502) {
                message = Utils.getString(R.string.error_no_route_to_host);
            } else {
                message = Utils.getString(R.string.error_http_exception, String.valueOf(((HttpException) throwable).response().code()));
            }
        } else if (throwable instanceof SocketTimeoutException) {
            message = Utils.getString(R.string.error_socket_timeout);
        } else if (throwable instanceof NoRouteToHostException) {
            message = Utils.getString(R.string.error_no_route_to_host);
        } else if (throwable instanceof MalformedJsonException) {
            message = Utils.getString(R.string.error_malformed_json);
        } else if (throwable instanceof UnknownHostException) {
            message = Utils.getString(R.string.error_unknown_host);
        } else {
            message = Utils.getString(R.string.activity_login_error);
        }

        return message;
    }

    public abstract void call(String message);
    public abstract void unauthorized();
}
