package com.ub.alibi.di.services.api.models.requests;

import java.util.Set;

/**
 * Created by pozharov on 15.03.2018.
 */

public class SaveMelodiesRequest {

    private Set<String> ids;

    public SaveMelodiesRequest(Set<String> ids){
        this.ids = ids;
    }
}
