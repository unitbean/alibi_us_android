package com.ub.alibi.di.services

import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.ui.melody.models.MelodyModel
import com.ub.alibi.utils.LogUtils

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by pozharov on 02.03.2018.
 */

class UserService(private val mApiService: ApiService) {

    private var userModel: UserModel? = null

    val userObservable: Observable<UserModel>
        get() = if (userModel != null) {
            Observable.just(userModel)
        } else {
            mApiService
                .api
                .userInfo
                .retryWhen { this.retryAfterRefreshToken(it) }
                .map { it.result }
                .map { userInfoResponse ->
                    userModel = UserModel(userInfoResponse)
                    if (userInfoResponse.currentMelodyId != null) {
                        val subscription = mApiService
                            .api
                            .getMelodyById(userInfoResponse.currentMelodyId)
                            .subscribe({ melodyResponse -> userModel?.currentMelody = MelodyModel(melodyResponse.result) }
                            ) { throwable -> LogUtils.e(TAG, throwable?.message, throwable) }
                    }
                    userModel!!
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }

    fun updateMelody(melodyModel: MelodyModel): Disposable {
        return userObservable.subscribe {
            it.currentMelody = melodyModel
        }
    }

    fun cleanUser() {
        userModel = null
    }

    /**
     * Check if exception while unauthorised then refresh token and repeat request
     * @param errors - emitted error
     * @return
     */
    private fun retryAfterRefreshToken(errors: Observable<out Throwable>): Observable<*> {
        return ApiService.retryAfterRefreshToken(mApiService, errors)
    }

    companion object {

        private const val TAG = "UserService"
    }
}
