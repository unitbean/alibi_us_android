package com.ub.alibi.di.services;

import com.ub.alibi.di.services.api.ApiService;
import com.ub.alibi.di.services.database.DatabaseService;
import com.ub.alibi.utils.webrtc.WebRtcService;

import javax.inject.Inject;

/**
 * Created by pozharov on 01.03.2018.
 */

public class CoreServices {

    @Inject ApiService apiService;
    @Inject UserService userService;
    @Inject DatabaseService database;
    @Inject WebRtcService webRtcService;

    public ApiService getApiService(){
        return this.apiService;
    }
    public UserService getUserService(){
        return this.userService;
    }
    public DatabaseService getDatabase() {
        return database;
    }

    public WebRtcService getWebRtcService() {
        return webRtcService;
    }
}