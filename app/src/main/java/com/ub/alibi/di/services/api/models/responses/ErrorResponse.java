package com.ub.alibi.di.services.api.models.responses;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pozharov on 02.03.2018.
 */

public class ErrorResponse {

    private String message;
    private Map<String, List<String>> errors = new HashMap<String, List<String>>();


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, List<String>> errors) {
        this.errors = errors;
    }
}
