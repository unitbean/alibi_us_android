package com.ub.alibi.di.services.database;

import android.content.Context;

import com.ub.alibi.di.services.database.models.CallModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by VOLixanov on 22.03.2018.
 */

public class DatabaseService {
    private RoomDb db;

    public DatabaseService(Context context) {
        this.db = RoomDb.getRoomDb(context);
    }

    public Observable<List<CallModel>> getAllCalls() {
        return Observable.defer(() -> Observable.just(db.callDao().getAll()));
    }

    public Observable<Integer> deleteAllCalls() {
        return Observable.defer(() -> Observable.just(db.callDao().deleteAll()));
    }

    public Observable<Integer> deleteCall(int position) {
        return Observable.defer(() -> Observable.create((ObservableOnSubscribe<Integer>) emitter -> {
            try {
                CallModel model = db.callDao().getAll().get(position);
                db.callDao().deleteItem(model);
            } catch (Exception e) {
                emitter.onError(e);
            }

            emitter.onNext(position);
            emitter.onComplete();
        }));
    }

    public Observable<Long[]> insertCalls(CallModel... models) {
        return Observable.defer(() -> Observable.just(db.callDao().insertAll(models)));
    }
}
