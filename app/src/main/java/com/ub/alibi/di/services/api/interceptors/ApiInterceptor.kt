package com.ub.alibi.di.services.api.interceptors

import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.utils.PreferenceUtils

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Response

import com.ub.alibi.utils.PreferenceKeys.Companion.KEY_SETTINGS_USER_ACCESS_TOKEN

/**
 * Created by pozharov on 02.03.2018.
 */

class ApiInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val method = original.method()

        val accessToken = PreferenceUtils.pref
            .getString(KEY_SETTINGS_USER_ACCESS_TOKEN, "")

        val deviceId = ApiService.DEVICE_TOKEN

        val requestBuilder = original.newBuilder()
            .method(method, original.body())

        requestBuilder.header("X-Auth-Token", accessToken)
            .addHeader("X-Device-Type", ANDROID)

        if (deviceId != null) {
            requestBuilder.addHeader("X-Device-Token", deviceId)
        }

        val request = requestBuilder.build()
        return chain.proceed(request)
    }

    companion object {

        private const val ANDROID = "ANDROID"
    }
}
