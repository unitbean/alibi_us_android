package com.ub.alibi.di.services.api.models.requests

/**
 * Created by VOLixanov on 27.03.2018.
 */
class TransactionRequest {
    var millisec: Long? = null
    var token: String? = null

    val sendFrom = "Android"

    constructor(millisec: Long?) {
        this.millisec = millisec
    }

    constructor(millisec: Long?, token: String) {
        this.millisec = millisec
        this.token = token
    }
}
