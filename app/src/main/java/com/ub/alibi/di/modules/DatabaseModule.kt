package com.ub.alibi.di.modules

import android.content.Context

import com.ub.alibi.di.services.database.DatabaseService

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

/**
 * Created by VOLixanov on 22.03.2018.
 */

@Module(includes = [(ContextModule::class)])
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabaseService(context: Context): DatabaseService {
        return DatabaseService(context)
    }
}
