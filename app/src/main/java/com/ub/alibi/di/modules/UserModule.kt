package com.ub.alibi.di.modules

import com.ub.alibi.di.services.UserService
import com.ub.alibi.di.services.api.ApiService

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

/**
 * Created by pozharov on 02.03.2018.
 */

@Module
class UserModule {

    @Provides
    @Singleton
    internal fun provideUserService(apiService: ApiService): UserService {
        return UserService(apiService)
    }
}
