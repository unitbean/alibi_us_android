package com.ub.alibi.di.services.api.models.responses;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pozharov on 02.03.2018.
 */

public class UserInfoResponse {

    private String id;
    private String currentMelodyId;
    private List<Object> favoriteMelodies = new ArrayList<>();
    private long millisec;
    private boolean alibiMode;
    private String sipLogin;
    private String sipPassword;

    public String getId() {
        return id;
    }

    public String getCurrentMelodyId() {
        return currentMelodyId;
    }

    public List<Object> getFavoriteMelodies() {
        return favoriteMelodies;
    }

    public long getMillisec() {
        return millisec;
    }

    public boolean isAlibiMode() {
        return alibiMode;
    }

    public String getSipLogin() {
        return sipLogin;
    }

    public String getSipPassword() {
        return sipPassword;
    }
}
