package com.ub.alibi.di.services.api.models.responses;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VOLixanov on 27.03.2018.
 */
public class UpdateTokenResponse {

    private List<String> tokens = new ArrayList<>();

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }
}
