package com.ub.alibi.di.services.api.models.responses;

/**
 * Created by VOLixanov on 26.03.2018.
 */

public class UssdResponse {

    private String operator;
    private String operatorId;
    private String region;
    private String regionId;
    private String ussdTurnOnAlibi;
    private String ussdTurnOffAlibi;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getUssdTurnOnAlibi() {
        return ussdTurnOnAlibi;
    }

    public void setUssdTurnOnAlibi(String ussdTurnOnAlibi) {
        this.ussdTurnOnAlibi = ussdTurnOnAlibi;
    }

    public String getUssdTurnOffAlibi() {
        return ussdTurnOffAlibi;
    }

    public void setUssdTurnOffAlibi(String ussdTurnOffAlibi) {
        this.ussdTurnOffAlibi = ussdTurnOffAlibi;
    }
}
