package com.ub.alibi.di.services.api.models.requests;

/**
 * Created by pozharov on 05.03.2018.
 */

public class ChangeAlibiModeRequest {

    private boolean alibiMode;

    public ChangeAlibiModeRequest(boolean alibiMode) {
        this.alibiMode = alibiMode;
    }
}
