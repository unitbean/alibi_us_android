package com.ub.alibi.di.services.database.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

/**
 * Created by VOLixanov on 22.03.2018.
 */

@Entity(tableName = "call")
public class CallModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "sipLogin")
    private String sipLogin;

    @ColumnInfo(name = "callTime")
    private long callTime;

    @ColumnInfo(name = "isIncoming")
    private boolean isIncoming;

    @ColumnInfo(name = "isMissed")
    private boolean isMissed;

    @Ignore
    private boolean isCallingNow = false;

    public CallModel() {
    }

    @Ignore
    public CallModel(String sipLogin, long callTime, boolean isIncoming, boolean isMissed) {
        this.sipLogin = sipLogin;
        this.callTime = callTime;
        this.isIncoming = isIncoming;
        this.isMissed = isMissed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSipLogin() {
        return sipLogin;
    }

    public void setSipLogin(String sipLogin) {
        this.sipLogin = sipLogin;
    }

    public long getCallTime() {
        return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }

    public boolean isIncoming() {
        return isIncoming;
    }

    public void setIncoming(boolean incoming) {
        isIncoming = incoming;
    }

    public boolean isMissed() {
        return isMissed;
    }

    public void setMissed(boolean missed) {
        isMissed = missed;
    }

    public boolean isCallingNow() {
        return isCallingNow;
    }

    public void setCallingNow(boolean callingNow) {
        isCallingNow = callingNow;
    }
}