package com.ub.alibi.di.services.api.models.responses;

/**
 * Created by pozharov on 01.03.2018.
 */

public class BaseResponse<T> {
    private T result;

    public T getResult(){
        return result;
    }
}
