package com.ub.alibi.di.services.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ub.alibi.di.services.database.models.CallModel;

import java.util.List;

/**
 * Created by VOLixanov on 22.03.2018.
 */

@Dao
public interface CallDao {

    @Query("SELECT * FROM call")
    List<CallModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertAll(CallModel... info);

    @Query("DELETE FROM call")
    int deleteAll();

    @Delete
    void deleteItem(CallModel... model);
}