package com.ub.alibi.di.services.api.models.requests;

/**
 * Created by pozharov on 01.03.2018.
 */

public class AuthInitRequest {

    private String phone;

    public AuthInitRequest(String phone){
        this.phone = phone;
    }
}
