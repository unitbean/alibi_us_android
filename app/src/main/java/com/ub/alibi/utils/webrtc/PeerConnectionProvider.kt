package com.ub.alibi.utils.webrtc

import android.content.Context
import android.util.Log
import com.ub.alibi.BuildConfig
import com.ub.alibi.utils.LogUtils
import com.ub.alibi.utils.webrtc.rtcutils.PeerConnectionListener
import com.ub.alibi.utils.webrtc.rtcutils.RtcEventLog
import com.ub.alibi.utils.webrtc.rtcutils.AudioDeviceUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.webrtc.*
import kotlin.coroutines.CoroutineContext

class PeerConnectionProvider(val context: Context, val peerConnectionListener: PeerConnectionListener) : RtcClient, CoroutineScope {

    companion object {
        private val TAG = "AlibiWebRtc_" + PeerConnectionProvider::class.java.simpleName

        private const val DISABLE_WEBRTC_AGC_FIELDTRIAL = "WebRTC-IntelVP8/Enabled/"
        private const val AUDIO_TRACK_ID = "ARDAMS"
    }

    override val coroutineContext: CoroutineContext = Dispatchers.IO

    private var factory: PeerConnectionFactory? = null
    private var rtcEventLog: RtcEventLog? = null
    private var audioSource: AudioSource? = null
    private var localAudioTrack: AudioTrack? = null
    private var peerConnection: PeerConnection? = null

    private var pcObserver: PeerConnectionObserver? = null

    private var sdpMediaConstraints: MediaConstraints? = null

    /**
     * Инициализация PeerConnectionFactory
     */
    init {
        if (BuildConfig.DEBUG) {
            PeerConnectionFactory.printStackTraces()
        }

        val options =
                PeerConnectionFactory.InitializationOptions.builder(context)
                        .setFieldTrials(DISABLE_WEBRTC_AGC_FIELDTRIAL)
                        .setEnableInternalTracer(true)
                        .createInitializationOptions()
        PeerConnectionFactory.initialize(options)
        createPeerConnectionFactory()
        setupMediaResources()
        createPeerConnection()

    }

    /**
     * Создает PeerConnectionFactory
     * В [PeerConnectionFactory.Options] свойство disableEncryption устанавливаем равным true, так как мы не будем использовать шифрование
     * Так же создается AudioDeviceModule при помощи метода [AudioDeviceUtils.createAudioDevice].
     */
    private fun createPeerConnectionFactory() {
        val options = PeerConnectionFactory.Options()

        options.disableEncryption = true

        val adm = AudioDeviceUtils.createAudioDevice(context)

        val audioEncoderFactory: AudioEncoderFactoryFactory
        val audioDecoderFactory: AudioDecoderFactoryFactory
        audioEncoderFactory = BuiltinAudioEncoderFactoryFactory()
        audioDecoderFactory = BuiltinAudioDecoderFactoryFactory()

        factory = PeerConnectionFactory.builder()
                .setOptions(options)
                .setAudioDeviceModule(adm)
                .setAudioEncoderFactoryFactory(audioEncoderFactory)
                .setAudioDecoderFactoryFactory(audioDecoderFactory)
                .createPeerConnectionFactory()


        adm.release()
    }

    /**
     * Создает и настраивает Медиа-данные
     * В методе создаются и настраиваются [audioSource] (Источник звука) и [localAudioTrack] (Аудио-поток)
     */
    private fun setupMediaResources() {
        val audioConstraints = MediaConstraints()
        audioSource = factory?.createAudioSource(audioConstraints)
        localAudioTrack = factory?.createAudioTrack(AUDIO_TRACK_ID, audioSource)
        localAudioTrack?.setEnabled(true)
    }

    /**
     * Создает экземпляр peerConnection.
     * Создает экземпляр [PeerConnection.RTCConfiguration], который настраивает sdp-сообщение
     * В этом же методе создается [PeerConnectionObserver], в который будут падать события создания соединения,
     * аудио-потоков, ice candidates и пр. В целом нас интересует только события [PeerConnectionObserver.onIceGatheringChange],
     * который вызвается после создания ice candidates и [PeerConnectionObserver.onAddStream], в котором мы открываем звонилку
     */
    private fun createPeerConnection() {
        val rtcConfig = PeerConnection.RTCConfiguration(arrayListOf())
        rtcConfig.sdpSemantics = PeerConnection.SdpSemantics.PLAN_B
        rtcConfig.tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.DISABLED
        rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.BALANCED
        rtcConfig.candidateNetworkPolicy = PeerConnection.CandidateNetworkPolicy.ALL
        rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE
        rtcConfig.continualGatheringPolicy = PeerConnection.ContinualGatheringPolicy.GATHER_ONCE

        sdpMediaConstraints = MediaConstraints()
        sdpMediaConstraints?.mandatory?.add(
                MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"))
        sdpMediaConstraints?.mandatory?.add(MediaConstraints.KeyValuePair(
                "OfferToReceiveVideo", "false"))

        pcObserver = PeerConnectionObserver()

        peerConnection = factory?.createPeerConnection(rtcConfig, pcObserver)

        val mediaStreamLabels = listOf(AUDIO_TRACK_ID)
        peerConnection?.addTrack(localAudioTrack, mediaStreamLabels)

        //Create log file
        rtcEventLog = RtcEventLog(context, peerConnection)
        rtcEventLog?.start()
    }

    /**
     * Пытаемся создать local sdp. События создания или ошибки будут пробрасываться в [sdpObserver]
     */
    override fun createOfferSdp() {
        peerConnection?.createOffer(sdpObserver, sdpMediaConstraints)
    }

    /**
     * Пытаемся установить полученный с веб-сокета remote sdp. События приминения или ошибки будут пробрасываться в [sdpObserver]
     */
    override fun setupAnswerSdp(sdp: String?) {
        LogUtils.w("$TAG SDP ANSWER", sdp)
        val sessionDescription = SessionDescription(SessionDescription.Type.ANSWER, sdp)
        peerConnection?.setRemoteDescription(sdpObserver, sessionDescription)
    }

    /**
     * Вызываем этот метод для закрытия соединения и завершения звонка.
     * методы [PeerConnectionFactory.stopAecDump] и [PeerConnectionFactory.dispose] нужно обязательно вызывать в самом конце,
     * в ином случае мы имеем высокий шанс заблокировать поток и получить утечки памяти, которые выльются в краш приложения.
     */
    override fun dismiss() {
        launch {
            peerConnection?.dispose()
            peerConnection = null

            localAudioTrack?.dispose()
            localAudioTrack = null

            audioSource?.dispose()
            audioSource = null

            factory?.stopAecDump()
            factory?.dispose()
            factory = null

            PeerConnectionFactory.stopInternalTracingCapture()
            PeerConnectionFactory.shutdownInternalTracer()
        }
    }

    /**
     * В [SdpObserver] пробрасываются события создания и сохранения sdp-сообщений,
     * а так же соответствующие события ошибок.
     */
    private val sdpObserver = object: SdpObserver {

        /**
         * Callback, вызванный после успешного создания local sdp.
         * В этом методе можно вручную менять/добавлять поля полученного sdp-сообщения.
         * Чтобы применить полученный local sdp к [peerConnection], нужно вызвать метод [PeerConnection.setLocalDescription],
         * в который пердается [SdpObserver] и полученное, и в последующем, возможно, модифицированное, sdp-сообщение.
         */
        override fun onCreateSuccess(origSdp: SessionDescription) {
            Log.d(TAG, "Set local SDP from " + origSdp.type)
            peerConnection?.setLocalDescription(this, origSdp)
        }

        /**
         * Callback, вызванный после успешного применения sdp-сообщения к [peerConnection]
         */
        override fun onSetSuccess() {
            if (peerConnection?.remoteDescription == null) {
                Log.d(TAG, "Local SDP set successfully")
            } else {
                Log.d(TAG, "Remote SDP set successfully")
            }
        }

        /**
         * Callback ошибки, вызванной при неудачном создании sdp (SDP имеет невалидный формат)
         */
        override fun onCreateFailure(error: String) {
            LogUtils.e(TAG, error)
            peerConnectionListener.onError()
        }

        /**
         * Callback ошибки, вызванный при применении sdp-сообщения к [peerConnection]
         */
        override fun onSetFailure(error: String) {
            LogUtils.e(TAG, error)
            peerConnectionListener.onError()
        }
    }

    /**
     * В [PeerConnectionObserver] будут падать различные события [PeerConnection].
     * Нас интересуют только событие [PeerConnectionObserver.onIceGatheringChange], в котором после создания
     * ice candidates, мы отправляем запрос verto.invite на веб-сокет с sdp-сообщением
     * Так же стоит обратить на [PeerConnectionObserver.onAddStream], в котором запускается экран звонилки
     */
    inner class PeerConnectionObserver : PeerConnection.Observer {

        override fun onIceCandidate(candidate: IceCandidate) {
            Log.d(TAG, "onIceCandidate")
        }

        override fun onIceCandidatesRemoved(candidates: Array<IceCandidate>) {
            Log.d(TAG, "onIceCandidatesRemoved")
        }

        override fun onSignalingChange(newState: PeerConnection.SignalingState) {
            Log.d(TAG, "SignalingState: $newState")
        }

        override fun onIceConnectionChange(newState: PeerConnection.IceConnectionState) {
            Log.d(TAG, "IceConnectionState: $newState")
        }

        override fun onConnectionChange(newState: PeerConnection.PeerConnectionState) {
            Log.d(TAG, "PeerConnectionState: $newState")
        }

        /**
         * Метод слушателя вызывается после создания всех ice candidates.
         * Вызывает [PeerConnectionListener.onIceCandidatesCreated] для совершение запроса "verto.invite" для передачи sdp-сообщения второму клиенту.
         * То есть по факту у себя мы оставляем sdp без ice candidates (в нашем sdp они в целом и не нужны), но отправляем
         * второму клиенту sdp с ice candidates.
         */
        override fun onIceGatheringChange(newState: PeerConnection.IceGatheringState) {
            Log.d(TAG, "IceGatheringState: $newState")
            if (newState == PeerConnection.IceGatheringState.COMPLETE) {
                val sdp = peerConnection?.localDescription?.description ?: return
                peerConnectionListener.onIceCandidatesCreated(sdp)
            }
        }

        override fun onIceConnectionReceivingChange(receiving: Boolean) {
            Log.d(TAG, "IceConnectionReceiving changed to $receiving")
        }

        /**
         * Метод вызвается после сохранения local sdp и remote sdp.
         * Вызываем [PeerConnectionListener.onConnected] для начала звонка (открытия экрана звонилки)
         */
        override fun onAddStream(stream: MediaStream) {
            Log.d(TAG, "onAddStream")
            peerConnectionListener.onConnected()
        }

        override fun onRemoveStream(stream: MediaStream) {
            Log.d(TAG, "onRemoveStream")
        }

        override fun onDataChannel(dc: DataChannel) {
            Log.d(TAG, "onDataChannel")
        }

        override fun onRenegotiationNeeded() {
            Log.d(TAG, "onRenegotiationNeeded")
        }

        override fun onAddTrack(receiver: RtpReceiver, mediaStreams: Array<MediaStream>) {
            Log.d(TAG, "onAddTrack")
        }

    }

}
