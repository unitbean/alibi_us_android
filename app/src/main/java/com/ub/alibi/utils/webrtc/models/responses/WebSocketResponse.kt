package com.ub.alibi.utils.webrtc.models.responses

class WebSocketResponse {
    var jsonrpc = "2.0"
    var id: Int? = null
    var method: String? = null
    var result: HashMap<String, String> = hashMapOf()
    var params: HashMap<String, String> = hashMapOf()
}