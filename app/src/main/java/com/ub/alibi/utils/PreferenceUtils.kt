package com.ub.alibi.utils

import android.content.Context
import android.content.SharedPreferences

import com.ub.alibi.BaseApplication

/**
 * Created by pozharov on 01.03.2018.
 */

class PreferenceUtils {

    companion object {
        /**
         * Get shared preference by key "settings"
         * @return
         */
        @JvmStatic
        val pref: SharedPreferences
            get() = BaseApplication.appComponent.context.getSharedPreferences(PreferenceKeys.KEY_SETTINGS, Context.MODE_PRIVATE)

        /**
         * Get editor for shared preference
         * @return
         */
        @JvmStatic
        fun edit(): SharedPreferences.Editor {
            return pref.edit()
        }
    }
}
