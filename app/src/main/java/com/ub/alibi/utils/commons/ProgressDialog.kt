package com.ub.alibi.utils.commons

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window

import com.ub.alibi.R
import com.ub.alibi.ui.base.activities.BaseActivity

/**
 * Created by pozharov on 01.03.2018.
 */

class ProgressDialog : androidx.fragment.app.DialogFragment() {

    private var activity: BaseActivity? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity = context as BaseActivity?
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_progress_bar, container, false)
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
        activity?.onBackPressed()
    }

    companion object {

        const val TAG = "ProgressDialog"
    }
}
