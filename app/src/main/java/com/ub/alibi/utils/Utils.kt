package com.ub.alibi.utils

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import android.view.inputmethod.InputMethodManager

import com.ub.alibi.BaseApplication

import java.net.NetworkInterface
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 * Created by pozharov on 01.03.2018.
 */

class Utils {

    companion object {
        /**
         * Get application resources
         * @return
         */
        @JvmStatic
        fun getResources() : Resources {
            return BaseApplication.appComponent.context.resources
        }

        /**
         * Converter dp to px
         * @param context - context for resources
         * @param dp - value of dp
         * @return - value of pixels
         */
        @JvmStatic
        fun convertDpToPx(context: Context, dp: Int): Float {
            val resources = context.resources
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics)
        }

        /**
         * Get string resource
         * @param resId - id string resource
         * @param args - args for formatter
         * @return
         */
        @JvmStatic
        fun getString(resId: Int, vararg args: Any): String {
            return BaseApplication.appComponent.context.getString(resId, *args)
        }

        @JvmStatic
        fun formatTelephoneNumber(phoneNumber: String): String {
            try {
                return String.format("+1 (%s) %s-%s-%s",
                    phoneNumber.substring(0, 3),
                    phoneNumber.substring(3, 6),
                    phoneNumber.substring(6, 8),
                    phoneNumber.substring(8, 10))
            } catch (e: Exception) {
                return phoneNumber
            }

        }

        @JvmStatic
        fun formatRemainingMinutes(first: Long, second: Long): String {
            val otherSymbols = DecimalFormatSymbols(Locale.getDefault())
            otherSymbols.decimalSeparator = ':'
            val pattern = "##0.00"
            val formatter = DecimalFormat(pattern, otherSymbols)
            val first = first / 60000
            val second = second % 60000 * 0.00001

            return formatter.format(first + second)
        }

        /**
         * Hide keyboard
         * @param context
         */
        @JvmStatic
        fun hideSoftKeyboard(context: Context) {
            try {
                val inputMethodManager = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow((context as Activity).currentFocus!!.windowToken, 0)
                context.currentFocus!!.clearFocus()
            } catch (e: NullPointerException) {
                LogUtils.e("KeyBoard", " NULL point exception in input method service")
            }

        }

        @JvmStatic
        fun isValidPhoneNumber(number: String): Boolean {
            return android.util.Patterns.PHONE.matcher(number).matches()
        }

        @JvmStatic
        fun getIPAddress(useIPv4: Boolean): String {
            try {
                val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
                for (intf in interfaces) {
                    val addrs = Collections.list(intf.inetAddresses)
                    for (addr in addrs) {
                        if (!addr.isLoopbackAddress) {
                            val sAddr = addr.hostAddress
                            //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                            val isIPv4 = sAddr.indexOf(':') < 0

                            if (useIPv4) {
                                if (isIPv4)
                                    return sAddr
                            } else {
                                if (!isIPv4) {
                                    val delim = sAddr.indexOf('%') // drop ip6 zone suffix
                                    return if (delim < 0) sAddr.toUpperCase() else sAddr.substring(0, delim).toUpperCase()
                                }
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
            }
            // for now eat exceptions
            return ""
        }


        /**
         * Накладывает маску на номер.
         * При изменении номера необходимо вызывать этот метод
         */
        @JvmStatic
        fun maskNumber(phoneNumber: String): String {
            if (phoneNumber.isEmpty()) return phoneNumber

            var charIndex = 0
            val musk = "+# (###) ###-##-##"
            var textString = ""

            for(char in musk) {
                if(charIndex < phoneNumber.length) {
                    if(char == '#' || char == phoneNumber.get(charIndex)) {
                        textString += phoneNumber.get(charIndex)
                        charIndex++
                    } else {
                        textString += char
                    }
                } else {
                    if(char != '#') {
                        textString += char
                    } else {
                        break
                    }
                }
            }

            if(charIndex < phoneNumber.length) {
                return phoneNumber
            }
            return textString
        }
    }
}
