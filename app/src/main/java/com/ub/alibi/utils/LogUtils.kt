package com.ub.alibi.utils

import android.util.Log
import com.crashlytics.android.Crashlytics

import com.ub.alibi.BuildConfig

/**
 * Created by pozharov on 01.03.2018.
 */

class LogUtils {

    companion object {
        private const val ERROR = "Undefined error"

        @JvmStatic
        fun e(tag: String, message: String?, error: Throwable) {
            if (BuildConfig.DEBUG) {
                Log.e(tag, message ?: ERROR, error)
            } else {
                Crashlytics.logException(error)
            }
        }

        @JvmStatic
        fun e(tag: String, message: String?) {
            if (BuildConfig.DEBUG) {
                Log.e(tag, message ?: ERROR)
            } else {
                Crashlytics.log("$tag $message")
            }
        }

        @JvmStatic
        fun d(tag: String, message: String?) {
            if (BuildConfig.DEBUG) {
                Log.d(tag, message ?: ERROR)
            }
        }

        @JvmStatic
        fun i(tag: String, message: String?) {
            if (BuildConfig.DEBUG) {
                Log.i(tag, message ?: ERROR)
            }
        }

        @JvmStatic
        fun w(tag: String, message: String?) {
            if (BuildConfig.DEBUG) {
                Log.w(tag, message ?: ERROR)
            }
        }
    }
}
