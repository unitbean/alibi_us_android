package com.ub.alibi.utils.webrtc.models.requests

class SdpOfferWebSocketRequest(sdpOfferParams: SdpOfferParams): WebRtcModel(METHOD_INVITE, sdpOfferParams)

class SdpOfferParams(var sdp: String, var dialogParams: DialogParams, var sessid: String?) : WebRtcParams() {
}

class DialogParams(login: String, callerId: String, dialerNumber: String, callId: String) {
    var screenShare = false
    var useMic = "any"
    var useSpeak = "any"
    var tag = callId
    var localTag = null
    var login = login
    var videoParams = VideoParams()
    var destination_number = dialerNumber
    var caller_id_name = callerId
    var caller_id_number = callerId
    var callID = callId
    var remote_caller_id_name = "Outbound Call"
    var remote_caller_id_number = dialerNumber
}

class VideoParams() {

}