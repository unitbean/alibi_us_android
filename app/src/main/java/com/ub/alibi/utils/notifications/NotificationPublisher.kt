package com.ub.alibi.utils.notifications

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat

import com.ub.alibi.R
import com.ub.alibi.ui.main.activities.MainActivity
import com.ub.utils.UbNotify
import java.util.Random

/**
 * Created by VOLixanov on 26.03.2018.
 */

object NotificationPublisher {

    const val CALL_IS_ANSWER = "call_is_answer"
    const val FROM_NOTIFICATION = "from_notification"
    const val NOTIFICATION_ID = "notification_id"

    @JvmStatic
    fun sendNotification(context: Context, title: String, message: String, data: Map<String, String>) {
        val notificationId = Random().nextInt(11310210)

        UbNotify.Builder(context)
            .fromLocal(R.drawable.ic_dialpad_white_24dp, title, message)
            .setChannelParams(context.getString(R.string.notification_channel_id),
                context.getString(R.string.notification_channel_name),
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        enableLights(true)
                        enableVibration(true)
                        vibrationPattern = longArrayOf(500, 500)
                        lightColor = ContextCompat.getColor(context, R.color.notification_color)
                        lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                    }
                })
            .setParams {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    color = ContextCompat.getColor(context, R.color.notification_color)
                }

                val acceptIntent = Intent(context, MainActivity::class.java)
                acceptIntent.putExtra(CALL_IS_ANSWER, true)
                acceptIntent.putExtra(NOTIFICATION_ID, notificationId)

                val denyIntent = Intent(context, MainActivity::class.java)
                denyIntent.putExtra(CALL_IS_ANSWER, false)
                denyIntent.putExtra(NOTIFICATION_ID, notificationId)

                val acceptPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    acceptIntent,
                    PendingIntent.FLAG_ONE_SHOT
                )

                val denyPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    denyIntent,
                    PendingIntent.FLAG_ONE_SHOT
                )

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                    addAction(NotificationCompat.Action.Builder(R.drawable.ic_call_white_36dp, context.getString(R.string.notification_accept_call), acceptPendingIntent).build())
                    addAction(NotificationCompat.Action.Builder(R.drawable.ic_call_end_white_36dp, context.getString(R.string.notification_deny_call), denyPendingIntent).build())
                } else {
                    addAction(R.drawable.ic_call_white_36dp, context.getString(R.string.notification_accept_call), acceptPendingIntent)
                    addAction(R.drawable.ic_call_end_white_36dp, context.getString(R.string.notification_deny_call), denyPendingIntent)
                }

                setVibrate(longArrayOf(500, 500))
                setLights(Color.WHITE, 1000, 500)
                priority = Notification.PRIORITY_MAX
                setAutoCancel(true)

                val clickNotificationIntent = Intent(context, MainActivity::class.java)
                clickNotificationIntent.putExtra(FROM_NOTIFICATION, true)
                clickNotificationIntent.putExtra(NOTIFICATION_ID, notificationId)

                /* for (key in data.keys) {
                    clickNotificationIntent.putExtra(key, data.get(key))
                } */

                // TODO создание интента-содержимого

                val clickPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    clickNotificationIntent,
                    PendingIntent.FLAG_ONE_SHOT)

                setContentIntent(clickPendingIntent)
            }
            .show(notificationId)
    }

    @JvmStatic
    fun removeNotification(context: Context, notificationId: Int) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        notificationManager?.cancel(notificationId)
    }
}
