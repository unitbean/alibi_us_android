package com.ub.alibi.utils.webrtc.models.requests

abstract class WebRtcModel(val method: String, val params: WebRtcParams) {

    companion object {
        const val METHOD_LOGIN = "login"
        const val METHOD_INVITE = "verto.invite"
        const val METHOD_BYE = "verto.bye"
    }

    var jsonrpc = "2.0"
    var id: Int? = null
}

abstract class WebRtcParams()