package com.ub.alibi.utils.webrtc.rtcutils

interface PeerConnectionListener {
    fun onIceCandidatesCreated(sdp: String)
    fun onConnected()
    fun onError()
}