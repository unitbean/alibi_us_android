package com.ub.alibi.utils.commons.help

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.StyleSpan
import com.ub.alibi.R
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.PreferenceUtils
import com.ub.alibi.utils.Utils

class HelpKeeper {

    companion object {

        /**
         * @return true, если первый диалог с предложением выбрать мелодию еще не был показан
         */
        fun isFirstDialogNotShowed(): Boolean {
            val wasShowed = PreferenceUtils.pref.getBoolean(PreferenceKeys.MELODY_SELECTING__OFFER_WAS_SHOWED, false)
            return wasShowed == false
        }

        /**
         * @return true, если второй диалог с предложением позвонить другу еще не был показан
         */
        fun isSecondDialogNotShowed(): Boolean {
            val firstDialogWasShowed = PreferenceUtils.pref.getBoolean(PreferenceKeys.MELODY_SELECTING__OFFER_WAS_SHOWED, false)
            val secondDialogWasShowed = PreferenceUtils.pref.getBoolean(PreferenceKeys.FRIEND_CALLING_OFFER_WAS_SHOWED, false)
            return firstDialogWasShowed == true && secondDialogWasShowed == false
        }

        /**
         * @return true, если третий диалог с предложением купить 3 минуты за 3 бакса еще не был показан
         */
        fun isThirdDialogNotShowed(): Boolean {
            val secondDialogWasShowed = PreferenceUtils.pref.getBoolean(PreferenceKeys.FRIEND_CALLING_OFFER_WAS_SHOWED, false)
            val thirdDialogWasShowed = PreferenceUtils.pref.getBoolean(PreferenceKeys.PACKAGE_BOUGHT_OFFER_WAS_SHOWED, false)
            return secondDialogWasShowed == true && thirdDialogWasShowed == false
        }

        /**
         * Устанавливат первый диалог, как показанный
         */
        fun setFirstStepDone() {
            PreferenceUtils.edit().putBoolean(PreferenceKeys.MELODY_SELECTING__OFFER_WAS_SHOWED, true).commit()
        }

        /**
         * Устанавливат второй диалог, как показанный
         */
        fun setSecondStepDone() {
            PreferenceUtils.edit().putBoolean(PreferenceKeys.FRIEND_CALLING_OFFER_WAS_SHOWED, true).commit()
        }

        /**
         * Устанавливат третий диалог, как показанный
         */
        fun setThirdStepDone() {
            PreferenceUtils.edit().putBoolean(PreferenceKeys.PACKAGE_BOUGHT_OFFER_WAS_SHOWED, true).commit()
        }


        /**
         * Подготавливаем и возвращаем первый HelpDialog с предложением выбрать мелодию на фон
         */
        fun getFirstStep(): HelpDialog {
            val firstDescPart = SpannableString(Utils.getString(R.string.dialog_offer_step_1_first_description_part_2))
            firstDescPart.setSpan(StyleSpan(Typeface.BOLD), 0, firstDescPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            val firstDescription = TextUtils.concat(
                    Utils.getString(R.string.dialog_offer_step_1_first_description_part_1),
                    " ",
                    firstDescPart
            )

            val secondDescPartTwo = SpannableString(Utils.getString(R.string.dialog_offer_step_1_second_description_part_2))
            secondDescPartTwo.setSpan(StyleSpan(Typeface.BOLD), 0, secondDescPartTwo.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            val secondDescription = TextUtils.concat(
                    Utils.getString(R.string.dialog_offer_step_1_second_description_part_1),
                    " ",
                    secondDescPartTwo,
                    " ",
                    Utils.getString(R.string.dialog_offer_step_1_second_description_part_3)
            )

            return HelpDialog.newInstance("1",
                    R.drawable.ic_help_dialog_choose_sound,
                    R.string.dialog_offer_step_1_title,
                    firstDescription,
                    secondDescription,
                    R.string.dialog_offer_step_1_button_text)
        }


        /**
         * Подготавливаем и возвращаем второй HelpDialog с предложением позвонить другу
         */
        fun getSecondStep(): HelpDialog {
            val firstDescPart = SpannableString(Utils.getString(R.string.dialog_offer_step_2_first_description_part_2))
            firstDescPart.setSpan(StyleSpan(Typeface.BOLD), 0, firstDescPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            val firstDescription = TextUtils.concat(
                    Utils.getString(R.string.dialog_offer_step_2_first_description_part_1),
                    " ",
                    firstDescPart,
                    " ",
                    Utils.getString(R.string.dialog_offer_step_2_first_description_part_3)
            )

            return HelpDialog.newInstance("2",
                    R.drawable.ic_help_dialog_choose_make_fun,
                    R.string.dialog_offer_step_2_title,
                    firstDescription,
                    null,
                    R.string.dialog_offer_step_2_button_text)
        }

        /**
         * Подготавливаем и возвращаем третий HelpDialog с предложением купить 3 минуты за 3 "бакса"
         */
        fun getThirdStep(): HelpDialog {
            val firstDescPart = SpannableString(Utils.getString(R.string.dialog_offer_step_3_first_description_part_2))
            firstDescPart.setSpan(StyleSpan(Typeface.BOLD), 0, firstDescPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            val secondDescPart = SpannableString(Utils.getString(R.string.dialog_offer_step_3_first_description_part_4))
            secondDescPart.setSpan(StyleSpan(Typeface.BOLD), 0, secondDescPart.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            val firstDescription = TextUtils.concat(
                    Utils.getString(R.string.dialog_offer_step_3_first_description_part_1),
                    " ",
                    firstDescPart,
                    " ",
                    Utils.getString(R.string.dialog_offer_step_3_first_description_part_3),
                    " ",
                    secondDescPart
            )

            return HelpDialog.newInstance("3",
                    R.drawable.ic_offer_buy_a_minutes,
                    R.string.dialog_offer_step_3_title,
                    firstDescription,
                    null,
                    R.string.dialog_offer_step_3_button_text)
        }

    }

}