package com.ub.alibi.utils.webrtc

import android.content.Context
import androidx.annotation.StringRes
import com.ub.alibi.di.services.UserService
import com.ub.alibi.di.services.database.DatabaseService
import com.ub.alibi.di.services.database.models.CallModel
import com.ub.alibi.utils.LogUtils
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.PreferenceUtils
import com.ub.alibi.utils.webrtc.rtcutils.OnCallEventListener
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext

class WebRtcService(val context: Context, private val userService: UserService, private val databaseService: DatabaseService) : WebRtcCallProvider.WebRtcEventListener, CoroutineScope {

    companion object {
        val TAG = "AlibiWebRtc_" + WebRtcService::class.java.simpleName

        @JvmStatic
        fun getFullUserLoginByNumber(number: String): String {
            return "$number@ca01.voip.alibicall.com"
        }
    }

    override val coroutineContext: CoroutineContext = Dispatchers.Main

    private var startTime: Long = -1

    /**
     * Список слушателей, оповещаемых при событиях звонка
     */
    private val onCallEventListeners: MutableList<OnCallEventListener> = mutableListOf()

    private val webSocketAddress = "wss://ca01.voip.alibicall.com:8081/"

    private var webRtcCallProvider: WebRtcCallProvider? = null

    private var callDisposable: Disposable? = null

    val currentDialerNumber: String?
            get() = webRtcCallProvider?.currentDialerNumber

    private val currentStartCallTime: Long?
        get() = webRtcCallProvider?.callStartTime?.time

    /**
     * Добавляет слушателя в список [onCallEventListeners], если его еще нет в списке
     * @param listener добавляемый слушатель
     */
    fun addCallEventListener(listener: OnCallEventListener) {
        if (!onCallEventListeners.contains(listener)) {
            onCallEventListeners.add(listener)
        }
    }

    /**
     * Удаляет слушателя из списка [onCallEventListeners], если он есть в списке
     * @param listener удаляемый слушатель
     */
    fun removeCallEventListener(listener: OnCallEventListener) {
        if (onCallEventListeners.contains(listener)) {
            onCallEventListeners.remove(listener)
        }
    }

    /**
     * Оповещает подписанных слушателей о наступлении события (см. [OnCallEventListener])
     * @param event событие, вызываемое слушателями (см. [OnCallEventListener])
     */
    private fun notifyEventListeners(event: OnCallEventListener.() -> Unit) {
        onCallEventListeners.forEach {
            it.event()
        }
    }

    /**
     * Необходимо вызвать, чтобы открыть соединение с веб сокетом и начать звонок
     */
    fun startCall(dialerNumber: String) {
        if (webRtcCallProvider == null) {
            callDisposable = userService.userObservable
                    .subscribe ({
                        webRtcCallProvider = WebRtcCallProvider(context, this, webSocketAddress)
                        webRtcCallProvider?.invite(it, dialerNumber)
                    }, {
                        LogUtils.w(TAG, "The call was aborted because the user instance could not be retrieved")
                        endCall()
                    })
        }
    }

    /**
     * Необходимо вызвать, чтобы закрыть соединение с веб сокетом завершить звонок
     */
    fun endCall() {
        webRtcCallProvider?.bye()
    }

    /**
     * Callback начала звонка. Вызывается, когда обмен sdp прошел успешно
     */
    override fun onCallStarted() {
        startTime = System.currentTimeMillis()

        launch {
            notifyEventListeners { onCallStarted() }
        }

    }

    /**
     * Callback завершения звонка. Вызывается, когда звонок был завершен одной из сторон, либо при ошибках соединения
     */
    override fun onCallIsOver() {
        saveCallInDatabase()
        if (webRtcCallProvider != null) {
            webRtcCallProvider = null
        }

        if (callDisposable != null && callDisposable?.isDisposed == false) {
            callDisposable?.dispose()
            callDisposable = null
        }

        launch {
            val callTime = getCallTime()
            notifyEventListeners { onCallCompleted(callTime) }
        }
    }

    private fun getCallTime(): Long {
        return System.currentTimeMillis() - startTime
    }

    /**
     * Callback ошибки. Будет вызван при проблемах с соединением.
     */
    override fun onConnectionError(@StringRes messId: Int) {
        webRtcCallProvider?.bye()
        launch {
            withContext(Dispatchers.Main) {
                notifyEventListeners { onCallError(messId) }
            }
        }
    }

    private fun saveCallInDatabase() {
        val dialerNumber = currentDialerNumber
        val callTime = currentStartCallTime

        if (dialerNumber == null || callTime == null) {
            return
        }

        if (PreferenceUtils.pref.getBoolean(PreferenceKeys.KEY_IS_CALLS_HISTORY_ENABLE, true)) {
            val subscription = Observable.just(CallModel())
                    .subscribeOn(Schedulers.io())
                    .map { model ->
                        model.sipLogin = dialerNumber
                        model.callTime = callTime
                        model
                    }
                    .flatMap { model -> databaseService.insertCalls(model) }
                    .subscribe({ result -> LogUtils.d(WebRtcService.TAG, "Call saved in DB: " + Arrays.toString(result)) },
                            { throwable -> LogUtils.e(WebRtcService.TAG, throwable.message, throwable) })
        }
    }

}