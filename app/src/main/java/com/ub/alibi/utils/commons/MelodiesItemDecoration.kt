package com.ub.alibi.utils.commons

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

class MelodiesItemDecoration(private val verticalSpaceHeight: Int, private val horizontalSpace: Int) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
        outRect.bottom = verticalSpaceHeight
        outRect.left = horizontalSpace
        outRect.right = horizontalSpace
    }
}