package com.ub.alibi.utils.commons.help

import android.app.Dialog
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.core.content.ContextCompat
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.ub.alibi.BaseApplication
import com.ub.alibi.R


class HelpDialog : androidx.fragment.app.DialogFragment() {

    private var onHelpDialogListener: OnHelpDialogListener? = null

    lateinit var tvSteps: TextView
    lateinit var ivImage: ImageView
    lateinit var tvTitle: TextView
    lateinit var tvFirstDescription: TextView
    lateinit var tvSecondDescription: TextView
    lateinit var btnPositive: Button

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCanceledOnTouchOutside(true)
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_help, container, false)

        tvSteps = view.findViewById(R.id.tv_dialog_steps)
        ivImage = view.findViewById(R.id.iv_dialog_image)
        tvTitle = view.findViewById(R.id.tv_dialog_title)
        tvFirstDescription = view.findViewById(R.id.tv_dialog_first_description)
        tvSecondDescription = view.findViewById(R.id.tv_dialog_second_description)
        btnPositive = view.findViewById(R.id.btn_dialog_ok)


        if (arguments != null) {
            val spannable = SpannableString(getString(R.string.dialog_offer_step))
            val currentStep = SpannableString(arguments?.getString(KEY_STEP))
            val totalSteps = SpannableString(getString(R.string.dialog_offer_steps_total))

            val colorRes = ContextCompat.getColor(BaseApplication.appComponent.context, R.color.colorPrimary)

            currentStep.setSpan(ForegroundColorSpan(colorRes), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            currentStep.setSpan(StyleSpan(Typeface.BOLD), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            totalSteps.setSpan(StyleSpan(Typeface.BOLD), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            val stepResult = TextUtils.concat(spannable, " ", currentStep, " / ", totalSteps)

            if(arguments != null) {
                val imageResource = arguments?.getInt(KEY_IMAGE_RES)
                val titleRes = arguments?.getInt(KEY_TITLE)
                val firstDescription = arguments?.getCharSequence(KEY_FIRST_DESCRIPTION)
                val secondDescription = arguments?.getCharSequence(KEY_SECOND_DESCRIPTION)
                val btnTextRes = arguments?.getInt(KEY_BUTTON_TEXT)

                tvSteps.text = stepResult

                if(imageResource != null) { ivImage.setImageResource(imageResource) }

                if(titleRes != null) { tvTitle.text = getString(titleRes) }

                if(firstDescription != null) { tvFirstDescription.text = firstDescription }

                if(secondDescription != null) { tvSecondDescription.text = secondDescription }
                else { tvSecondDescription.visibility = View.GONE }

                if(btnTextRes != null) { btnPositive.text = getString(btnTextRes) }
            }
        }

        btnPositive.setOnClickListener { it ->
            onHelpDialogListener?.onClickButton()
            dismiss()
        }

        return view
    }

    fun setListener(listener: OnHelpDialogListener) {
        onHelpDialogListener = listener
    }

    interface OnHelpDialogListener {
        fun onClickButton()
    }

    companion object {

        val TAG = HelpDialog::class.java.simpleName

        val KEY_STEP = "KEY_STEP"
        val KEY_IMAGE_RES = "KEY_IMAGE_RES"
        val KEY_TITLE = "KEY_TITLE"
        val KEY_FIRST_DESCRIPTION = "KEY_FIRST_DESCRIPTION"
        val KEY_SECOND_DESCRIPTION = "KEY_SECOND_DESCRIPTION"
        val KEY_BUTTON_TEXT = "KEY_BUTTON_TEXT"

        fun newInstance(step: String, imageRes: Int, titleRes: Int, firstDescription: CharSequence, secondDescription: CharSequence?, btnTextRes: Int): HelpDialog {
            val numberAlertDialog = HelpDialog()
            val args = Bundle()
            args.putString(KEY_STEP, step)
            args.putInt(KEY_IMAGE_RES, imageRes)
            args.putInt(KEY_TITLE, titleRes)
            args.putCharSequence(KEY_FIRST_DESCRIPTION, firstDescription)
            args.putCharSequence(KEY_SECOND_DESCRIPTION, secondDescription)
            args.putInt(KEY_BUTTON_TEXT, btnTextRes)
            numberAlertDialog.arguments = args
            return numberAlertDialog
        }
    }
}
