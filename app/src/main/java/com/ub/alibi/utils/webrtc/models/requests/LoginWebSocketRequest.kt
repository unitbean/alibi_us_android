package com.ub.alibi.utils.webrtc.models.requests

class LoginWebSocketRequest(login: String, passwd: String) : WebRtcModel(METHOD_LOGIN, LoginParams(login, passwd))

class LoginParams(val login: String, val passwd: String): WebRtcParams()