package com.ub.alibi.utils.webrtc.models.requests

class ByeWebSocketRequest(byeParams: ByeParams) : WebRtcModel(METHOD_BYE, byeParams)

class ByeParams(var dialogParams: ByeDialogParams, var sessid: String?) : WebRtcParams()

class ByeDialogParams(login: String, callerId: String, dialerNumber: String, callId: String) {
    var useVideo = false
    var screenShare = false
    var useMic = "any"
    var useSpeak = "any"
    var tag = callId
    var localTag = null
    var login = login
    var destination_number = dialerNumber
    var caller_id_name = callerId
    var caller_id_number = callerId
    var callID = callId
    var remote_caller_id_name = "Outbound Call"
    var remote_caller_id_number = dialerNumber
}