package com.ub.alibi.utils

class PreferenceKeys {
    companion object {
        const val KEY_SETTINGS = "settings"

        const val KEY_SETTINGS_USER_ACCESS_TOKEN = "settings_user_access_token"
        const val KEY_SETTINGS_USER_ACCESS_TOKEN_EXPIRED_IN = "settings_user_access_token_expired_in"
        const val KEY_SETTINGS_USER_ACCESS_TOKEN_TIMEZONE = "settings_user_access_token_timezone"
        const val KEY_SETTINGS_USER_REFRESH_TOKEN = "settings_user_refresh_token"
        const val KEY_SETTINGS_USER_REFRESH_TOKEN_EXPIRED_IN = "settings_user_refresh_token_expired_in"
        const val KEY_SETTINGS_USER_REFRESH_TOKEN_TIMEZONE = "settings_user_refresh_token_timezone"

        const val KEY_SETTINGS_USER_SEEN_HELP = "settings_user_seen_help"

        const val KEY_USER_PASSWORD = "user_password"
        const val KEY_IS_PASSWORD_ENABLE = "is_password_enable"
        const val KEY_IS_CALLS_HISTORY_ENABLE = "is_calls_history_enable"
        const val KEY_IS_BACKGROUND_MODE_ENABLE = "is_background_mode_enable"

        const val CALL_BUTTON_DID_TOUCH = "CALL_BUTTON_DID_TOUCH"
        const val RATE_DID_TOUCH = "RATE_DID_TOUCH"

        const val MELODY_SELECTING__OFFER_WAS_SHOWED = "MELODU_SELECTING__OFFER_WAS_SHOWED"
        const val FRIEND_CALLING_OFFER_WAS_SHOWED = "FRIEND_CALLING_OFFER_WAS_SHOWED"
        const val PACKAGE_BOUGHT_OFFER_WAS_SHOWED = "PACKAGE_BOUGHT_OFFER_WAS_SHOWED"

        const val KEY_DEVICE_TOKEN = "KEY_DEVICE_TOKEN"
    }
}