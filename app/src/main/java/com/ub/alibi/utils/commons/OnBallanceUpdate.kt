package com.ub.alibi.utils.commons

import com.ub.alibi.ui.main.models.UserModel

/**
 * Интерфейс используется для того чтобы прокидывать балланс в фрагменты после звонка
 */
interface OnBallanceUpdate {
    fun onBallanceUpdated(user: UserModel)
}