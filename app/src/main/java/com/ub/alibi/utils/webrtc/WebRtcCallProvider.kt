package com.ub.alibi.utils.webrtc

import android.content.Context
import androidx.annotation.StringRes
import com.google.gson.Gson
import com.ub.alibi.R
import com.ub.alibi.di.services.database.DatabaseService
import com.ub.alibi.di.services.database.models.CallModel
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.utils.LogUtils
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.PreferenceUtils
import com.ub.alibi.utils.webrtc.audio.AppRTCAudioManager
import com.ub.alibi.utils.webrtc.models.requests.*
import com.ub.alibi.utils.webrtc.models.responses.WebSocketResponse
import com.ub.alibi.utils.webrtc.rtcutils.PeerConnectionListener
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import java.lang.Exception
import java.util.*

class WebRtcCallProvider(val context: Context, val webRtcEventListener: WebRtcEventListener,
                         private val webSocketAddress: String) {

    companion object {
        val TAG = "AlibiWebRtc_" + WebRtcCallProvider::class.java.simpleName
    }

    private var webSocketChannel: WebSocketChannel? = null

    private var peerConnectionProvider: RtcClient? = null

    var callStartTime: Date? = null

    private var userModel: UserModel? = null
    var currentDialerNumber: String? = null

    private var sessId: String? = null
    private var callId: String? = null
    private var audioManager: AppRTCAudioManager? = null

    /**
     * Начинаем вызов.
     * В методе создается экземпляр, в котором открывается соединения с веб-сокетом.
     * Так же создается [audioManager], отвечающий за настройку аудио устройств
     */
    fun invite(userModel: UserModel, dialerNumber: String) {
        callStartTime = Date()
        this.userModel = userModel
        currentDialerNumber = dialerNumber

        if (webSocketChannel == null || webSocketChannel?.currentState == WebSocketState.STATE_NEW) {
            val webSocketListener = AlibiWebSocketListener()
            webSocketChannel = WebSocketChannel(webSocketListener)
            webSocketChannel?.connect(webSocketAddress)
            webSocketChannel?.currentState = WebSocketState.STATE_CONNECTING
        }

        audioManager = AppRTCAudioManager.create(context)
    }

    /**
     * Подготавливаем и отправляем запрос verto.bye. В [AlibiWebSocketListener.onMessage] ожидаем
     * result с полем message = CALL ENDED и в этом случае закрываем соединение с web-сокетом
     */
    fun bye() {
        val dialerNumber = currentDialerNumber ?: return
        val callerId = userModel?.sipLogin ?: return
        val userLogin = WebRtcService.getFullUserLoginByNumber(userModel?.sipLogin ?: return)
        val byeDialogParams = ByeDialogParams(userLogin, callerId, dialerNumber, callId ?: "")
        val byeParams = ByeParams(byeDialogParams, sessId)
        val byeWebSocketRequest = ByeWebSocketRequest(byeParams)

        webSocketChannel?.bye(byeWebSocketRequest)
    }

    /**
     * Метод создает RtcClient для настройки соединения. И начинает вызов
     */
    private fun createRtcClient() {
        peerConnectionProvider = PeerConnectionProvider(context, object : PeerConnectionListener {
            override fun onIceCandidatesCreated(sdp: String) {

                callId = UUID.randomUUID().toString()
                val dialerNumber = currentDialerNumber ?: return
                val callerId = userModel?.sipLogin ?: return
                val userLogin = WebRtcService.getFullUserLoginByNumber(userModel?.sipLogin ?: return)

                val dialogParams = DialogParams(userLogin, callerId, dialerNumber, callId ?: "")

                val sdpOfferParams = SdpOfferParams(sdp, dialogParams, sessId)

                val sdpOfferRequest = SdpOfferWebSocketRequest(sdpOfferParams)

                webSocketChannel?.sendOffer(sdpOfferRequest)
            }

            override fun onConnected() {
                GlobalScope.launch(Dispatchers.Main) {
                    audioManager?.start { audioDevice, availableAudioDevices -> }
                }
                webRtcEventListener.onCallStarted()
            }

            override fun onError() {
                webRtcEventListener.onConnectionError(R.string.error_while_trying_to_connect)
            }

        })

        peerConnectionProvider?.createOfferSdp()
    }

    /**
     * WEB-SOCKET CALLBACKS
     * Все события, связанные c взаимодействием с вебсокетом падают сюда
     */
    private inner class AlibiWebSocketListener : WebSocketListener() {
        override fun onOpen(webSocket: WebSocket, response: Response) {
            webSocketChannel?.currentState = WebSocketState.STATE_CONNECTED
            val login = userModel?.sipLogin ?: return
            val password = userModel?.sipPassword ?: return
            webSocketChannel?.register(WebRtcService.getFullUserLoginByNumber(login), password)
            webSocketChannel?.currentState = WebSocketState.STATE_REGISTERING
        }

        override fun onMessage(webSocket: WebSocket, text: String) {
            LogUtils.d("${WebSocketChannel.TAG} <<< <<< <<<", text)
            try {
                val model = toModel(text)

                if (model.result["message"] == "logged in") {
                    webSocketChannel?.currentState = WebSocketState.STATE_REGISTERED
                    sessId = model.result["sessId"]
                    createRtcClient()
                } else if (model.result["message"] == "CALL ENDED") {
                    destroyConnection()
                } else if (model.method == "verto.answer" || model.method == "verto.media") {
                    peerConnectionProvider?.setupAnswerSdp(model.params["sdp"] ?: return)
                } else if (model.method == "verto.bye") {
                    destroyConnection()
                }
            } catch (e: Exception) {
                LogUtils.e(WebSocketChannel.TAG, e.message)
            }
        }

        override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
            webSocketChannel?.currentState = WebSocketState.STATE_CLOSED
        }

        override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            LogUtils.d(WebSocketChannel.TAG, "Web socket is closed with code $code $reason")
            webSocketChannel = null
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            LogUtils.d(WebSocketChannel.TAG, t.message)
            destroyConnection()
        }
    }

    /**
     * Закрывает соединение с web сокетом. Останавливает аудио-поток и закрывает peerConnection.
     * Функция, завершающая вызов.
     */
    private fun destroyConnection() {
        if (audioManager != null) {
            GlobalScope.launch(Dispatchers.Main) {
                audioManager?.stop()
                audioManager = null
            }
        }

        if (peerConnectionProvider != null) {
            peerConnectionProvider?.dismiss()
            peerConnectionProvider = null
        }

        if (webSocketChannel?.currentState != WebSocketState.STATE_CLOSED &&
                webSocketChannel != null) {
            webSocketChannel?.disconnect()
        }

        webRtcEventListener.onCallIsOver()
    }

    private fun toModel(json: String): WebSocketResponse {
        return Gson().fromJson(json, WebSocketResponse::class.java)
    }

    interface WebRtcEventListener {
        fun onCallStarted()
        fun onCallIsOver()
        fun onConnectionError(@StringRes messId: Int)
    }
}