package com.ub.alibi.utils

import com.bumptech.glide.module.AppGlideModule

/**
 * Created by pozharov on 02.03.2018.
 */

@com.bumptech.glide.annotation.GlideModule
class GlideModule : AppGlideModule()
