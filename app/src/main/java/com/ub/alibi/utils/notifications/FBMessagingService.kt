package com.ub.alibi.utils.notifications

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.ub.alibi.R

/**
 * Created by VOLixanov on 26.03.2018.
 */

class FBMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        NotificationPublisher.sendNotification(this, getString(R.string.notification_title), getString(R.string.notification_message), remoteMessage!!.data)
    }

    companion object {

        private const val TAG = "FBMessagingService"
    }
}
