package com.ub.alibi.utils.webrtc.rtcutils

import androidx.annotation.StringRes

interface OnCallEventListener {
    /**
     * Callback успешного начала вызова
     */
    fun onCallStarted()

    /**
     * Callback успешного завершения вызова. Не обязателен для реализации.
     */
    fun onCallCompleted(callTime: Long) {  }

    /**
     * Callback ошибки соединения. Не обязателен для реализации.
     */
    fun onCallError(@StringRes strId: Int) {  }
}
