package com.ub.alibi.utils.webrtc

interface RtcClient {
    fun createOfferSdp()
    fun dismiss()
    fun setupAnswerSdp(sdp: String?)
}