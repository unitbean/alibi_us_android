package com.ub.alibi.utils.webrtc

import android.annotation.SuppressLint
import com.google.gson.Gson
import com.ub.alibi.utils.LogUtils
import com.ub.alibi.utils.webrtc.models.requests.*
import okhttp3.*
import okhttp3.OkHttpClient
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.*


enum class WebSocketState {
    STATE_NEW,
    STATE_CONNECTING,
    STATE_CONNECTED,
    STATE_REGISTERING,
    STATE_REGISTERED,
    STATE_CLOSED
}

class WebSocketChannel(private val webSocketListener: WebSocketListener) {

    companion object {
        val TAG = "AlibiWebRtc_" + WebSocketChannel::class.java.simpleName

        const val NORMAL_REASON = 1000
    }

    var currentState = WebSocketState.STATE_NEW

    private var client: OkHttpClient = createUnsafeOkHttpClient()
    private var webSocket: WebSocket? = null

    fun connect(wsUrl: String) {
        val request = Request.Builder().url(wsUrl).build()
        webSocket = client.newWebSocket(request, webSocketListener)
        client.dispatcher().executorService().shutdown()
    }

    /**
     * Создает небезопасный OkHttpClient, который доверяет всем сертификатам.
     */
    fun createUnsafeOkHttpClient(): OkHttpClient {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }

                @SuppressLint("TrustAllX509TrustManager")
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                @SuppressLint("TrustAllX509TrustManager")
                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.getSocketFactory()

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier(object : HostnameVerifier {
                @SuppressLint("BadHostnameVerifier")
                override fun verify(hostname: String, session: SSLSession): Boolean {
                    return true
                }
            })

            return builder.build()
        } catch (e: Exception) {
            return OkHttpClient()
        }
    }

    fun disconnect() {
        webSocket?.close(NORMAL_REASON, "The call was completed")
    }

    fun register(login: String, password: String) {
        try {
            if (currentState == WebSocketState.STATE_CONNECTED) {
                val registerRtcModel = LoginWebSocketRequest(login, password)
                val string = toJson(registerRtcModel)
                webSocket?.send(string)
                LogUtils.d("$TAG >>> >>> >>>", string)
            }
        } catch (e: Exception) {
            LogUtils.w(TAG, "Json parsing exception")
        }
    }

    fun sendOffer(sdpOfferModel: SdpOfferWebSocketRequest) {
        send(sdpOfferModel)
    }

    private fun send(model: WebRtcModel) {
        try {
            val string = toJson(model)
            send(string)
        } catch (e: Exception) {
            LogUtils.w(TAG, "Json parsing exception")
        }
    }

    private fun send(str: String) {
        if (currentState == WebSocketState.STATE_REGISTERED) {
            LogUtils.d("$TAG >>> >>> >>>", str)
            webSocket?.send(str)
        }
    }

    private fun <T : WebRtcModel> toJson(model: T): String {
        return Gson().toJson(model)
    }

    fun bye(byeWebSocketRequest: ByeWebSocketRequest) {
        send(byeWebSocketRequest)
    }

}
