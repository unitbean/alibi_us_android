package com.ub.alibi.utils.commons.numbersDialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ub.alibi.R
import com.ub.alibi.ui.base.adapters.BaseAdapter

class NumbersDialog : androidx.fragment.app.DialogFragment(), BaseAdapter.OnItemClickListener {

    private lateinit var onNumberSelectedListener: OnNumberSelectListener

    private lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var tvSubtitleName: TextView

    var stringNumberList: List<String> = arrayListOf()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true)
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_numbers, container, false)

        tvSubtitleName = view.findViewById(R.id.tv_dialog_subtitle)
        recyclerView = view.findViewById(R.id.rv_numbers_dialog)

        stringNumberList = arguments?.getStringArrayList(KEY_NUMBER_LIST) ?: arrayListOf()

        tvSubtitleName.text = arguments?.getString(KEY_NAME)

        val adapter = NumberDialogRvAdapter(stringNumberList)
        adapter.setListener(this)
        recyclerView.adapter = adapter

        return view
    }

    fun setListener(listener: OnNumberSelectListener) {
        onNumberSelectedListener = listener
    }

    interface OnNumberSelectListener {
        fun onClickNumber(number: String)
    }


    override fun onClick(v: View?, position: Int) {
        if(this::onNumberSelectedListener.isInitialized) {
            onNumberSelectedListener.onClickNumber(stringNumberList.get(position))
            dismiss()
        }
    }

    companion object {

        val TAG = NumbersDialog::class.java.simpleName

        val KEY_NAME = "KEY_NAME"
        val KEY_NUMBER_LIST = "KEY_NUMBER_LIST"

        fun newInstance(name: String, numbers: List<String>): NumbersDialog {
            val numbersDialog = NumbersDialog()
            val args = Bundle()
            args.putString(KEY_NAME, name)
            val numberArrayList: ArrayList<String> = arrayListOf()
            numberArrayList.addAll(numbers)
            args.putStringArrayList(KEY_NUMBER_LIST, numberArrayList)
            numbersDialog.arguments = args
            return numbersDialog
        }
    }
}
