package com.ub.alibi.utils.commons.numbersDialog

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ub.alibi.R
import com.ub.alibi.ui.base.adapters.BaseAdapter
import android.text.method.ScrollingMovementMethod
import android.view.Gravity


class NumberDialogRvAdapter(val numbers: List<String>)  : BaseAdapter<NumberDialogRvAdapter.NumberViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumberViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rv_dialog_numbers, parent, false)
        return NumberViewHolder(v)
    }

    override fun getItemCount(): Int {
        return numbers.size
    }

    override fun onBindViewHolder(baseHolder: NumberViewHolder, position: Int) {
        baseHolder.bind()
    }

    inner class NumberViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        val tvNumber: TextView
        val viewUnderline: View

        init {
            tvNumber = itemView.findViewById(R.id.tv_dialog_number)
            viewUnderline = itemView.findViewById(R.id.view_dialog_number)

            tvNumber.setMovementMethod(ScrollingMovementMethod())

            if(listener != null) {
                tvNumber.setOnClickListener({
                    listener.onClick(tvNumber, adapterPosition)
                })
            }
        }

        fun bind() {
            tvNumber.text = numbers.get(adapterPosition)
            tvNumber.gravity = Gravity.CENTER
            if(numbers.size - 1 == adapterPosition) {
                viewUnderline.visibility = View.GONE
            } else {
                viewUnderline.visibility = View.VISIBLE
            }
        }
    }

}