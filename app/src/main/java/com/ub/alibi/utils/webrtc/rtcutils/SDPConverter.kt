package com.ub.alibi.utils.webrtc.rtcutils

import org.webrtc.IceCandidate

class SDPConverter {

    companion object {
        @JvmStatic
        fun addCondidates(sdp: String, candidateList: ArrayList<IceCandidate>): String {
            val lines = sdp.split("\r\n").toTypedArray().toMutableList()

            var rtcpIndex = lines.indexOfFirst { it.contains("a=rtcp") }
            rtcpIndex += 1
            for (i in 0 until candidateList.size) {
                val ice = candidateList[i]
                lines.add(rtcpIndex + i, "a=${ice.sdp}")
            }

            val endSdp = lines.merge("\r\n")

            return endSdp
        }

        fun getCondidates(sdp: String): List<IceCandidate> {
            val lines = sdp.split("\r\n").toTypedArray().toMutableList()

            val condidateList = ArrayList<IceCandidate>()
            lines.forEach {
                if (it.contains("a=candidate")) {
                    condidateList += IceCandidate("data", 1, it.replace("a=", ""))
                }
            }

            return condidateList
        }

    }

}


fun List<String>.merge(divider: String): String {
    var str = ""
    this.forEachIndexed { index, s ->
        str += s
        if (index + 1 < this.size) {
            str += divider
        }
    }
    return str
}
