package com.ub.alibi.ui.tariffs.interactors

import com.ub.alibi.R
import com.ub.alibi.di.services.api.models.requests.TransactionRequest
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.ui.tariffs.models.TariffModel
import com.ub.alibi.ui.tariffs.reporitories.ITariffRepository
import com.ub.alibi.utils.LogUtils
import com.ub.alibi.utils.Utils
import io.reactivex.Observable
import java.util.ArrayList

class TariffInteractor(private val repository: ITariffRepository) : ITariffInteractor {

    override fun loadUserInfo(): Observable<UserModel> {
        return repository.getUser()
    }

    override fun loadTariffs(): Observable<List<TariffModel>> {
        return repository
            .getTariffs()
            .map { listResponse ->
                val tariffModels = ArrayList<TariffModel>()

                listResponse.map {
                    val tariffModel = TariffModel()
                    tariffModel.id = it.id
                    tariffModel.title = it.title
                    tariffModel.price = Utils.getString(R.string.activity_tariffs_currency, it.amount)
                    tariffModel.millis = it.millisec
                    tariffModel.billingId = it.androidId
                    tariffModels.add(tariffModel)
                }

                tariffModels
        }
    }

    override fun startPurchaseTransaction(model: TariffModel?, token: String): Observable<UserModel> {
        return repository
            .createTransaction(TransactionRequest(model?.millis ?: throw IllegalStateException("Tariff model == null"), token))
            .retryWhen { repository.retryAfterRefreshToken(it) }
            .flatMap { transaction ->
                LogUtils.d("Transaction", "Status $transaction")

                repository.cleanUser()
                repository.getUser()
            }
    }
}