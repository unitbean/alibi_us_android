package com.ub.alibi.ui.contacts.models

class HeaderModel(val letter : String) : ContactType {

    override fun getType(): Type = Type.HEADER

    override fun equals(other: Any?): Boolean {
        return when (other) {
            null -> false
            !is HeaderModel -> false
            other.letter == letter -> true
            else -> false
        }
    }

    override fun hashCode(): Int {
        return letter.hashCode()
    }
}