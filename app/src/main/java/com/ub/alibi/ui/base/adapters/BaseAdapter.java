package com.ub.alibi.ui.base.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by pozharov on 05.03.2018.
 */

public abstract class BaseAdapter <T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    protected OnItemClickListener listener;

    @Override
    abstract public void onBindViewHolder(@NonNull T baseHolder, int position);

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener{
        void onClick(View v, int position);
    }
}
