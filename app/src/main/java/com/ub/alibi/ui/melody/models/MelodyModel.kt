package com.ub.alibi.ui.melody.models

import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.di.services.api.models.responses.MelodyResponse

/**
 * Created by pozharov on 05.03.2018.
 */

class MelodyModel(melodyResponse: MelodyResponse) {

    val id: String = melodyResponse.id
    val title: String = melodyResponse.title
    val description: String = melodyResponse.description
    val fileUrl: String = ApiService.getFileUrl(melodyResponse.fileId)
    val pictureUrl: String = ApiService.getPictureUrl(melodyResponse.picId)
    var isCurrent: Boolean = false
    var isFavorite: Boolean = false
    var isPlaying: Boolean = false

    init {
        this.isCurrent = melodyResponse.isCurrentMelody
        this.isFavorite = melodyResponse.isFavoriteMelody
    }
}
