package com.ub.alibi.ui.settings.presenters

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.settings.views.SettingsView
import com.ub.alibi.utils.LogUtils
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.PreferenceUtils

import io.reactivex.schedulers.Schedulers

/**
 * Created by pozharov on 06.03.2018.
 */

@InjectViewState
class SettingsPresenter : BasePresenter<SettingsView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.onSetCallsHistoryEnable(PreferenceUtils.pref
            .getBoolean(PreferenceKeys.KEY_IS_CALLS_HISTORY_ENABLE, true))
        viewState.onSetBackgroundMode(PreferenceUtils.pref
            .getBoolean(PreferenceKeys.KEY_IS_BACKGROUND_MODE_ENABLE, false))
        viewState.onSetConfirmByPasswordEnable(PreferenceUtils.pref
            .getBoolean(PreferenceKeys.KEY_IS_PASSWORD_ENABLE, true))
    }

    fun onCallsHistoryStateChanged(isChecked: Boolean) {
        PreferenceUtils.edit()
            .putBoolean(PreferenceKeys.KEY_IS_CALLS_HISTORY_ENABLE, isChecked)
            .apply()
    }

    fun clearHistory() {
        val subscription = coreServices.database.deleteAllCalls()
            .subscribeOn(Schedulers.io())
            .subscribe({ result -> }
            ) { throwable -> LogUtils.e(TAG, throwable.message, throwable) }
        subscriptions.add(subscription)
    }

    fun onBackgroundModeStateChange(isChecked: Boolean) {
        PreferenceUtils.edit()
            .putBoolean(PreferenceKeys.KEY_IS_BACKGROUND_MODE_ENABLE, isChecked)
            .apply()
    }

    fun onConfirmByPasswordStateChange(isChecked: Boolean) {
        PreferenceUtils.edit()
            .putBoolean(PreferenceKeys.KEY_IS_PASSWORD_ENABLE, isChecked)
            .apply()
    }

    companion object {

        private const val TAG = "SettingsPresenter"
    }
}
