package com.ub.alibi.ui.call.fragments

import android.annotation.TargetApi
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.PowerManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ub.alibi.R
import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.ui.base.dialogs.BaseDialog
import com.ub.alibi.ui.call.presenters.CallPresenter
import com.ub.alibi.ui.call.views.CallView
import com.ub.alibi.utils.GlideApp
import com.ub.alibi.utils.LogUtils

import java.util.Locale
import java.util.concurrent.TimeUnit

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

import android.content.Context.POWER_SERVICE
import android.content.Context.SENSOR_SERVICE

/**
 * Created by VOLixanov on 23.03.2018.
 */

class CallFragment : BaseDialog(), CallView, View.OnClickListener, SensorEventListener {

    @InjectPresenter
    lateinit var presenter: CallPresenter

    lateinit var partnerImage: ImageView
    lateinit var background: ImageView
    private var callTime: TextView? = null
    private var partnerNumber: TextView? = null
    private var timer: Disposable? = null
    private var sensorManager: SensorManager? = null
    private var sensor: Sensor? = null
    private var mPowerManager: PowerManager? = null
    private var wakeLock: PowerManager.WakeLock? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_call, null)

        val partnerName = view.findViewById<TextView>(R.id.et_call_parnter)
        partnerImage = view.findViewById(R.id.iv_call_partner)
        val btnDeny = view.findViewById<ImageButton>(R.id.b_call_deny)
        val btnAccept = view.findViewById<ImageButton>(R.id.b_call_accept)
        val btnMute = view.findViewById<ImageButton>(R.id.b_call_mute)
        val btnSpeaker = view.findViewById<ImageButton>(R.id.b_call_loud)
        val btnKeypad = view.findViewById<ImageButton>(R.id.b_call_keypad)
        background = view.findViewById(R.id.iv_call_background)
        callTime = view.findViewById(R.id.tv_call_time)
        partnerNumber = view.findViewById(R.id.tv_call_number)

        mPowerManager = view.context.getSystemService(POWER_SERVICE) as PowerManager

        sensorManager = view.context.getSystemService(SENSOR_SERVICE) as SensorManager
        if (sensorManager != null) {
            sensor = sensorManager?.getDefaultSensor(Sensor.TYPE_PROXIMITY)
        } else {
            Toast.makeText(view.context, R.string.activity_call_proximity, Toast.LENGTH_SHORT).show()
        }

        presenter.getBackgroundPic()

        partnerNumber?.text = presenter.getDialerNumber()
        //partnerName.setText(presenter?.getSip().getPartnerName())

        btnDeny.setOnClickListener(this)
        btnAccept.setOnClickListener(this)
        btnMute.setOnClickListener(this)
        btnSpeaker.setOnClickListener(this)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        timer = Observable.interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ time -> callTime?.text = getTextFromTime(time) },
                        { throwable -> LogUtils.e("Timer", throwable.message, throwable) })
    }

    override fun onResume() {
        super.onResume()
        sensorManager?.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager?.unregisterListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (timer != null && timer?.isDisposed == false) {
            timer?.dispose()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.b_call_deny -> presenter.endCall()
            R.id.b_call_accept -> {
            }
            R.id.b_call_mute -> presenter.mute()
            R.id.b_call_loud -> presenter.speaker()
        }
    }

    override fun userLoaded() {
        GlideApp.with(requireContext())
                .load(presenter.user?.currentMelody?.pictureUrl)
                .transform(CircleCrop())
                .into(background)

        GlideApp.with(requireContext())
                .load(ApiService.MOCK_AVATAR)
                .transform(CircleCrop())
                .into(partnerImage)
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.values[0] == 0f) {
            turnOffScreen()
        } else {
            turnOnScreen()
        }
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {

    }

    private fun getTextFromTime(time: Long): String {
        val minutes = time / 60
        val seconds = time % 60

        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
    }

    fun turnOnScreen() {
        wakeLock = mPowerManager?.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP, "tag")
        wakeLock?.acquire()
    }

    //PowerManager.WakeLock wl = manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "Your Tag");
    @TargetApi(21)
    fun turnOffScreen() {
        wakeLock = mPowerManager?.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "tag")
        wakeLock?.acquire()
    }

    companion object {

        @JvmField
        val TAG = "CallFragment"

        @JvmStatic
        fun newInstance(): CallFragment {
            val fragment = CallFragment()
            fragment.setStyle(androidx.fragment.app.DialogFragment.STYLE_NORMAL, R.style.CallDialogTheme)
            return fragment
        }
    }
}
