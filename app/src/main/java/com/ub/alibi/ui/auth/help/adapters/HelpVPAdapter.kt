package com.ub.alibi.ui.auth.help.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

import com.ub.alibi.ui.auth.help.fragments.HelpPageFragment

class HelpVPAdapter(fm: androidx.fragment.app.FragmentManager, private val listener: HelpPageFragment.ChangeListener) : androidx.fragment.app.FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return HelpPageFragment.newInstance(position, listener)
    }

    override fun getCount(): Int = 3
}
