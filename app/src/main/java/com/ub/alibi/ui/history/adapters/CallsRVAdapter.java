package com.ub.alibi.ui.history.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ub.alibi.R;
import com.ub.alibi.di.services.database.models.CallModel;
import com.ub.alibi.ui.base.adapters.BaseAdapter;
import com.ub.alibi.utils.LogUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;

/**
 * Created by VOLixanov on 23.03.2018.
 */

public class CallsRVAdapter extends BaseAdapter<CallsRVAdapter.CallViewHolder> {

    private static final String TAG = "CallsRVAdapter";

    private List<CallModel> list = new ArrayList<>();
    private SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private CallsExistListener existListener;
    private int green, red;

    public CallsRVAdapter(Context context, CallsExistListener listener) {
        this.existListener = listener;

        green = ContextCompat.getColor(context, R.color.colorPrimary);
        red = ContextCompat.getColor(context, R.color.beauty_purple);
    }

    public void setDataSource(Observable<List<CallModel>> list, boolean isMissed) {
        list.subscribe(data -> {
            this.list.clear();

            if (isMissed) {
                for (CallModel call : data) {
                    if (call.isMissed()) {
                        this.list.add(call);
                    }
                }
            } else {
                this.list.addAll(data);
            }

            if (existListener != null) {
                existListener.onCallsReceive(!this.list.isEmpty());
            }

            notifyDataSetChanged();
        },
            throwable -> {
                LogUtils.e(TAG, throwable.getMessage(), throwable);
            });
    }

    @NonNull
    @Override
    public CallViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CallViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_call_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CallViewHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<CallModel> getList() {
        return list;
    }

    public class CallViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LinearLayout container;
        private ImageView phone;
        private ImageView options;
        private TextView phoneUser;
        private TextView name;
        private TextView time;
        private ProgressBar progressBar;

        CallViewHolder(View itemView) {
            super(itemView);

            container = itemView.findViewById(R.id.ll_call_container);
            phone = itemView.findViewById(R.id.iv_call_phone);
            options = itemView.findViewById(R.id.iv_call_options);
            phoneUser = itemView.findViewById(R.id.tv_call_member_phone);
            name = itemView.findViewById(R.id.tv_call_member_name);
            time = itemView.findViewById(R.id.tv_call_time);
            progressBar = itemView.findViewById(R.id.id_call_progressbar);

            options.setOnClickListener(this);
            container.setOnClickListener(this);
        }

        void bind() {
            CallModel model = list.get(getAdapterPosition());
            phone.setRotation(model.isIncoming() ? 180 : 0);
            phoneUser.setText(model.getSipLogin());
            name.setText(R.string.fragment_calls_history_phone);
            time.setText(formatter.format(new Date(model.getCallTime())));

            if(model.isCallingNow()) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onClick(View v) {
            if (listener != null && getAdapterPosition() != RecyclerView.NO_POSITION) {
                listener.onClick(v, getAdapterPosition());
            }
        }
    }

    public interface CallsExistListener {
        void onCallsReceive(boolean isExists);
    }
}
