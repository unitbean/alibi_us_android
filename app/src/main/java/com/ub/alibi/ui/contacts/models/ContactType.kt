package com.ub.alibi.ui.contacts.models

interface ContactType {
    fun getType() : Type
}

enum class Type {
    HEADER,
    CONTACT
}