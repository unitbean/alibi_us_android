package com.ub.alibi.ui.auth.register.presenters

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.R
import com.ub.alibi.di.services.api.ApiException
import com.ub.alibi.di.services.api.models.requests.AuthInitRequest
import com.ub.alibi.ui.auth.register.views.RegisterView
import com.ub.alibi.ui.base.presenters.BasePresenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

private const val TAG = "RegisterPresenter"

@InjectViewState
class RegisterPresenter : BasePresenter<RegisterView>() {

    fun getCode(phone: String) {
        if (phone.length < 4) {
            viewState.onShowError(R.string.activity_auth_enter_phone_number)
            return
        }

        val subscription = coreServices.apiService.api.authInit(AuthInitRequest(phone))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.onShowProgressBar(true) }
                .doOnTerminate { viewState.onShowProgressBar(false) }
                .subscribe( Consumer { viewState.onShowEnterCodeDialog(phone) },
                    object : ApiException(TAG) {
                        override fun call(message: String) {
                            viewState.onShowError(message)
                        }

                        override fun unauthorized() {
                            // unused
                        }
                    })
        subscriptions.add(subscription)
    }
}
