package com.ub.alibi.ui.auth.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ub.alibi.R
import com.ub.alibi.ui.base.fragments.BaseFragment

class SplashFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    companion object {
        const val TAG = "SplashFragment"
    }
}
