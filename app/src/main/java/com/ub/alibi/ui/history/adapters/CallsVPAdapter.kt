package com.ub.alibi.ui.history.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

import com.ub.alibi.R
import com.ub.alibi.ui.history.fragments.AllCallsFragment
import com.ub.alibi.ui.history.fragments.MissedCallsFragment
import com.ub.alibi.ui.history.presenters.HistoryPresenter
import com.ub.alibi.utils.Utils

/**
 * Created by pozharov on 07.03.2018.
 */

class CallsVPAdapter(private val context: Context?, fm: androidx.fragment.app.FragmentManager, private val presenter: HistoryPresenter) : androidx.fragment.app.FragmentStatePagerAdapter(fm), AllCallsFragment.AllCalls, MissedCallsFragment.MissedCalls {

    private lateinit var allCallsFragment: AllCallsFragment
    private lateinit var missedCallsFragment: MissedCallsFragment

    init {
        if (context != null) {
            allCallsFragment = AllCallsFragment.newInstance(this, context, presenter.observableCalls)
            missedCallsFragment = MissedCallsFragment.newInstance(this, context, presenter.observableCalls)
        }
    }

    override fun getItem(position: Int): androidx.fragment.app.Fragment? {
        context ?: return null
        when (position) {
            0 -> return allCallsFragment
            1 -> return missedCallsFragment
        }
        return null
    }

    override fun getCount(): Int = SIZE

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> Utils.getString(R.string.fragment_all_calls_title)
            1 -> Utils.getString(R.string.fragment_missed_calls_title)
            else -> ""
        }
    }

    override fun allCallDeleted(position: Int) {
        presenter.deleteCall(position)
    }

    override fun allTryCall(id: Int) {
        presenter.setCallingNowModel(id)
    }

    override fun allCallInit() {
        presenter.removeCallingNowModel()
    }

    override fun missedCallDeleted(position: Int) {
        presenter.deleteCall(position)
    }

    companion object {
        private val TAG = CallsVPAdapter::class.java.simpleName
        private const val SIZE = 2
    }
}
