package com.ub.alibi.ui.base.presenters

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable

open class CleanPresenter<View : MvpView> : MvpPresenter<View>() {

    @JvmField protected var subscriptions = CompositeDisposable()

    override fun onDestroy() {
        subscriptions.clear()
    }
}