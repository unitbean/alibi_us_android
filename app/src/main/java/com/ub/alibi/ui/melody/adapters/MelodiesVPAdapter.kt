package com.ub.alibi.ui.melody.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ub.alibi.R
import com.ub.alibi.ui.melody.fragments.FavMelodiesFragment
import com.ub.alibi.ui.melody.fragments.MelodiesFragment
import com.ub.alibi.ui.melody.models.MelodyModel
import com.ub.alibi.utils.Utils

class MelodiesVPAdapter(fm : androidx.fragment.app.FragmentManager,
                        allList: List<MelodyModel>,
                        favList: List<MelodyModel>,
                        allListener: MelodiesFragment.OnListAction,
                        favListener: FavMelodiesFragment.OnListAction) : androidx.fragment.app.FragmentPagerAdapter(fm) {

    private val allMelodies = MelodiesFragment.newInstance(allList, allListener)
    private val favMelodies = FavMelodiesFragment.newInstance(favList, favListener)

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return when (position) {
            0 -> allMelodies
            1 -> favMelodies
            else -> allMelodies
        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> Utils.getString(R.string.fragment_fav_melodies_add)
            1 -> Utils.getString(R.string.fragment_fav_melodies_title)
            else -> Utils.getString(R.string.fragment_fav_melodies_add)
        }
    }

    fun updateFragments() {
        allMelodies.updateList()
        favMelodies.updateList()
    }
}