package com.ub.alibi.ui.history.fragments

import android.content.Context
import android.os.Bundle
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.ub.alibi.R
import com.ub.alibi.di.services.database.models.CallModel
import com.ub.alibi.ui.base.adapters.BaseAdapter
import com.ub.alibi.ui.base.fragments.BaseFragment
import com.ub.alibi.ui.history.adapters.CallsRVAdapter
import com.ub.alibi.ui.history.presenters.MissedHistoryPresenter
import com.ub.alibi.ui.history.views.HistoryView
import com.ub.alibi.utils.commons.VerticalSpaceItemDecoration
import com.ub.alibi.utils.dpToPx
import com.ub.alibi.utils.gone
import com.ub.alibi.utils.visible
import com.ub.alibi.utils.webrtc.rtcutils.OnCallEventListener

import io.reactivex.Observable

/**
 * Created by pozharov on 07.03.2018.
 */

class MissedCallsFragment : BaseFragment(), HistoryView, BaseAdapter.OnItemClickListener,
        CallsRVAdapter.CallsExistListener {

    @InjectPresenter lateinit var presenter: MissedHistoryPresenter

    private lateinit var adapter: CallsRVAdapter
    private lateinit var listener: MissedCalls
    private lateinit var recycler: androidx.recyclerview.widget.RecyclerView
    private lateinit var empty: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_all_calls, container, false)

        empty = view.findViewById(R.id.tv_call_empty)

        recycler = view.findViewById(R.id.rv_all_calls)
        recycler.adapter = adapter
        recycler.addItemDecoration(VerticalSpaceItemDecoration(recycler.dpToPx(5).toInt()))

        adapter.setListener(this)

        return view
    }

    fun setDataObservable(context: Context, list: Observable<List<CallModel>>) {
        adapter = CallsRVAdapter(context, this)
        adapter.setDataSource(list, true)
    }

    override fun onClick(v: View, position: Int) {
        when (v.id) {
            R.id.iv_call_options -> {
                val popup = PopupMenu(context!!, v)
                popup.menuInflater.inflate(R.menu.call_menu, popup.menu)
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.remove -> listener.missedCallDeleted(position)
                    }

                    true
                }

                popup.show()
            }
            R.id.ll_call_container -> presenter.startCall(adapter.list[position].sipLogin)
        }
    }

    override fun onCallsReceive(isExists: Boolean) {
        if (isExists) recycler.visible else recycler.gone
        if (isExists) empty.gone else empty.visible
    }

    interface MissedCalls {
        fun missedCallDeleted(position: Int)
    }

    companion object {

        @JvmStatic
        fun newInstance(listener: MissedCalls, context: Context, list: Observable<List<CallModel>>): MissedCallsFragment {
            val fragment = MissedCallsFragment()
            fragment.setDataObservable(context, list)
            fragment.listener = listener

            return fragment
        }
    }
}
