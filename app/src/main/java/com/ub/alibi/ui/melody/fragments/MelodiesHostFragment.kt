package com.ub.alibi.ui.melody.fragments

import android.content.Context
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.ub.alibi.R
import com.ub.alibi.ui.base.fragments.BaseFragment
import com.ub.alibi.ui.main.activities.MainActivity
import com.ub.alibi.ui.main.views.MainView
import com.ub.alibi.ui.melody.adapters.MelodiesVPAdapter
import com.ub.alibi.ui.melody.presenters.HostMelodyPresenter
import com.ub.alibi.ui.melody.views.MelodiesHostView

class MelodiesHostFragment : BaseFragment(),
    MelodiesHostView,
    MelodiesFragment.OnListAction,
    FavMelodiesFragment.OnListAction {

    @InjectPresenter lateinit var presenter : HostMelodyPresenter

    private lateinit var adapter: MelodiesVPAdapter

    private lateinit var mainActivity: MainView

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is MainView) {
            mainActivity = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = MelodiesVPAdapter(childFragmentManager, presenter.melodyModels, presenter.favoriteMelodies, this, this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_host_melodies, container, false)

        val viewPager = view.findViewById<androidx.viewpager.widget.ViewPager>(R.id.vp_container)
        viewPager.adapter = adapter
        val layout = view.findViewById<TabLayout>(R.id.tl_melodies)
        layout.setupWithViewPager(viewPager)

        return view
    }

    override fun onStart() {
        super.onStart()
        setTitle(R.string.activity_melodies_title)
    }

    override fun onUpdateMelodies() {
        adapter.updateFragments()
    }

    override fun onOpenDialerFragment() {
        arguments?.putBoolean(KEY_IS_RETURN_TO_DIALER_SCREEN, false)
        (requireActivity() as MainActivity).bottomSelect(0)
    }

    /**
     * Покажет диалог с предложением позвонить другу, если он еще не был показан
     */
    override fun onShowHelpDialogAfterMelodySelecting() {
        if(this::mainActivity.isInitialized) {
            mainActivity.onShowSecondHelpDialog()
        }
    }

    override fun onMelodyFavoriteClick(position: Int) {
        presenter.onFavoriteClick(false, position)
    }

    override fun onMelodyItemClick(position: Int) {
        val isReturnToDialerScreen = arguments?.getBoolean(KEY_IS_RETURN_TO_DIALER_SCREEN) ?: false
        presenter.onMelodyClick(false, position, isReturnToDialerScreen)
    }

    override fun onMelodyPlayClick(position: Int) {
        presenter.onPlayClick(false, position)
    }

    override fun onFilterMelodies(query: String) {
        presenter.searchFromList(false, query)
    }

    override fun onFavFavoriteClick(position: Int) {
        presenter.onFavoriteClick(true, position)
    }

    override fun onFavItemClick(position: Int) {
        val isReturnToDialerScreen = arguments?.getBoolean(KEY_IS_RETURN_TO_DIALER_SCREEN) ?: false
        presenter.onMelodyClick(true, position, isReturnToDialerScreen)
    }

    override fun onFavPlayClick(position: Int) {
        presenter.onPlayClick(true, position)
    }

    override fun onFilterFav(query: String) {
        presenter.searchFromList(true, query)
    }

    fun setReturnToDialerScreenAfterSelecting(isReturn: Boolean) {
        arguments?.putBoolean(KEY_IS_RETURN_TO_DIALER_SCREEN, isReturn)
    }

    companion object {
        const val TAG = "MelodiesHostFragment"
        const val KEY_IS_RETURN_TO_DIALER_SCREEN = "KEY_IS_RETURN_TO_DIALER_SCREEN"

        @JvmStatic
        fun newInstance() : MelodiesHostFragment {
            val fragment = MelodiesHostFragment()
            fragment.arguments = Bundle()
            return fragment
        }
    }
}