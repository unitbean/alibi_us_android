package com.ub.alibi.ui.settings.views

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.ub.alibi.ui.base.views.BaseView

/**
 * Created by pozharov on 06.03.2018.
 */

@StateStrategyType(OneExecutionStateStrategy::class)
interface SettingsView : BaseView {

    fun onSetCallsHistoryEnable(isEnable: Boolean)

    fun onSetBackgroundMode(isEnable: Boolean)

    fun onSetConfirmByPasswordEnable(isEnable: Boolean)
}
