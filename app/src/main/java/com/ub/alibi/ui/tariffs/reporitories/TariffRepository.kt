package com.ub.alibi.ui.tariffs.reporitories

import com.ub.alibi.BaseApplication
import com.ub.alibi.di.services.UserService
import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.di.services.api.models.requests.LoginRequest
import com.ub.alibi.di.services.api.models.requests.TransactionRequest
import com.ub.alibi.di.services.api.models.responses.TariffResponse
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.PreferenceUtils
import io.reactivex.Observable
import retrofit2.HttpException
import javax.inject.Inject

class TariffRepository : ITariffRepository {

    @Inject lateinit var userManager: UserService
    @Inject lateinit var apiService: ApiService

    init {
        BaseApplication.appComponent.inject(this)
    }

    override fun getUser(): Observable<UserModel> {
        return userManager.userObservable
    }

    override fun getTariffs(): Observable<List<TariffResponse>> {
        return apiService.api.tariffs.map { it.result }
    }

    override fun createTransaction(model: TransactionRequest): Observable<String> {
        return apiService.api.createTransaction(model).map { it.result }
    }

    override fun cleanUser() {
        userManager.cleanUser()
    }

    /**
     * Check if exception while unauthorised then refresh token and repeat request
     * @param errors - emitted error
     * @return
     */
    override fun retryAfterRefreshToken(errors: Observable<out Throwable>): Observable<*> {
        return ApiService.retryAfterRefreshToken(apiService, errors)
    }
}