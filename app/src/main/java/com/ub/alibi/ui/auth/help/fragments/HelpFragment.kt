package com.ub.alibi.ui.auth.help.fragments

import android.content.Intent
import android.os.Bundle
import androidx.annotation.IntRange
import androidx.core.content.res.ResourcesCompat
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ub.alibi.R
import com.ub.alibi.ui.auth.help.adapters.HelpVPAdapter
import com.ub.alibi.ui.base.fragments.BaseFragment
import androidx.constraintlayout.widget.ConstraintLayout
import com.ub.alibi.ui.main.activities.MainActivity
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.PreferenceUtils


class HelpFragment : BaseFragment(), androidx.viewpager.widget.ViewPager.OnPageChangeListener, HelpPageFragment.ChangeListener {

    private lateinit var pager: androidx.viewpager.widget.ViewPager

    private lateinit var dotSelected: View
    private lateinit var dotFirst: View
    private lateinit var dotSecond: View
    private lateinit var dotThird: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_help, container, false)

        val adapter = HelpVPAdapter(childFragmentManager, this)

        pager = view.findViewById(R.id.vp_help_tabs)
        pager.adapter = adapter
        pager.addOnPageChangeListener(this)

        dotFirst = view.findViewById(R.id.v_dot_first)
        dotSecond = view.findViewById(R.id.v_dot_second)
        dotThird = view.findViewById(R.id.v_dot_third)

        dotSelected = View(context)
        dotSelected.background = ResourcesCompat.getDrawable(resources, R.drawable.shape_help_dot_selected, activity?.theme)
        dotSelected.y = dotFirst.y
        dotSelected.layoutParams = dotFirst.layoutParams

        view.findViewById<ConstraintLayout>(R.id.cl_help).addView(dotSelected)

        return view
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        moveSelection(position + positionOffset)
    }

    override fun onPageSelected(position: Int) {
    }

    override fun pageChange(position: Int) {
        if (position == 2) {
            PreferenceUtils.edit().putBoolean(PreferenceKeys.KEY_SETTINGS_USER_SEEN_HELP, true).apply()
            startActivity(Intent(context, MainActivity::class.java))
        } else {
            pager.setCurrentItem(position + 1, true)
        }
    }

    /**
     * Передвигает выделение позиции на нужную
     * @param position - новая позиция
     */
    private fun moveSelection(@IntRange(from = 0, to = 2) position: Float) {
        val distance = dotSecond.x - dotFirst.x
        dotSelected.x = dotFirst.x + position * distance
    }

    companion object {
        const val TAG = "HelpFragment"

        @JvmStatic
        fun newInstance() : HelpFragment {
            val fragment = HelpFragment()

            return fragment
        }
    }
}
