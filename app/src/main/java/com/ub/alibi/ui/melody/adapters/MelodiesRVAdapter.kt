package com.ub.alibi.ui.melody.adapters

import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView

import com.ub.alibi.R
import com.ub.alibi.ui.base.adapters.BaseAdapter
import com.ub.alibi.ui.melody.fragments.MelodiesFragment
import com.ub.alibi.ui.melody.models.MelodyModel
import com.ub.alibi.utils.GlideApp
import com.ub.alibi.utils.commons.WatcherAdapter

/**
 * Created by pozharov on 05.03.2018.
 */

class MelodiesRVAdapter : BaseAdapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private var list: List<MelodyModel>? = null
    private var actionListener: MelodiesFragment.OnListAction? = null

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            R.layout.component_search_bar
        } else {
            R.layout.rv_melody_item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        return if (viewType == R.layout.rv_melody_item) {
            MelodyViewHolder(view)
        } else {
            SearchViewHolder(view)
        }
    }

    override fun onBindViewHolder(baseHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        if (baseHolder is MelodyViewHolder) {
            baseHolder.bind(position)
        }
    }

    override fun getItemCount(): Int {
        return list?.size?.plus(1) ?: 0
    }

    fun setListener(listener: MelodiesFragment.OnListAction) {
        this.actionListener = listener
    }

    fun setData(list: List<MelodyModel>) {
        this.list = list
    }

    private inner class SearchViewHolder internal constructor(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        private val etSearch: EditText = itemView.findViewById(R.id.et_search)

        init {

            etSearch.addTextChangedListener(object : WatcherAdapter() {
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    actionListener?.onFilterMelodies(s!!.toString())
                }
            })
        }
    }

    private inner class MelodyViewHolder internal constructor(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        private val btnPlay: ImageButton = itemView.findViewById(R.id.btn_play)
        private val btnFavorite: ImageButton = itemView.findViewById(R.id.btn_favorite)
        private val ivMelodyImage: ImageView = itemView.findViewById(R.id.iv_melody_image)

        init {
            itemView.findViewById<View>(R.id.ll_melody_item).setOnClickListener(this)

            btnPlay.setOnClickListener(this)
            btnFavorite.setOnClickListener(this)
        }

        fun bind(position: Int) {
            tvTitle.text = list!![position - 1].title
            tvTitle.setTextColor(if (list!![position - 1].isCurrent)
                Color.WHITE
            else
                ContextCompat.getColor(itemView.context, R.color.deep_dark_blue))

            btnFavorite.isSelected = list!![position - 1].isFavorite
            btnPlay.isSelected = list!![position - 1].isPlaying
            GlideApp.with(itemView)
                .load(list!![position - 1].pictureUrl)
                .into(ivMelodyImage)
        }

        override fun onClick(v: View) {
            when (v.id) {
                R.id.btn_play -> actionListener?.onMelodyPlayClick(adapterPosition - 1)
                R.id.btn_favorite -> actionListener?.onMelodyFavoriteClick(adapterPosition - 1)
                R.id.ll_melody_item -> actionListener?.onMelodyItemClick(adapterPosition - 1)
            }
        }
    }
}
