package com.ub.alibi.ui.auth.auth.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.ub.alibi.BuildConfig;
import com.ub.alibi.R;
import com.ub.alibi.ui.auth.fragments.CheckFragment;
import com.ub.alibi.ui.auth.fragments.EnterPasswordFragment;
import com.ub.alibi.ui.auth.fragments.SetPasswordFragment;
import com.ub.alibi.ui.auth.help.fragments.HelpFragment;
import com.ub.alibi.ui.auth.register.fragments.RegisterFragment;
import com.ub.alibi.ui.auth.fragments.SplashFragment;
import com.ub.alibi.ui.auth.auth.presenters.AuthPresenter;
import com.ub.alibi.ui.auth.auth.views.AuthView;
import com.ub.alibi.ui.base.activities.BaseActivity;
import com.ub.alibi.ui.main.activities.MainActivity;
import com.ub.alibi.utils.LogUtils;
import com.ub.alibi.utils.PreferenceKeys;
import com.ub.alibi.utils.PreferenceUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pozharov on 01.03.2018.
 */

public class AuthActivity extends BaseActivity implements AuthView,
    CheckFragment.CheckListener,
    SetPasswordFragment.PasswordListener,
    EnterPasswordFragment.PasswordListener {

    @InjectPresenter AuthPresenter presenter;

    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        container = findViewById(R.id.fl_container);
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(container.getId(), new SplashFragment(), SplashFragment.TAG)
                .commit();
    }

    @Override
    public void onPhoneInputEnable(boolean isEnable) {
        if (isEnable) {
            getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(container.getId(), new RegisterFragment(), RegisterFragment.TAG)
                .commit();
        }
    }

    @Override
    public void onShowSetPasswordDialog() {
        getSupportFragmentManager().beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(container.getId(), SetPasswordFragment.newInstance(this), SetPasswordFragment.TAG)
            .commit();
    }

    @Override
    public void onShowEnterPasswordDialog(String password) {
        getSupportFragmentManager().beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(container.getId(), EnterPasswordFragment.newInstance(password, this), EnterPasswordFragment.TAG)
            .commit();
    }

    @Override
    public void timerUpdate(Long time) {
        Fragment password = getSupportFragmentManager().findFragmentByTag(RegisterFragment.TAG);

        if (password != null) {
            ((RegisterFragment) password).updateTimer(time);
        }
    }

    @Override
    public void onAuthSuccess() {
        if (BuildConfig.DEBUG) {
            Disposable disposable = new RxPermissions(this).request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    result -> manageAuthSuccess(),
                    throwable -> {
                        LogUtils.e("RxPerm", throwable.getMessage(), throwable);
                        manageAuthSuccess();
                    });
        } else {
            manageAuthSuccess();
        }
    }

    @Override
    public void onCodeEntered(@NonNull String code) {
        presenter.verifyCode(code);
    }

    @Override
    public void onPasswordEntered(@NonNull String password) {
        presenter.setPassword(password);
    }

    @Override
    public void onPasswordCorrect() {
        onAuthSuccess();
    }

    @Override
    public void onForgotPassword() {
        presenter.onForgotPasswordClick();
    }

    @Override
    public void onTokenExpired() {
        onPhoneInputEnable(true);
    }

    private void manageAuthSuccess() {
        if (PreferenceUtils.getPref().getBoolean(PreferenceKeys.KEY_SETTINGS_USER_SEEN_HELP, false)) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            showHelpFragement();
        }
    }

    public void onShowEnterCodeFragment() {
        presenter.startTimerCountdown();
        getSupportFragmentManager().beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(container.getId(), CheckFragment.newInstance(this), CheckFragment.TAG)
            .commit();
    }

    public void showHelpFragement() {
        getSupportFragmentManager().beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(container.getId(), HelpFragment.newInstance(), HelpFragment.TAG)
            .commit();
    }

    public void stopTimer() {
        presenter.stopTimer();
    }

    public void setPhone(String phone) {
        presenter.setPhone(phone);
    }
}
