package com.ub.alibi.ui.contacts.models

class ContactModel(val name: String, val numbers: MutableList<String>) : ContactType {

    override fun getType(): Type = Type.CONTACT

    fun isNumberNotAdded(number: String): Boolean {
        return numbers.firstOrNull { it == number } == null
    }

    override fun equals(other: Any?): Boolean {
        return when (other) {
            null -> false
            !is ContactModel -> false
            other.name == name && other.numbers == numbers -> true
            else -> false
        }
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + numbers.hashCode()
        return result
    }
}