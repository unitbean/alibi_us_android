package com.ub.alibi.ui.settings.fragments

import android.os.Bundle
import androidx.appcompat.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CompoundButton

import com.arellomobile.mvp.presenter.InjectPresenter
import com.ub.alibi.R
import com.ub.alibi.ui.base.fragments.BaseFragment
import com.ub.alibi.ui.settings.presenters.SettingsPresenter
import com.ub.alibi.ui.settings.views.SettingsView

/**
 * Created by pozharov on 06.03.2018.
 */

class SettingsFragment : BaseFragment(), SettingsView, CompoundButton.OnCheckedChangeListener {

    @InjectPresenter lateinit var presenter: SettingsPresenter

    private lateinit var sCallsHistory: SwitchCompat
    private lateinit var sBackgroundMode: SwitchCompat
    private lateinit var sConfirmByPassword: SwitchCompat

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        sCallsHistory = view.findViewById(R.id.s_calls_history)
        sBackgroundMode = view.findViewById(R.id.s_background_mode)
        sConfirmByPassword = view.findViewById(R.id.s_confirm_by_password)

        val sClearHistory = view.findViewById<Button>(R.id.b_clear_history)
        sClearHistory.setOnClickListener { presenter.clearHistory() }

        sCallsHistory.setOnCheckedChangeListener(this)
        sBackgroundMode.setOnCheckedChangeListener(this)
        sConfirmByPassword.setOnCheckedChangeListener(this)

        return view
    }

    override fun onStart() {
        super.onStart()
        setTitle(R.string.fragment_settings_title)
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        when (buttonView.id) {
            R.id.s_calls_history -> presenter.onCallsHistoryStateChanged(isChecked)
            R.id.s_background_mode -> presenter.onBackgroundModeStateChange(isChecked)
            R.id.s_confirm_by_password -> presenter.onConfirmByPasswordStateChange(isChecked)
        }
    }

    override fun onSetCallsHistoryEnable(isEnable: Boolean) {
        sCallsHistory.setOnCheckedChangeListener(null)
        sCallsHistory.isChecked = isEnable
        sCallsHistory.setOnCheckedChangeListener(this)
    }

    override fun onSetBackgroundMode(isEnable: Boolean) {
        sBackgroundMode.isChecked = isEnable
    }

    override fun onSetConfirmByPasswordEnable(isEnable: Boolean) {
        sConfirmByPassword.isChecked = isEnable
    }

    companion object {

        const val TAG = "SettingsFragment"

        @JvmStatic
        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }
}
