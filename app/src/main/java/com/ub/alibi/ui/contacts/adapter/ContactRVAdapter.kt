package com.ub.alibi.ui.contacts.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ub.alibi.R
import com.ub.alibi.ui.base.adapters.BaseAdapter
import com.ub.alibi.ui.contacts.models.ContactModel
import com.ub.alibi.ui.contacts.models.HeaderModel
import com.ub.alibi.ui.contacts.models.Type
import com.ub.alibi.ui.contacts.presenters.ContactPresenter

class ContactRVAdapter(val presenter: ContactPresenter) : BaseAdapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        return when (presenter.contacts[position].getType()) {
            Type.HEADER -> R.layout.rv_contact_header
            Type.CONTACT -> R.layout.rv_contact_item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)

        return when (viewType) {
            R.layout.rv_contact_item -> ContactVH(view)
            R.layout.rv_contact_header -> HeaderVH(view)
            else -> ContactVH(view)
        }
    }

    override fun getItemCount(): Int = presenter.contacts.size

    override fun onBindViewHolder(baseHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        when (baseHolder) {
            is ContactVH -> baseHolder.bind(position)
            is HeaderVH -> baseHolder.bind(position)
        }
    }


    inner class ContactVH(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view), View.OnClickListener {
        private val name = view.findViewById<TextView>(R.id.tv_contact_item)

        init {
            name.setOnClickListener(this)
        }

        fun bind(position : Int) {
            name.text = (presenter.contacts[position] as ContactModel).name
        }

        override fun onClick(v: View?) {
            listener?.onClick(v, adapterPosition)
        }
    }

    inner class HeaderVH(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

        private val headerLetter = view.findViewById<TextView>(R.id.tv_contact_header)

        fun bind(position : Int) {
            headerLetter.text = (presenter.contacts[position] as HeaderModel).letter
        }
    }
}