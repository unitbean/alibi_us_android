package com.ub.alibi.ui.call.presenters

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.call.views.CallView
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.utils.LogUtils

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by VOLixanov on 23.03.2018.
 */

@InjectViewState
class CallPresenter : BasePresenter<CallView>() {

    var user: UserModel? = null
        private set

    fun getBackgroundPic() {
        val subscription = coreServices.userService.userObservable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ user ->
                this.user = user
                viewState.userLoaded()
            }
            ) { throwable -> LogUtils.e(TAG, throwable.message, throwable) }
        subscriptions.add(subscription)
    }

    fun mute() {

    }

    fun speaker() {

    }

    fun endCall() {
        coreServices
                .webRtcService
                .endCall()
    }

    fun getDialerNumber() = coreServices.webRtcService.currentDialerNumber

    companion object {

        private const val TAG = "CallPresenter"
    }
}
