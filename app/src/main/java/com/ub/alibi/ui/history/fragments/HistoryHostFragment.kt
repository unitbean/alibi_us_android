package com.ub.alibi.ui.history.fragments

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ub.alibi.R
import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.ui.base.fragments.BaseFragment
import com.ub.alibi.ui.history.adapters.CallsVPAdapter
import com.ub.alibi.ui.history.presenters.HistoryPresenter
import com.ub.alibi.ui.history.views.HistoryView
import com.ub.alibi.ui.main.activities.MainActivity
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.utils.GlideApp
import com.ub.alibi.utils.Utils
import com.ub.alibi.utils.commons.OnBallanceUpdate

/**
 * Created by pozharov on 07.03.2018.
 */

class HistoryHostFragment : BaseFragment(), HistoryView,
    OnBallanceUpdate {

    @InjectPresenter lateinit var presenter: HistoryPresenter

    private lateinit var adapter: CallsVPAdapter
    private lateinit var ivAvatar: ImageView
    private lateinit var tvPhone: TextView
    private lateinit var tvBalance: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_calls_history, container, false)

        ivAvatar = view.findViewById(R.id.iv_avatar)
        tvPhone = view.findViewById(R.id.tv_phone)
        tvBalance = view.findViewById(R.id.tv_balance)

        adapter = CallsVPAdapter(context, childFragmentManager, presenter)

        val vpContainer = view.findViewById<androidx.viewpager.widget.ViewPager>(R.id.vp_container)
        vpContainer.adapter = adapter

        val tlContainer = view.findViewById<TabLayout>(R.id.tl_container)
        tlContainer.setupWithViewPager(vpContainer)

        return view
    }

    override fun onStart() {
        super.onStart()
        setTitle(R.string.fragment_calls_history_title)

        GlideApp.with(this)
            .load(ApiService.MOCK_AVATAR)
            .transform(CircleCrop())
            .into(ivAvatar)

        ivAvatar.isActivated = true

        val user = (activity as MainActivity).user
        if(user != null) {
            tvPhone.text = Utils.formatTelephoneNumber(user.sipLogin)
            tvBalance.text = Utils.getString(R.string.activity_main_balance, Utils.formatRemainingMinutes(user.remainingTime, user.remainingTime))
        }
    }

    override fun onBallanceUpdated(user: UserModel) {
        tvBalance.text = Utils.getString(R.string.activity_main_balance, Utils.formatRemainingMinutes(user.remainingTime, user.remainingTime))
    }

    companion object {

        const val TAG = "HistoryHostFragment"

        fun newInstance(): HistoryHostFragment {
            return HistoryHostFragment()
        }
    }
}
