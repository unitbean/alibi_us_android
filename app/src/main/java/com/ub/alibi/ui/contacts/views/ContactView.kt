package com.ub.alibi.ui.contacts.views

import com.ub.alibi.ui.base.views.BaseView

interface ContactView : BaseView {
    fun onContactsLoaded()
}