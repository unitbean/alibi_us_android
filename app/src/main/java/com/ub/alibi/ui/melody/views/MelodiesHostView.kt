package com.ub.alibi.ui.melody.views

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.ub.alibi.ui.base.views.BaseView

interface MelodiesHostView : BaseView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onUpdateMelodies()
    fun onShowHelpDialogAfterMelodySelecting()
    @StateStrategyType(SkipStrategy::class)
    fun onOpenDialerFragment()
}