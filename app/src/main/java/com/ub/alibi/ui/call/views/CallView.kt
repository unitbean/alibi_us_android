package com.ub.alibi.ui.call.views

import com.arellomobile.mvp.MvpView

/**
 * Created by VOLixanov on 23.03.2018.
 */

interface CallView : MvpView {
    fun userLoaded()
}
