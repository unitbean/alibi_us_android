package com.ub.alibi.ui.auth.help.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

import com.ub.alibi.R
import com.ub.alibi.ui.base.fragments.BaseFragment

class HelpPageFragment : BaseFragment() {

    private var position: Int = 0
    private lateinit var listener: ChangeListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_help_page, container, false)

        val image = view.findViewById<ImageView>(R.id.iv_help_image)
        val header = view.findViewById<TextView>(R.id.tv_help_header)
        val message = view.findViewById<TextView>(R.id.tv_help_message)
        val btnNext = view.findViewById<Button>(R.id.btn_help_next)
        btnNext.setOnClickListener { listener.pageChange(position) }

        val imageId : Int
        val headerText : Int
        val messageText : Int
        val nextText : Int

        when (position) {
            0 -> {
                imageId = R.drawable.ic_help_cassette
                headerText = R.string.fragment_help_first_title
                messageText = R.string.fragment_help_first_message
                nextText = R.string.fragment_help_next
            }
            1 -> {
                imageId = R.drawable.ic_help_smartphone
                headerText = R.string.fragment_help_second_title
                messageText = R.string.fragment_help_second_message
                nextText = R.string.fragment_help_next
            }
            2 -> {
                imageId = R.drawable.ic_help_restaurant
                headerText = R.string.fragment_help_third_title
                messageText = R.string.fragment_help_third_message
                nextText = R.string.fragment_help_last_next
            }
            else -> {
                imageId = R.drawable.ic_help_cassette
                headerText = R.string.fragment_help_first_title
                messageText = R.string.fragment_help_first_message
                nextText = R.string.fragment_help_next
            }
        }

        header.setText(headerText)
        message.setText(messageText)
        btnNext.setText(nextText)

        image.setImageResource(imageId)

        return view
    }

    interface ChangeListener {
        fun pageChange(position : Int)
    }

    companion object {

        @JvmStatic
        fun newInstance(position: Int, listener: ChangeListener): HelpPageFragment {
            val fragment = HelpPageFragment()
            fragment.position = position
            fragment.listener = listener

            return fragment
        }
    }
}
