package com.ub.alibi.ui.base.presenters

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.ub.alibi.BaseApplication
import com.ub.alibi.di.services.CoreServices
import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.di.services.api.models.requests.LoginRequest
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.PreferenceUtils

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException

/**
 * Created by pozharov on 01.03.2018.
 */

open class BasePresenter<View : MvpView> : MvpPresenter<View>() {

    @JvmField protected var coreServices = CoreServices()
    @JvmField protected var subscriptions = CompositeDisposable()

    init {

        BaseApplication.appComponent.inject(coreServices)
    }

    override fun onDestroy() {
        subscriptions.clear()
    }

    /**
     * Check if exception while unauthorised then refresh token and repeat request
     * @param errors - emitted error
     * @return
     */
    protected fun retryAfterRefreshToken(errors: Observable<out Throwable>): Observable<*> {
        return ApiService.retryAfterRefreshToken(coreServices.apiService, errors)
    }
}
