package com.ub.alibi.ui.tariffs.reporitories

import com.ub.alibi.di.services.api.models.requests.TransactionRequest
import com.ub.alibi.di.services.api.models.responses.TariffResponse
import com.ub.alibi.ui.base.repositories.IBaseApiRepository
import com.ub.alibi.ui.main.models.UserModel
import io.reactivex.Observable

interface ITariffRepository : IBaseApiRepository {

    fun getUser(): Observable<UserModel>
    fun getTariffs(): Observable<List<TariffResponse>>
    fun createTransaction(model: TransactionRequest): Observable<String>
    fun cleanUser()
}