package com.ub.alibi.ui.main.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.IntRange;
import androidx.annotation.Nullable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.ub.alibi.R;
import com.ub.alibi.ui.base.activities.BaseActivity;
import com.ub.alibi.ui.call.fragments.CallFragment;
import com.ub.alibi.ui.contacts.fragments.ContactFragment;
import com.ub.alibi.ui.history.fragments.HistoryHostFragment;
import com.ub.alibi.ui.dialer.fragments.DialerFragment;
import com.ub.alibi.ui.main.models.UserModel;
import com.ub.alibi.ui.main.presenters.MainPresenter;
import com.ub.alibi.ui.main.views.MainView;
import com.ub.alibi.ui.melody.fragments.MelodiesHostFragment;
import com.ub.alibi.ui.settings.fragments.SettingsFragment;
import com.ub.alibi.ui.tariffs.activities.TariffActivity;
import com.ub.alibi.utils.LogUtils;
import com.ub.alibi.utils.commons.OnBallanceUpdate;
import com.ub.alibi.utils.commons.help.HelpDialog;
import com.ub.alibi.utils.commons.help.HelpKeeper;
import com.ub.alibi.utils.webrtc.rtcutils.OnCallEventListener;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.ub.alibi.utils.notifications.NotificationPublisher.CALL_IS_ANSWER;
import static com.ub.alibi.utils.notifications.NotificationPublisher.FROM_NOTIFICATION;
import static com.ub.alibi.utils.notifications.NotificationPublisher.NOTIFICATION_ID;

/**
 * Created by pozharov on 02.03.2018.
 */

public class MainActivity extends BaseActivity implements
        MainView,
        CompoundButton.OnCheckedChangeListener,
        View.OnClickListener,
        OnCallEventListener {

    private static final int REQUEST_CODE_TARIFFS = 101;

    @InjectPresenter MainPresenter presenter;

    @ProvidePresenter
    MainPresenter providePresenter() {
        return new MainPresenter(this);
    }

    private SwitchCompat sAlibiMode;
    private DrawerLayout dlMain;

    private String curFragmentTAG;
    private FragmentTransaction curTransaction;
    private CallFragment callFragment;
    private BottomNavigationView bnMenu;

    private boolean isReturnToDialerFragmentAfterMelody = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bnMenu = findViewById(R.id.bn_menu);
        sAlibiMode = findViewById(R.id.s_alibi_mode);
        dlMain = findViewById(R.id.dl_main);

        findViewById(R.id.ll_dialer).setOnClickListener(this);
        findViewById(R.id.ll_melodies).setOnClickListener(this);
        findViewById(R.id.ll_calls_history).setOnClickListener(this);
        findViewById(R.id.ll_contacts).setOnClickListener(this);
        findViewById(R.id.ll_tariffs).setOnClickListener(this);
        findViewById(R.id.ll_settings).setOnClickListener(this);

        sAlibiMode.setOnCheckedChangeListener(this);

        bnMenu.setOnNavigationItemSelectedListener((menuItem) -> {
            switch (menuItem.getItemId()) {
                case R.id.menu_dialer:
                    presenter.getViewState().onOpenDialerScreen(null);
                    return true;
                case R.id.menu_contacts:
                    requestPermissionAndOpenContacts();
                    return true;
                case R.id.menu_history:
                    presenter.getViewState().onOpenCallsHistoryScreen();
                    return true;
                case R.id.menu_melody:
                    presenter.getViewState().onOpenMelodiesScreen();
                    return true;
                case R.id.menu_settings:
                    presenter.getViewState().onOpenSettingScreen();
                    return true;
                default:
                    return false;
            }
        });

        bottomSelect(0);

        presenter.setListener(this);

        handleIncomingIntent(getIntent());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.removeListener(this);
    }

    /**
     * Показываем диалог с предложением выбрать мелодию в том случае, если он еще не был показан.
     */
    @Override
    public void onShowFirstHelpDialog() {
        if (HelpKeeper.Companion.isFirstDialogNotShowed()) {
            HelpKeeper.Companion.setFirstStepDone();
            HelpDialog helpDialog = HelpKeeper.Companion.getFirstStep();
            helpDialog.setListener(() -> bottomSelect(3));
            helpDialog.show(getSupportFragmentManager(), HelpDialog.class.getSimpleName());
        }
    }

    /**
     * Показываем диалог с предложением позвонить другу в случае если первый диалог был показан
     */
    @Override
    public void onShowSecondHelpDialog() {
        if (HelpKeeper.Companion.isSecondDialogNotShowed()) {
            HelpKeeper.Companion.setSecondStepDone();

            HelpDialog helpDialog = HelpKeeper.Companion.getSecondStep();
            helpDialog.setListener(() -> bottomSelect(0));
            helpDialog.show(getSupportFragmentManager(), HelpDialog.class.getSimpleName());
        }
    }

    /**
     * Показываем диалог с предложением купить 3 минуты в случае если второй диалог был показан
     */
    @Override
    public void onShowThirdHelpDialog() {
        if (HelpKeeper.Companion.isThirdDialogNotShowed()) {
            HelpKeeper.Companion.setThirdStepDone();
            HelpDialog helpDialog = HelpKeeper.Companion.getThirdStep();
            helpDialog.setListener(() -> {
                startActivityForResult(new Intent(this, TariffActivity.class), REQUEST_CODE_TARIFFS);
            });
            helpDialog.show(getSupportFragmentManager(), HelpDialog.class.getSimpleName());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.loadUserInfo();
    }

    @Override
    public void onShowUserInfo(UserModel userModel) {
        sAlibiMode.setOnCheckedChangeListener(null);
        sAlibiMode.setChecked(userModel.isAlibiMode());
        sAlibiMode.setOnCheckedChangeListener(this);

        List<Fragment> fragments = getSupportFragmentManager().getFragments();

        for (Fragment fragment : fragments) {
            if (fragment instanceof OnBallanceUpdate && fragment.isAdded()) {
                ((OnBallanceUpdate) fragment).onBallanceUpdated(userModel);
            }
        }

        onShowFirstHelpDialog();

        Disposable check = new RxPermissions(this).request(Manifest.permission.CALL_PHONE, Manifest.permission.RECORD_AUDIO)
            .subscribe(result -> { if (result) {
                /*String ussd = Uri.encode("*#") + "21" + Uri.encode("#");
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));*/
            } else {
                    new AlertDialog.Builder(this)
                        .setOnDismissListener((dialog -> this.finish()))
                        .setMessage(R.string.call_and_microphone_requires)
                        .setPositiveButton(android.R.string.ok, ((dialog, which) -> {}))
                        .show();
            }
            },
                throwable -> LogUtils.e("RxPerm", "Call permission denied"));
    }

    @Override
    public void onShowChangingAlibiModeProgressBar(boolean isShow) {
        sAlibiMode.setClickable(!isShow);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIncomingIntent(intent);
    }

    @Override
    public void onOpenDialerScreen(String phoneNumber) {
        DialerFragment dialerFragment = (DialerFragment) getSupportFragmentManager().findFragmentByTag(DialerFragment.TAG);

        curTransaction = getSupportFragmentManager().beginTransaction();

        detachCurFragment();

        if(dialerFragment == null) {
            dialerFragment = DialerFragment.newInstance();
            curTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.fl_container, dialerFragment, DialerFragment.TAG);
        } else {
            curTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .attach(dialerFragment);
            if(phoneNumber != null){
                dialerFragment.updateDialerInput(phoneNumber);
            }
        }
        curTransaction.commit();
        curFragmentTAG = DialerFragment.TAG;
    }

    @Override
    public void onOpenCallsHistoryScreen() {
        Fragment callsHistoryFragment = getSupportFragmentManager().findFragmentByTag(HistoryHostFragment.TAG);

        curTransaction = getSupportFragmentManager().beginTransaction();
        
        detachCurFragment();

        if(callsHistoryFragment == null) {
            callsHistoryFragment = HistoryHostFragment.Companion.newInstance();
            curTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.fl_container, callsHistoryFragment, HistoryHostFragment.TAG);
        } else {
            curTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .attach(callsHistoryFragment);
        }
        curTransaction.commit();
        curFragmentTAG = HistoryHostFragment.TAG;
    }

    @Override
    public void onOpenMelodiesScreen() {
        Fragment favMelodiesFragment = getSupportFragmentManager().findFragmentByTag(MelodiesHostFragment.TAG);

        curTransaction = getSupportFragmentManager().beginTransaction();

        detachCurFragment();

        if(favMelodiesFragment == null) {
            favMelodiesFragment = MelodiesHostFragment.newInstance();
            curTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.fl_container, favMelodiesFragment, MelodiesHostFragment.TAG);
        } else {
            curTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .attach(favMelodiesFragment);
        }

        if (favMelodiesFragment instanceof MelodiesHostFragment) {
            ((MelodiesHostFragment) favMelodiesFragment)
                    .setReturnToDialerScreenAfterSelecting(isReturnToDialerFragmentAfterMelody);
            isReturnToDialerFragmentAfterMelody = false;
        }

        curTransaction.commit();
        curFragmentTAG = MelodiesHostFragment.TAG;
    }

    @Override
    public void onOpenSettingScreen() {
        Fragment settingsFragment = getSupportFragmentManager().findFragmentByTag(SettingsFragment.TAG);

        curTransaction = getSupportFragmentManager().beginTransaction();

        detachCurFragment();

        if(settingsFragment == null) {
            settingsFragment = SettingsFragment.newInstance();
            curTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.fl_container, settingsFragment, SettingsFragment.TAG);
        } else {
            curTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .attach(settingsFragment);
        }
        curTransaction.commit();
        curFragmentTAG = SettingsFragment.TAG;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_TARIFFS && resultCode == RESULT_OK) {
            presenter.updateUserInfo();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            sAlibiMode.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            sAlibiMode.setTextColor(getResources().getColor(R.color.beauty_purple));
        }

        Disposable check = new RxPermissions(this).request(Manifest.permission.CALL_PHONE)
            .subscribe(result -> {
                if (!result) {
                    sAlibiMode.setOnCheckedChangeListener(null);
                    sAlibiMode.setChecked(false);
                    sAlibiMode.setTextColor(getResources().getColor(R.color.beauty_purple));
                    sAlibiMode.setOnCheckedChangeListener(this);
                }

                String ussd;
                if (isChecked) {
                    ussd = "**21*+7" + "4995770348" + Uri.encode("#");
                } else {
                    ussd = Uri.encode("##") + "21" + Uri.encode("#");
                }

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
                presenter.onAlibiModeEnable(isChecked);
                },
                throwable -> LogUtils.e("RxPerm", throwable.getMessage(), throwable));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_dialer:
                bottomSelect(0);
                dlMain.closeDrawers();
                break;
            case R.id.ll_melodies:
                bottomSelect(3);
                dlMain.closeDrawers();
                break;
            case R.id.ll_calls_history:
                bottomSelect(2);
                dlMain.closeDrawers();
                break;
            case R.id.ll_contacts:
                bottomSelect(1);
                dlMain.closeDrawers();
                break;
            case R.id.ll_tariffs:
                startActivityForResult(new Intent(this, TariffActivity.class), REQUEST_CODE_TARIFFS);
                dlMain.closeDrawers();
                break;
            case R.id.ll_settings:
                bottomSelect(4);
                dlMain.closeDrawers();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentByTag(CallFragment.TAG) == null) {
            super.onBackPressed();
        }
    }

    private void detachCurFragment(){
        Fragment curFragment = getSupportFragmentManager().findFragmentByTag(curFragmentTAG);
        if(curFragment != null && !curFragment.isDetached()) {
            curTransaction.detach(curFragment);
        }
    }

    /**
     * Закрытие диалога звонилки и обнуление ресурсов, с ней связанных
     */
    private void closeCaller() {
        if (callFragment != null && callFragment.isAdded()) {
            callFragment.dismiss();
            callFragment = null;
        }
    }

    /**
     * Берет интент и проверяет его на наличие флагов звонилки в нем
     * @param intent
     */
    private void handleIncomingIntent(Intent intent) {
        if (intent.getExtras() != null) {
            if (intent.getExtras().containsKey(CALL_IS_ANSWER)) {
                presenter.acceptCall(this, intent.getIntExtra(NOTIFICATION_ID, 0), intent.getBooleanExtra(CALL_IS_ANSWER, false));
            } else if (intent.getExtras().containsKey(FROM_NOTIFICATION)) {
                presenter.acceptCall(this, intent.getIntExtra(NOTIFICATION_ID, 0), true); // предполагаем, что пока это принятие вызова
            }
        }
    }

    /**
     * Запрос разрешения на контакты и если оно получено, открыть выбор контактов
     */
    private void requestPermissionAndOpenContacts() {
        Disposable check = new RxPermissions(this).request(Manifest.permission.READ_CONTACTS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(result -> {
                    if (result) {
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag(ContactFragment.TAG);

                        curTransaction = getSupportFragmentManager().beginTransaction();

                        detachCurFragment();

                        if(fragment == null) {
                            fragment = ContactFragment.newInstance();
                            curTransaction
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .add(R.id.fl_container, fragment, ContactFragment.TAG);
                        } else {
                            curTransaction
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .attach(fragment);
                        }
                        curTransaction.commit();
                        curFragmentTAG = ContactFragment.TAG;
                    } else {
                        bottomSelect(0);
                    }
                },
                throwable -> LogUtils.e("RxPerm", throwable.getMessage(), throwable));
    }

    public void openMelodyFragment() {
        isReturnToDialerFragmentAfterMelody = true;
        bottomSelect(3);
    }

    public void bottomSelect(@IntRange(from = 0, to = 4) int position) {
        int resId;
        switch (position) {
            case 0:
                resId = R.id.menu_dialer;
                break;
            case 1:
                resId = R.id.menu_contacts;
                break;
            case 2:
                resId = R.id.menu_history;
                break;
            case 3:
                resId = R.id.menu_melody;
                break;
            case 4:
                resId = R.id.menu_settings;
                break;
            default:
                resId = R.id.menu_dialer;
        }

        bnMenu.setSelectedItemId(resId);
    }

    //User может быть null, если отсутствует интернет соединение
    public @Nullable UserModel getUser() {
        return presenter.getUser();
    }

    /**
     * Открывает экран начала звонка
     */
    @Override
    public void onCallStarted() {
        if (callFragment == null) {
            callFragment = CallFragment.newInstance();
            callFragment.show(getSupportFragmentManager(), CallFragment.TAG);
        }
    }

    @Override
    public void onCallCompleted(long callTime) {
        closeCaller();
        presenter.removeTheMinutes(callTime);

        onShowThirdHelpDialog();
    }

    @Override
    public void onCallError(int strId) {
        onShowError(strId);
    }
}