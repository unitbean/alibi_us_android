package com.ub.alibi.ui.auth.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import com.ub.alibi.R
import com.ub.alibi.ui.auth.auth.fragments.BaseAuthFragment

class CheckFragment : BaseAuthFragment() {

    private lateinit var listener: CheckListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_check, container, false)

        val back = view.findViewById<Button>(R.id.btn_back)
        back.setOnClickListener { hostActivity.onPhoneInputEnable(true) }

        val smsField = view.findViewById<EditText>(R.id.et_sms_code)

        val check = view.findViewById<Button>(R.id.btn_check_code)
        check.setOnClickListener {
            if (smsField.text.toString().trim().isEmpty()) {
                smsField.error = getString(R.string.activity_auth_check_error_lenght)
            } else {
                smsField.error = null
                listener.onCodeEntered(smsField.text.toString())
            }
        }

        return view
    }

    interface CheckListener {
        fun onCodeEntered(code: String)
    }

    companion object {

        const val TAG = "CheckFragment"

        @JvmStatic
        fun newInstance(listener : CheckListener) : CheckFragment {
            val fragment = CheckFragment()
            fragment.listener = listener

            return fragment
        }
    }
}
