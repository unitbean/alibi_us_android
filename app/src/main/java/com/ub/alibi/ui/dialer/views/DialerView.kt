package com.ub.alibi.ui.dialer.views

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.ub.alibi.ui.base.views.BaseView

/**
 * Created by pozharov on 02.03.2018.
 */

@StateStrategyType(OneExecutionStateStrategy::class)
interface DialerView : BaseView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onSetDialerInput(input: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onSetAlibiMelody(melody: String?)

    fun onStartCall(dialerInput: String)
}
