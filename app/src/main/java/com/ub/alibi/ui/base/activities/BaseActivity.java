package com.ub.alibi.ui.base.activities;

import android.os.Bundle;
import androidx.annotation.StringRes;

import com.flurry.android.FlurryAgent;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.ub.alibi.R;
import com.ub.alibi.utils.LogUtils;
import com.ub.alibi.utils.commons.ProgressDialog;
import com.ub.utils.base.MvpXActivity;

/**
 * Created by pozharov on 01.03.2018.
 */

public abstract class BaseActivity extends MvpXActivity {

    private ProgressDialog progressDialog;
    private AlertDialog errorDialog;
    private boolean isShownProgressDialog;
    private NavigationView navigationView;
    private DrawerLayout dlMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog();
        overridePendingTransition(R.anim.transition_in, R.anim.transition_out);

        FlurryAgent.logEvent(getClass().getSimpleName());
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        Toolbar mToolbar = findViewById(R.id.toolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        navigationView = findViewById(R.id.nv_menu);
        dlMain = findViewById(R.id.dl_main);

        if(navigationView != null && dlMain != null && mToolbar != null){
            ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                    this,
                dlMain,
                    mToolbar,
                    R.string.navigation_menu_open,
                    R.string.navigation_menu_close
            ){
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    invalidateOptionsMenu();
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    invalidateOptionsMenu();
                }
            };
            dlMain.addDrawerListener(drawerToggle);
            drawerToggle.syncState();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.cancelErrorDialog();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.transition_in, R.anim.transition_out);
    }

    public void onShowProgressBar(boolean isShow) {
        if(isShow){
            if(!progressDialog.isAdded() && !isShownProgressDialog) {
                progressDialog.show(getSupportFragmentManager(), ProgressDialog.TAG);
                isShownProgressDialog = true;
            }
        } else {
            if(isShownProgressDialog){
                progressDialog.dismiss();
                isShownProgressDialog = false;
            }
        }
    }

    public void onShowError(String message) {
        if (errorDialog == null || !errorDialog.isShowing()) {
            errorDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme_DefaultAlertDialogTheme))
                    .setMessage(message)
                    .setPositiveButton(R.string.common_ok, null)
                    .create();
        }

        if (!errorDialog.isShowing()) {
            errorDialog.show();
        }
    }


    public void onShowError(@StringRes int resId) {
        if (errorDialog == null || !errorDialog.isShowing()) {
            errorDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme_DefaultAlertDialogTheme))
                    .setMessage(resId)
                    .setPositiveButton(R.string.common_ok, null)
                    .create();
        }

        if (!errorDialog.isShowing()) {
            errorDialog.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (dlMain != null && dlMain.isDrawerOpen(Gravity.START)) {
            dlMain.closeDrawers();
            return;
        }

        super.onBackPressed();
    }

    /**
     * Display arrow back button in toolbar
     */
    public void showBackArrowButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Cancel error dialog
     */
    protected void cancelErrorDialog() {
        if (errorDialog != null && errorDialog.isShowing()) {
            errorDialog.cancel();
        }
    }

    /**
     * Set toolbar elevation
     */
    protected void setToolbarElevation() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
        }
    }

    /**
     * Set toolbar subtitle
     */
    public void setSubtitle(String subtitle) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(subtitle);
        }
    }

    /**
     * Show toolbar title
     */
    protected void showTitle(boolean isShow) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(isShow);
        }
    }
}
