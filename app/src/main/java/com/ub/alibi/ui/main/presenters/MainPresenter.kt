package com.ub.alibi.ui.main.presenters

import android.content.Context

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.di.services.api.models.requests.ChangeAlibiModeRequest
import com.ub.alibi.di.services.api.models.requests.TransactionRequest
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.ui.main.views.MainView
import com.ub.alibi.utils.LogUtils
import com.ub.alibi.utils.webrtc.rtcutils.OnCallEventListener
import io.reactivex.Observable

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.withTimeout
import java.util.concurrent.TimeUnit

/**
 * Created by pozharov on 02.03.2018.
 */

@InjectViewState
class MainPresenter(val onCallEventListener: OnCallEventListener) : BasePresenter<MainView>() {

    var user: UserModel? = null
        private set

    init {
        coreServices.webRtcService
                .addCallEventListener(onCallEventListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        coreServices.webRtcService
                .removeCallEventListener(onCallEventListener)
    }

    fun loadUserInfo() {
        val subscription = coreServices
                .userService
                .userObservable
                .doOnSubscribe { disposable -> viewState.onShowProgressBar(true) }
                .doAfterTerminate { viewState.onShowProgressBar(false) }
                .subscribe({ userModel ->
                    this.user = userModel
                    viewState.onShowUserInfo(userModel)
                }, { throwable -> viewState.onShowError(throwable.message) })
        subscriptions.add(subscription)
    }

    fun onAlibiModeEnable(isEnable: Boolean) {
        val subscription = coreServices
                .apiService
                .api
                .changeAlibiMode(ChangeAlibiModeRequest(isEnable))
                .retryWhen{ this.retryAfterRefreshToken(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { disposable -> viewState.onShowChangingAlibiModeProgressBar(true) }
                .doFinally { viewState.onShowChangingAlibiModeProgressBar(false) }
                .doOnError { throwable -> viewState.onShowError(throwable.message) }
                .subscribe({ objectBaseResponse -> user!!.isAlibiMode = isEnable },
                        { throwable -> LogUtils.e(TAG, throwable.message, throwable) })
        subscriptions.add(subscription)
    }

    /**
     * Принятие вызова. Пока не используется
     */
    fun acceptCall(context: Context, id: Int, isAccept: Boolean) {
        /*val subscription = userObservable
                .subscribe({ userModel ->
                    if (isAccept) {
                        NotificationPublisher.removeNotification(context, id)
                    } else {
                        NotificationPublisher.removeNotification(context, id)
                    }
                }, { throwable -> LogUtils.e(TAG, throwable.message, throwable) })
        subscriptions.add(subscription)*/
    }

    fun removeTheMinutes(callTime: Long) {
        coreServices.userService.cleanUser()
        val subscription = Observable.timer(1, TimeUnit.SECONDS)
                .subscribe {
                    loadUserInfo()
                }
        subscriptions.add(subscription)
    }

    fun updateUserInfo() {
        viewState.onShowUserInfo(user)
    }

    fun setListener(listener: OnCallEventListener) {
        coreServices.webRtcService.addCallEventListener(listener)
    }

    fun removeListener(listener: OnCallEventListener) {
        coreServices.webRtcService.removeCallEventListener(listener)
    }

    companion object {

        private val TAG = MainPresenter::class.java.simpleName
    }

}
