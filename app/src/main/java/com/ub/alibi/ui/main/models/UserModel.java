package com.ub.alibi.ui.main.models;

import com.ub.alibi.di.services.api.models.responses.UserInfoResponse;
import com.ub.alibi.ui.melody.models.MelodyModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pozharov on 02.03.2018.
 */

public class UserModel {

    private String id;
    private MelodyModel currentMelody;
    private List<Object> favoriteMelodies = new ArrayList<>();
    private long remainingTime;
    private boolean alibiMode;
    private String sipLogin;
    private String sipPassword;

    public UserModel(UserInfoResponse userInfoResponse){
        this.id = userInfoResponse.getId();
        this.favoriteMelodies = userInfoResponse.getFavoriteMelodies();
        this.remainingTime = userInfoResponse.getMillisec();
        this.alibiMode = userInfoResponse.isAlibiMode();
        this.sipLogin = userInfoResponse.getSipLogin();
        this.sipPassword = userInfoResponse.getSipPassword();
    }

    public String getId() {
        return id;
    }

    public List<Object> getFavoriteMelodies() {
        return favoriteMelodies;
    }

    public long getRemainingTime() {
        return remainingTime;
    }

    public boolean isAlibiMode() {
        return alibiMode;
    }

    public String getSipLogin() {
        return sipLogin;
    }

    public String getSipPassword() {
        return sipPassword;
    }

    public MelodyModel getCurrentMelody() {
        return currentMelody;
    }

    public void setCurrentMelody(MelodyModel currentMelody) {
        this.currentMelody = currentMelody;
    }

    public void setFavoriteMelodies(List<Object> favoriteMelodies) {
        this.favoriteMelodies = favoriteMelodies;
    }

    public void setAlibiMode(boolean alibiMode) {
        this.alibiMode = alibiMode;
    }
}
