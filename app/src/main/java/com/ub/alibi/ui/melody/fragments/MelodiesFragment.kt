package com.ub.alibi.ui.melody.fragments

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ub.alibi.R
import com.ub.alibi.ui.base.fragments.BaseFragment
import com.ub.alibi.ui.melody.adapters.MelodiesRVAdapter
import com.ub.alibi.ui.melody.models.MelodyModel
import com.ub.alibi.utils.commons.MelodiesItemDecoration
import com.ub.alibi.utils.dpToPx

class MelodiesFragment : BaseFragment() {

    private val adapter = MelodiesRVAdapter()
    private lateinit var listener: OnListAction

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_melodies, container, false)

        val recycler = view.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.rv_melodies)
        recycler.adapter = adapter
        recycler.addItemDecoration(MelodiesItemDecoration(recycler.dpToPx(11).toInt(), recycler.dpToPx( 20).toInt()))

        return view
    }

    fun updateList() {
        adapter.notifyDataSetChanged()
    }

    interface OnListAction {
        fun onMelodyFavoriteClick(position: Int)
        fun onMelodyItemClick(position: Int)
        fun onMelodyPlayClick(position: Int)
        fun onFilterMelodies(query: String)
    }

    companion object {
        @JvmStatic
        fun newInstance(allList: List<MelodyModel>, listener: OnListAction) : MelodiesFragment {
            val fragment = MelodiesFragment()
            fragment.adapter.setData(allList)
            fragment.listener = listener
            fragment.adapter.setListener(listener)

            return fragment
        }
    }
}