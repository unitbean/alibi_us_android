package com.ub.alibi.ui.melody.fragments

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ub.alibi.R
import com.ub.alibi.ui.base.fragments.BaseFragment
import com.ub.alibi.ui.melody.adapters.FavMelodiesRVAdapter
import com.ub.alibi.ui.melody.models.MelodyModel
import com.ub.alibi.utils.commons.MelodiesItemDecoration
import com.ub.alibi.utils.dpToPx

class FavMelodiesFragment : BaseFragment() {

    private val adapter = FavMelodiesRVAdapter()
    private lateinit var listener: OnListAction

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_fav_melodies, container, false)

        val recycler = view.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.rv_melodies)
        recycler.adapter = adapter
        recycler.addItemDecoration(MelodiesItemDecoration(recycler.dpToPx(11).toInt(), recycler.dpToPx( 20).toInt()))

        return view
    }

    fun updateList() {
        adapter.notifyDataSetChanged()
    }

    interface OnListAction {
        fun onFavFavoriteClick(position: Int)
        fun onFavItemClick(position: Int)
        fun onFavPlayClick(position: Int)
        fun onFilterFav(query: String)
    }

    companion object {
        @JvmStatic
        fun newInstance(list: List<MelodyModel>, listener: OnListAction): FavMelodiesFragment {
            val fragment = FavMelodiesFragment()
            fragment.adapter.setData(list)
            fragment.listener = listener
            fragment.adapter.setListener(listener)

            return fragment
        }
    }
}
