package com.ub.alibi.ui.tariffs.models

/**
 * Created by pozharov on 07.03.2018.
 */

class TariffModel {

    var id: String? = null
    var title: String? = null
    var price: String? = null
    var millis: Long? = null
    var billingId: String? = null
}
