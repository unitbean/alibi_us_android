package com.ub.alibi.ui.auth.register.fragments

import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

import com.arellomobile.mvp.presenter.InjectPresenter
import com.ub.alibi.R
import com.ub.alibi.ui.auth.auth.activities.AuthActivity
import com.ub.alibi.ui.auth.auth.fragments.BaseAuthFragment
import com.ub.alibi.ui.auth.register.presenters.RegisterPresenter
import com.ub.alibi.ui.auth.register.views.RegisterView
import com.ub.utils.gone
import com.ub.utils.visible
import java.text.DecimalFormat

class RegisterFragment : BaseAuthFragment(), RegisterView, View.OnClickListener {

    @InjectPresenter lateinit var presenter: RegisterPresenter

    private lateinit var etPhone: EditText
    private lateinit var tvTimer: TextView
    private lateinit var btnGetCode: Button
    private val formatter = DecimalFormat("#0")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_register, container, false)

        etPhone = view.findViewById(R.id.et_phone)
        etPhone.addTextChangedListener(PhoneNumberFormattingTextWatcher())
        tvTimer = view.findViewById(R.id.tv_register_timer)
        btnGetCode = view.findViewById(R.id.btn_get_code)
        btnGetCode.setOnClickListener(this)
        return view
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_get_code -> presenter.getCode(etPhone.text.toString())
        }
    }

    override fun onShowError(message: String?) {
        etPhone.error = message
    }

    override fun onShowError(resId: Int) {
        etPhone.error = getString(resId)
    }

    override fun onShowEnterCodeDialog(phone: String) {
        hostActivity.setPhone(phone)
        hostActivity.onShowEnterCodeFragment()
    }

    fun updateTimer(time: Long) {
        if (time <= 60) {
            tvTimer.visible
            tvTimer.text = String.format(getString(R.string.activity_auth_register_timeout_text), formatter.format(60 - time))
            btnGetCode.isEnabled = false
        } else {
            (requireActivity() as AuthActivity).stopTimer()
            btnGetCode.isEnabled = true
            tvTimer.gone
        }
    }

    companion object {
        const val TAG = "RegisterFragment"
    }
}
