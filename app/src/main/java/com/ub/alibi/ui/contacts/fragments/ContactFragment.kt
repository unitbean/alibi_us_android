package com.ub.alibi.ui.contacts.fragments

import android.os.Bundle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ub.alibi.R
import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.ui.base.adapters.BaseAdapter
import com.ub.alibi.ui.base.fragments.BaseFragment
import com.ub.alibi.ui.contacts.adapter.ContactRVAdapter
import com.ub.alibi.ui.contacts.adapter.DiffContactUtil
import com.ub.alibi.ui.contacts.models.ContactModel
import com.ub.alibi.ui.contacts.presenters.ContactPresenter
import com.ub.alibi.ui.contacts.views.ContactView
import com.ub.alibi.ui.main.activities.MainActivity
import com.ub.alibi.utils.GlideApp
import com.ub.alibi.utils.Utils
import com.ub.alibi.utils.commons.OnBallanceUpdate
import com.ub.alibi.utils.commons.numbersDialog.NumbersDialog

class ContactFragment : BaseFragment(),
    ContactView,
    androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener,
    BaseAdapter.OnItemClickListener {

    @InjectPresenter lateinit var presenter : ContactPresenter

    private lateinit var adapter : ContactRVAdapter
    private lateinit var diffUtil : DiffContactUtil
    private lateinit var updater : androidx.swiperefreshlayout.widget.SwipeRefreshLayout
    private lateinit var queryField : EditText

    private lateinit var ivAvatar: ImageView
    private lateinit var tvPhone: TextView
    private lateinit var tvBalance: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contacts, container, false)

        diffUtil = DiffContactUtil(presenter.contacts, presenter.oldContacts)

        ivAvatar = view.findViewById(R.id.iv_avatar)
        tvPhone = view.findViewById(R.id.tv_phone)
        tvBalance = view.findViewById(R.id.tv_balance)

        queryField = view.findViewById(R.id.et_search)
        queryField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s : Editable) {
                presenter.loadContacts(s.toString(), context!!)
            }

            override fun beforeTextChanged(s : CharSequence, start : Int, count : Int, after : Int) {
            }

            override fun onTextChanged(s : CharSequence, start : Int, before : Int, count : Int) {
            }
        })

        updater = view.findViewById(R.id.srl_contacts)
        updater.setOnRefreshListener(this)
        val recycler = view.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.rv_contacts)
        adapter = ContactRVAdapter(presenter)
        adapter.setListener(this)
        recycler.adapter = adapter

        presenter.loadContacts("", context!!)

        return view
    }

    override fun onStart() {
        super.onStart()
        setTitle(R.string.navigation_menu_contacts)

        GlideApp.with(this)
            .load(ApiService.MOCK_AVATAR)
            .transform(CircleCrop())
            .into(ivAvatar)

        ivAvatar.isActivated = true

        val user = (activity as MainActivity).user
        if(user != null) {
            tvPhone.text = Utils.formatTelephoneNumber(user.sipLogin)
            tvBalance.text = Utils.getString(R.string.activity_main_balance, Utils.formatRemainingMinutes(user.remainingTime, user.remainingTime))
        }
    }

    override fun onContactsLoaded() {
        val differ = DiffUtil.calculateDiff(diffUtil)
        differ.dispatchUpdatesTo(adapter)
        presenter.updateOldContacts()
        updater.isRefreshing = false
    }

    override fun onRefresh() {
        queryField.setText("")  // автоматом вызовется обновление контактов
    }

    override fun onClick(v: View?, position: Int) {
        val contact = presenter.contacts[position] as ContactModel
        if(contact.numbers.size == 1) {
            selectNumber(contact.numbers.get(0))
        } else if(contact.numbers.size > 1) {
            val numbersDialog = NumbersDialog.newInstance(contact.name, contact.numbers)
            numbersDialog.setListener(object : NumbersDialog.OnNumberSelectListener {
                override fun onClickNumber(number: String) {
                    selectNumber(number)
                }
            })
            numbersDialog.show(fragmentManager, NumbersDialog.TAG)
        }
    }

    fun selectNumber(number: String) {
        (activity as MainActivity).onOpenDialerScreen(number)
        (activity as MainActivity).bottomSelect(0)
    }

    companion object {
        const val TAG = "ContactFragment"

        @JvmStatic
        fun newInstance() : ContactFragment {
            val fragment = ContactFragment()

            return fragment
        }
    }
}