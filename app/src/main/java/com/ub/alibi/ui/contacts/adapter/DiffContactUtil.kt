package com.ub.alibi.ui.contacts.adapter

import androidx.recyclerview.widget.DiffUtil
import com.ub.alibi.ui.contacts.models.ContactType

class DiffContactUtil(private val contacts : List<ContactType>, private val oldContacts : List<ContactType>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return contacts[newItemPosition].hashCode() == oldContacts[oldItemPosition].hashCode()
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return contacts[newItemPosition] == oldContacts[oldItemPosition]
    }

    override fun getOldListSize(): Int = oldContacts.size
    override fun getNewListSize(): Int = contacts.size
}