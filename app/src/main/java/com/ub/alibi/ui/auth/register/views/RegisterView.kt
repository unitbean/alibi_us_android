package com.ub.alibi.ui.auth.register.views

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.ub.alibi.ui.base.views.BaseView

@StateStrategyType(OneExecutionStateStrategy::class)
interface RegisterView : BaseView {

    fun onShowEnterCodeDialog(phone: String)
}
