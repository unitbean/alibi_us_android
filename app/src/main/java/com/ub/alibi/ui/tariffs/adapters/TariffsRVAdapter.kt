package com.ub.alibi.ui.tariffs.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.ub.alibi.R
import com.ub.alibi.ui.base.adapters.BaseAdapter
import com.ub.alibi.ui.tariffs.models.TariffModel
import com.ub.alibi.ui.tariffs.presenter.TariffPresenter

/**
 * Created by pozharov on 07.03.2018.
 */

class TariffsRVAdapter(private val presenter: TariffPresenter) : BaseAdapter<TariffsRVAdapter.TariffViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TariffsRVAdapter.TariffViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rv_tariff_item, parent, false)
        return TariffViewHolder(view)
    }

    override fun onBindViewHolder(baseHolder: TariffsRVAdapter.TariffViewHolder, position: Int) {
        baseHolder.bind(presenter.tariffModels[position])
    }

    override fun getItemCount(): Int = presenter.tariffModels.size

    inner class TariffViewHolder internal constructor(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        private val tvPrice: TextView = itemView.findViewById(R.id.tv_price)

        init {
            itemView.findViewById<View>(R.id.btn_buy).setOnClickListener(this)
        }

        fun bind(tariffModel: TariffModel) {
            tvTitle.text = tariffModel.title
            tvPrice.text = tariffModel.price
        }

        override fun onClick(v: View) {
            when (v.id) {
                R.id.btn_buy -> listener?.onClick(v, adapterPosition)
            }
        }
    }
}
