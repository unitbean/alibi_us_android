package com.ub.alibi.ui.tariffs.view

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.ub.alibi.ui.base.views.BaseView
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.ui.tariffs.models.TariffModel

/**
 * Created by pozharov on 07.03.2018.
 */

@StateStrategyType(OneExecutionStateStrategy::class)
interface TariffView : BaseView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onShowTariffs()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onShowUserInfo(userModel: UserModel)
}
