package com.ub.alibi.ui.tariffs.interactors

import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.ui.tariffs.models.TariffModel
import io.reactivex.Observable

interface ITariffInteractor {

    fun loadUserInfo(): Observable<UserModel>
    fun loadTariffs(): Observable<List<TariffModel>>
    fun startPurchaseTransaction(model: TariffModel?, token: String): Observable<UserModel>
}