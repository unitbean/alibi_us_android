package com.ub.alibi.ui.dialer.presenters

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.dialer.views.DialerView
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.utils.Utils
import com.ub.alibi.utils.webrtc.rtcutils.OnCallEventListener

/**
 * Created by pozharov on 02.03.2018.
 */

@InjectViewState
class DialerPresenter(val onCallEventListener: OnCallEventListener) : BasePresenter<DialerView>() {

    private var dialerInput = ""
    var user: UserModel? = null

    init {
        coreServices.webRtcService
                .addCallEventListener(onCallEventListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        coreServices.webRtcService
                .removeCallEventListener(onCallEventListener)
    }

    override fun attachView(view: DialerView) {
        super.attachView(view)

        val subscription = coreServices
            .userService
            .userObservable
            .subscribe({ userModel ->
                this.user = userModel
                if (this.user?.currentMelody != null) {
                    viewState.onSetAlibiMelody(this.user?.currentMelody?.pictureUrl ?: "User")
                } else {
                    viewState.onSetAlibiMelody(null)
                }
            }) { throwable -> viewState.onShowError(throwable.message) }
        subscriptions.add(subscription)
    }

    fun onDialerDeleteClick() {
        if (dialerInput.isEmpty()) {
            return
        }
        dialerInput = dialerInput.substring(0, dialerInput.length - 1)
        viewState.onSetDialerInput(Utils.maskNumber(dialerInput))
    }

    fun onDialerClick(symbol: String) {
        dialerInput += symbol
        viewState.onSetDialerInput(Utils.maskNumber(dialerInput))
    }

    fun onDialerDeleteLongClick() {
        dialerInput = ""
        viewState.onSetDialerInput(Utils.maskNumber(dialerInput))
    }

    fun setPhoneNumber(phoneNumber: String) {
        val regularNumber = Regex("[^0-9]")
        dialerInput = regularNumber.replace(phoneNumber, "")
        if(dialerInput[0] != '1') {
            dialerInput = "+1"+dialerInput
        }
        viewState.onSetDialerInput(Utils.maskNumber(dialerInput))
    }

    /**
     * Форматирует номер и совершает звонок
     */
    fun startCall() {
        if (dialerInput.isNotEmpty()) {
            viewState.onStartCall(dialerInput)
        }
    }

    fun invite(dialerInput: String) {
        coreServices.webRtcService.startCall(dialerInput)
    }

    companion object {

        private const val TAG = "DialerPresenter"
    }
}
