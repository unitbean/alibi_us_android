package com.ub.alibi.ui.auth.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.ub.alibi.R
import com.ub.alibi.ui.auth.auth.fragments.BaseAuthFragment

class EnterPasswordFragment : BaseAuthFragment() {

    private lateinit var listener: PasswordListener
    private lateinit var password: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_password_enter, container, false)

        val logout = view.findViewById<Button>(R.id.btn_logout)
        logout.setOnClickListener { hostActivity.onPhoneInputEnable(true) }

        val forget = view.findViewById<Button>(R.id.btn_forget_password)
        forget.setOnClickListener { listener.onForgotPassword() }

        val password = view.findViewById<EditText>(R.id.et_password)

        val check = view.findViewById<Button>(R.id.btn_check_password)
        check.setOnClickListener {
            if (password.text.toString() == this.password) {
                listener.onPasswordCorrect()
            } else {
                password.error = getString(R.string.activity_auth_invalid_password)
            }
        }

        return view
    }

    interface PasswordListener {
        fun onPasswordCorrect()
        fun onForgotPassword()
    }

    companion object {

        const val TAG = "EnterPasswordFragment"

        @JvmStatic
        fun newInstance(password : String, listener : PasswordListener) : EnterPasswordFragment {
            val fragment = EnterPasswordFragment()
            fragment.listener = listener
            fragment.password = password

            return fragment
        }
    }
}