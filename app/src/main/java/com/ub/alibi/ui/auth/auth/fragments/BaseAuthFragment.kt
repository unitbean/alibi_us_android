package com.ub.alibi.ui.auth.auth.fragments

import com.ub.alibi.ui.auth.auth.activities.AuthActivity
import com.ub.alibi.ui.base.fragments.BaseFragment

abstract class BaseAuthFragment : BaseFragment() {

    override fun getHostActivity() : AuthActivity {
        return activity as AuthActivity
    }
}