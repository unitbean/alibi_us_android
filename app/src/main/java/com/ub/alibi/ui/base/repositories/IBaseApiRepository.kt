package com.ub.alibi.ui.base.repositories

import io.reactivex.Observable

interface IBaseApiRepository {
    fun retryAfterRefreshToken(errors: Observable<out Throwable>): Observable<*>
}