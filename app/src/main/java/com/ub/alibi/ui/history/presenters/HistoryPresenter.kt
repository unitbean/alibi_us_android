package com.ub.alibi.ui.history.presenters

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.di.services.database.models.CallModel
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.history.views.HistoryView
import com.ub.alibi.utils.LogUtils
import com.ub.alibi.utils.webrtc.rtcutils.OnCallEventListener

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

/**
 * Created by pozharov on 07.03.2018.
 */

@InjectViewState
class HistoryPresenter : BasePresenter<HistoryView>() {

    private var isCallingNowID: Int? = null

    private val emitter = PublishSubject.create<String>()
    private val callsSource = PublishSubject.create<List<CallModel>>()
    private val subscription = coreServices.database.allCalls
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    val observableCalls: Observable<List<CallModel>>
        get() = callsSource

    init {
        val subscription = this.emitter
            .flatMap { this.subscription }
            .map { list ->
                list.forEach {
                    if (it.id == isCallingNowID) {
                        it.isCallingNow = true
                    } else {
                        it.isCallingNow = false
                    }
                }
                return@map list
            }
            .subscribe({ calls -> callsSource.onNext(calls) }
            ) { throwable -> callsSource.onError(throwable) }
        subscriptions.add(subscription)
    }

    override fun attachView(view: HistoryView) {
        super.attachView(view)

        updateCalls()
    }

    fun deleteCall(position: Int) {
        val subscription = coreServices.database.deleteCall(position)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ updateCalls() }
            ) { throwable -> LogUtils.e(TAG, throwable.message, throwable) }
        subscriptions.add(subscription)
    }

    fun setCallingNowModel(id: Int) {
        isCallingNowID = id
        updateCalls()
    }

    fun removeCallingNowModel() {
        isCallingNowID = null
        updateCalls()
    }

    private fun updateCalls() {
        emitter.onNext("")
    }

    companion object {

        private const val TAG = "HistoryPresenter"
    }
}
