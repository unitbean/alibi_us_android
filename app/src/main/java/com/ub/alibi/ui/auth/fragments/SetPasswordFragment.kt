package com.ub.alibi.ui.auth.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.ub.alibi.R
import com.ub.alibi.ui.auth.auth.fragments.BaseAuthFragment

class SetPasswordFragment : BaseAuthFragment() {

    private lateinit var listener: PasswordListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_password_set, container, false)

        val logout = view.findViewById<Button>(R.id.btn_logout)
        logout.setOnClickListener { hostActivity.onPhoneInputEnable(true) }

        val password = view.findViewById<EditText>(R.id.et_password)

        val set = view.findViewById<Button>(R.id.btn_set_password)
        set.setOnClickListener {
            if (password.text.toString().length > 3) {
                listener.onPasswordEntered(password.text.toString())
            } else {
                password.error = resources.getString(R.string.activity_auth_set_password_error_lenght)
            }
        }

        return view
    }

    interface PasswordListener {
        fun onPasswordEntered(password: String)
    }

    companion object {

        const val TAG = "SetPasswordFragment"

        @JvmStatic
        fun newInstance(listener : PasswordListener) : SetPasswordFragment {
            val fragment = SetPasswordFragment()
            fragment.listener = listener

            return fragment
        }
    }
}