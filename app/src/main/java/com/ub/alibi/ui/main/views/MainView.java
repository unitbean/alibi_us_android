package com.ub.alibi.ui.main.views;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.ub.alibi.ui.base.views.BaseView;
import com.ub.alibi.ui.main.models.UserModel;

/**
 * Created by pozharov on 02.03.2018.
 */

@StateStrategyType(OneExecutionStateStrategy.class)
public interface MainView extends BaseView {
    void onShowUserInfo(UserModel userModel);
    @StateStrategyType(AddToEndSingleStrategy.class)
    void onShowChangingAlibiModeProgressBar(boolean isShow);

    void onOpenDialerScreen(String phoneNumber);

    void onOpenCallsHistoryScreen();

    void onOpenMelodiesScreen();

    void onOpenSettingScreen();

    void onShowFirstHelpDialog();
    void onShowSecondHelpDialog();
    void onShowThirdHelpDialog();
}
