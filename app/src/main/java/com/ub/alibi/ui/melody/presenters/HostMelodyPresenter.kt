package com.ub.alibi.ui.melody.presenters

import android.media.AudioManager
import android.media.MediaPlayer

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.di.services.api.ApiException
import com.ub.alibi.di.services.api.models.requests.RemoveMelodiesRequest
import com.ub.alibi.di.services.api.models.requests.SaveMelodiesRequest
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.ui.melody.models.MelodyModel
import com.ub.alibi.ui.melody.views.MelodiesHostView
import com.ub.alibi.utils.LogUtils

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

import java.io.IOException
import java.util.ArrayList
import java.util.HashSet

const val TAG = "HostMelodyPresenter"

@InjectViewState
class HostMelodyPresenter : BasePresenter<MelodiesHostView>(), MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    val melodyModels = ArrayList<MelodyModel>()
    val favoriteMelodies = ArrayList<MelodyModel>()
    private val originMelodyModels = ArrayList<MelodyModel>()
    private val originFavoritesModels = ArrayList<MelodyModel>()
    private var userModel: UserModel? = null
    private var currentMelody: MelodyModel? = null
    private var playingMelody: MelodyModel? = null
    private var player: MediaPlayer? = null

    override fun onFirstViewAttach() {
        loadMelodies()
    }

    init {
        val subscription = coreServices
            .userService
            .userObservable
            .subscribe({ userModel -> this.userModel = userModel }) { throwable ->
                LogUtils.e(TAG, throwable.message ?: "Undefined error", throwable)
                viewState.onShowError(throwable.message)
            }
        subscriptions.add(subscription)
    }

    /**
     * Загрузка основного списка мелодий
     */
    private fun loadMelodies() {
        val subscription = coreServices.apiService.api.melodies
            .flatMapIterable { it.result }
            .map { melodyResponse ->
                val melodyModel = MelodyModel(melodyResponse)
                if (melodyModel.isCurrent) {
                    currentMelody = melodyModel
                }
                melodyModel
            }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _ -> viewState.onShowProgressBar(true) }
            .doFinally { viewState.onShowProgressBar(false) }
            .subscribe({ melodyModels ->
                this.melodyModels.clear()
                this.melodyModels.addAll(melodyModels)

                this.originMelodyModels.clear()
                this.originMelodyModels.addAll(melodyModels)

                loadFavMelodies()

            }) { throwable ->
                LogUtils.e(TAG, throwable.message ?: "Undefined error", throwable)
                viewState.onShowError(throwable.message)
            }
        subscriptions.add(subscription)
    }

    /**
     * Загрузка списка избранных мелодий
     */
    private fun loadFavMelodies() {
        val subscription = coreServices.apiService.api.favMelodies
            .flatMapIterable { it.result }
            .map { melodyResponse ->
                val melodyModel = MelodyModel(melodyResponse)
                if (melodyModel.isCurrent) {
                    currentMelody = melodyModel
                }
                melodyModel
            }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _ -> viewState.onShowProgressBar(true) }
            .doFinally { viewState.onShowProgressBar(false) }
            .subscribe({ melodyModels ->
                this.favoriteMelodies.clear()
                this.favoriteMelodies.addAll(melodyModels)

                this.originFavoritesModels.clear()
                this.originFavoritesModels.addAll(melodyModels)

                viewState.onUpdateMelodies()
            }) { throwable ->
                LogUtils.e(TAG, throwable?.message ?: "Undefined error", throwable)
                viewState.onShowError(throwable.message)
            }
        subscriptions.add(subscription)
    }

    /**
     * Опрерация по добавлению или удалению из избранного по клику на соотв. кнопку
     * @param isFromFav - из избранного вызов или из общего списка
     * @param position - позиция для действия
     */
    fun onFavoriteClick(isFromFav: Boolean, position: Int) {
        val subscription = Observable.just(HashSet<String>())
            .map { set ->
                if (isFromFav) {
                    favoriteMelodies                                                                    // из избранного получаем айдишник выбранной мелодии
                        .filter { it.id == favoriteMelodies[position].id }
                        .map { set.add(it.id) }
                } else if (melodyModels[position].isFavorite) {
                    set.add(melodyModels[position].id)                                                  // из списка всех мелодий тыкаем на "избранную", то она станет обычной
                } else {
                    set.addAll(favoriteMelodies.map { it.id })                                          // иначе - добавляем в лист избранные уже избранные
                    set.add(melodyModels[position].id)                                                  // и добавляем туда новую, избранную пользователем
                }

                set
            }
            .flatMap { request ->
                return@flatMap if (isFromFav) {
                    coreServices.apiService.api.removeFavMelodies(RemoveMelodiesRequest(request))       // из избранного можно только удалить
                } else {
                    if (melodyModels[position].isFavorite) {
                        coreServices.apiService.api.removeFavMelodies(RemoveMelodiesRequest(request))   // а из общего списка как удалить
                    } else {
                        coreServices.apiService.api.saveMelodies(SaveMelodiesRequest(request))          // так и добавить в "избранное"
                    }
                }
            }
            .flatMapIterable { it.result }
            .map { MelodyModel(it) }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _ -> viewState.onShowProgressBar(true) }
            .doFinally { viewState.onShowProgressBar(false) }
            .subscribe( Consumer { loadMelodies() },
                object : ApiException(TAG) {
                    override fun call(message: String) {
                        viewState.onShowError(message)
                    }

                    override fun unauthorized() {

                    }
                })
        subscriptions.add(subscription)
    }

    /**
     * Сохранение выбранной мелодии пользователя
     * @param isFromFav - вызов совершен из "избранного" или из общего списка мелодий
     * @param position - позиция нажатия в списке
     */
    fun onMelodyClick(isFromFav: Boolean, position: Int, isOpenDialerFragment: Boolean) {
        val subscription = Observable.just(isFromFav)
            .map {
                favoriteMelodies
                    .map { it.isCurrent = false }

                melodyModels
                    .map { it.isCurrent = false }

                return@map it
            }
            .flatMap {
                return@flatMap if (it) {
                    coreServices.apiService.api.setCurrentMelody(favoriteMelodies[position].id)
                } else {
                    coreServices.apiService.api.setCurrentMelody(melodyModels[position].id)
                }
            }
            .map { it.result }
            .map { MelodyModel(it) }
            .doOnSubscribe { _ -> viewState.onShowProgressBar(true) }
            .doFinally { viewState.onShowProgressBar(false) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ melodyModel ->
                coreServices.userService
                        .updateMelody(melodyModel)

                currentMelody?.isCurrent = false

                currentMelody = if (isFromFav) {
                    favoriteMelodies[position]
                } else {
                    melodyModels[position]
                }

                favoriteMelodies
                    .filter { it.id == currentMelody?.id }
                    .map { it.isCurrent = true }

                melodyModels
                    .filter { it.id == currentMelody?.id }
                    .map { it.isCurrent = true }

                viewState.onUpdateMelodies()

                if (isOpenDialerFragment) {
                    viewState.onOpenDialerFragment()
                }

                viewState.onShowHelpDialogAfterMelodySelecting()
            }) { throwable ->
                LogUtils.e(TAG, throwable.message ?: "Undefined error", throwable)
                viewState.onShowError(throwable.message)
            }
        subscriptions.add(subscription)
    }

    /**
     * Проигрывание выбранной мелодии
     * @param isFromFav - из избранного вызов или из основного списка
     * @param position - позиция в списке
     */
    fun onPlayClick(isFromFav: Boolean, position: Int) {
        val sound = if (isFromFav) {
            favoriteMelodies[position]
        } else {
            melodyModels[position]
        }

        if (playingMelody?.id != sound.id) {
            releasePlayer()
            playingMelody = sound
        } else {
            releasePlayer()
            viewState.onUpdateMelodies()
            return
        }

        try {
            playingMelody?.isPlaying = true
            player = MediaPlayer()
            player?.setAudioStreamType(AudioManager.STREAM_MUSIC)
            player?.setOnPreparedListener(this)
            player?.setOnCompletionListener(this)
            player?.setDataSource(playingMelody?.fileUrl)
            player?.prepareAsync()
        } catch (e: IOException) {
            LogUtils.e(TAG, e.message ?: "Undefined error", e)
        }

        viewState.onUpdateMelodies()
    }

    /**
     * Остановить проигрывание мелодии
     */
    private fun releasePlayer() {
        if (player != null) {
            player?.stop()
            player?.release()
            player = null
            playingMelody?.isPlaying = false
            playingMelody = null
        }
    }

    /**
     * Поиск по переданному запросу в избранном или основном листах
     * @param isFromFav - поиск по избранному или нет
     * @param query - строка запроса для поиска
     */
    fun searchFromList(isFromFav: Boolean, query: String) {
        if (query.isNotEmpty()) {
            if (isFromFav) {
                favoriteMelodies.clear()

                originFavoritesModels.filter { it.title.contains(query, true) }
                    .map { favoriteMelodies.add(it) }
            } else {
                melodyModels.clear()

                originMelodyModels.filter { it.title.contains(query, true) }
                    .map { melodyModels.add(it) }
            }
        } else {
            melodyModels.clear()
            melodyModels.addAll(originMelodyModels)

            favoriteMelodies.clear()
            favoriteMelodies.addAll(originFavoritesModels)
        }

        viewState.onUpdateMelodies()
    }

    override fun onPrepared(mp: MediaPlayer) {
        mp.start()
    }

    override fun onCompletion(mp: MediaPlayer) {
        releasePlayer()
        viewState.onUpdateMelodies()
    }

    override fun detachView(view: MelodiesHostView?) {
        releasePlayer()
        super.detachView(view)
    }
}