package com.ub.alibi.ui.history.views

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.ub.alibi.ui.base.views.BaseView

/**
 * Created by pozharov on 07.03.2018.
 */

@StateStrategyType(OneExecutionStateStrategy::class)
interface HistoryView : BaseView
