package com.ub.alibi.ui.tariffs.presenter

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.ui.base.presenters.CleanPresenter
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.ui.tariffs.interactors.TariffInteractor
import com.ub.alibi.ui.tariffs.models.TariffModel
import com.ub.alibi.ui.tariffs.reporitories.TariffRepository
import com.ub.alibi.ui.tariffs.view.TariffView
import com.ub.alibi.utils.LogUtils

import java.util.ArrayList

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by pozharov on 07.03.2018.
 */

@InjectViewState
class TariffPresenter : CleanPresenter<TariffView>() {
    val tariffModels = ArrayList<TariffModel>()
    private var userModel: UserModel? = null
    private var tariffProcess: TariffModel? = null
    private val interactor = TariffInteractor(TariffRepository())

    override fun onFirstViewAttach() {
        loadTariffs()
        loadUserInfo()
    }

    private fun loadUserInfo() {
        val subscription = interactor.loadUserInfo()
            .doOnSubscribe { viewState.onShowProgressBar(true) }
            .doFinally { viewState.onShowProgressBar(false) }
            .subscribe({ userModel ->
                this.userModel = userModel
                viewState.onShowUserInfo(userModel)
            }) { throwable ->
                LogUtils.e(TAG, throwable.message, throwable)
                viewState.onShowError(throwable.message)
            }

        subscriptions.add(subscription)
    }

    private fun loadTariffs() {
        val subscription = interactor.loadTariffs()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { viewState.onShowProgressBar(true) }
            .doFinally { viewState.onShowProgressBar(false) }
            .subscribe({ tariffModels ->
                this.tariffModels.clear()
                this.tariffModels.addAll(tariffModels)
                viewState.onShowTariffs()
            }) { throwable ->
                LogUtils.e(TAG, throwable.message, throwable)
                viewState.onShowError(throwable.message)
            }

        subscriptions.add(subscription)
    }

    fun startPurchaseTransaction(token: String) {
        val subscription = interactor.startPurchaseTransaction(tariffProcess, token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ user -> viewState.onShowUserInfo(user) }
            ) { throwable ->
                LogUtils.e(TAG, throwable.message, throwable)
                viewState.onShowError(throwable.message)
            }

        subscriptions.add(subscription)
    }

    fun setTransactionProcessPosition(position: Int) {
        tariffProcess = tariffModels[position]
    }

    companion object {

        private const val TAG = "TariffPresenter"
    }
}
