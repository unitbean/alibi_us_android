package com.ub.alibi.ui.tariffs.activities

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.ConsumeResponseListener
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchasesUpdatedListener
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.ub.alibi.R
import com.ub.alibi.ui.base.activities.BaseActivity
import com.ub.alibi.ui.base.adapters.BaseAdapter
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.ui.tariffs.adapters.TariffsRVAdapter
import com.ub.alibi.ui.tariffs.presenter.TariffPresenter
import com.ub.alibi.ui.tariffs.view.TariffView
import com.ub.alibi.utils.GlideApp
import com.ub.alibi.utils.Utils
import com.ub.alibi.utils.commons.VerticalSpaceItemDecoration

import com.android.billingclient.api.BillingClient.BillingResponse.*
import com.crashlytics.android.Crashlytics
import com.flurry.android.FlurryAgent
import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.utils.PreferenceKeys
import com.ub.alibi.utils.dpToPx

/**
 * Created by pozharov on 07.03.2018.
 */

class TariffActivity : BaseActivity(), TariffView, PurchasesUpdatedListener, ConsumeResponseListener,
        BillingClientStateListener, BaseAdapter.OnItemClickListener {

    @InjectPresenter lateinit var presenter: TariffPresenter

    private lateinit var adapter: TariffsRVAdapter

    private lateinit var ivAvatar: ImageView
    private lateinit var tvBalance: TextView
    private var billingClient: BillingClient? = null

    private var isPurchaseAvailable = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tariffs)

        FlurryAgent.logEvent(PreferenceKeys.RATE_DID_TOUCH)

        ivAvatar = findViewById(R.id.iv_tariff_avatar)
        tvBalance = findViewById(R.id.tv_tariff_balance)

        billingClient = BillingClient.newBuilder(this)
            .setListener(this)
            .build()

        val rvTariffs = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.rv_tariffs)
        rvTariffs.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        adapter = TariffsRVAdapter(presenter)
        adapter.setListener(this)
        rvTariffs.addItemDecoration(VerticalSpaceItemDecoration(rvTariffs.dpToPx(5).toInt()))
        rvTariffs.adapter = adapter
        billingClient?.startConnection(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (billingClient?.isReady == true) {
            billingClient?.endConnection()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        showBackArrowButton()
    }

    override fun onShowTariffs() {
        adapter.notifyDataSetChanged()
    }

    override fun onShowUserInfo(userModel: UserModel) {
        GlideApp.with(this)
            .load(ApiService.MOCK_AVATAR)
            .transform(CircleCrop())
            .into(ivAvatar)
        tvBalance.text = Utils.getString(R.string.activity_main_balance, Utils.formatRemainingMinutes(userModel.remainingTime, userModel.remainingTime))
    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: List<Purchase>?) {
        if (purchases != null && !purchases.isEmpty()) {
            billingClient?.consumeAsync(purchases[0].purchaseToken, this)
        }
    }

    override fun onConsumeResponse(@BillingClient.BillingResponse responseCode: Int, purchaseToken: String) {
        if (responseCode == OK) {
            presenter.startPurchaseTransaction(purchaseToken)
        }
    }

    override fun onBillingSetupFinished(responseCode: Int) {
        isPurchaseAvailable = responseCode == 0
    }

    override fun onBillingServiceDisconnected() {
        isPurchaseAvailable = false
    }

    override fun onClick(v: View, position: Int) {
        if (isPurchaseAvailable) {
            val billingParams = BillingFlowParams
                .newBuilder()
                .setSku(presenter.tariffModels[position].billingId)
                .setType(BillingClient.SkuType.INAPP)
                .build()

            val responseCode = billingClient?.launchBillingFlow(this, billingParams)
            if (0 == responseCode) {
                presenter.setTransactionProcessPosition(position)
            } else if (-1 == responseCode) {

                //TODO: Убрать
                onShowError(Utils.getString(R.string.error_initiating_payment, responseCode.toString()))
                Crashlytics.log("Billing client returns bad response code (responseCode: $responseCode)")

                billingClient?.endConnection()
                billingClient?.startConnection(this)
            } else {
                //TODO: Убрать
                onShowError(Utils.getString(R.string.error_initiating_payment, responseCode.toString()))
                Crashlytics.log("Billing client returns bad response code (responseCode: $responseCode)")
            }
        } else {
            super.onShowError(R.string.activity_tariffs_purchase_not_available)
        }
    }
}