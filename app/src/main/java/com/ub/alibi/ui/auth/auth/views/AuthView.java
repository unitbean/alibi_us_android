package com.ub.alibi.ui.auth.auth.views;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.ub.alibi.ui.base.views.BaseView;

/**
 * Created by pozharov on 01.03.2018.
 */

@StateStrategyType(OneExecutionStateStrategy.class)
public interface AuthView extends BaseView {
    @StateStrategyType(AddToEndSingleStrategy.class)
    void onPhoneInputEnable(boolean isEnable);
    void onShowSetPasswordDialog();
    void onShowEnterPasswordDialog(String password);
    void onAuthSuccess();
    void onTokenExpired();
    void timerUpdate(Long time);
}
