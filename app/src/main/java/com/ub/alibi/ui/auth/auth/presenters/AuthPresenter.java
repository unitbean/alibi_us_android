package com.ub.alibi.ui.auth.auth.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.ub.alibi.di.services.api.ApiException;
import com.ub.alibi.di.services.api.models.requests.LoginRequest;
import com.ub.alibi.di.services.api.models.requests.VerifyCodeRequest;
import com.ub.alibi.di.services.api.models.responses.BaseResponse;
import com.ub.alibi.di.services.api.models.responses.TokenResponse;
import com.ub.alibi.di.services.api.models.responses.UpdateTokenResponse;
import com.ub.alibi.ui.auth.auth.views.AuthView;
import com.ub.alibi.ui.base.presenters.BasePresenter;
import com.ub.alibi.utils.LogUtils;
import com.ub.alibi.utils.PreferenceKeys;
import com.ub.alibi.utils.PreferenceUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pozharov on 01.03.2018.
 */

@InjectViewState
public class AuthPresenter extends BasePresenter<AuthView> {

    private static final String TAG = AuthPresenter.class.getSimpleName();

    private String phone;
    private Disposable timer;

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        Disposable signIn = Observable.just("")
            .delay(600, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(empty -> checkSignIn(),
                throwable -> LogUtils.e(TAG, throwable.getMessage(), throwable));
        subscriptions.add(signIn);
    }

    private void checkSignIn() {
        String accessToken = PreferenceUtils.getPref().getString(PreferenceKeys.KEY_SETTINGS_USER_ACCESS_TOKEN, "");
        if (accessToken.isEmpty()) {
            getViewState().onPhoneInputEnable(true);
        } else {
            getViewState().onPhoneInputEnable(false);

            boolean isPasswordEnable = PreferenceUtils.getPref().getBoolean(PreferenceKeys.KEY_IS_PASSWORD_ENABLE, true);
            Disposable disposable = updateToken()
                .subscribe(result -> {
                        if(isPasswordEnable) {
                            String password = PreferenceUtils.getPref().getString(PreferenceKeys.KEY_USER_PASSWORD, "");
                            getViewState().onShowEnterPasswordDialog(password);
                        } else {
                            getViewState().onAuthSuccess();
                        }
                    },
                    new ApiException(TAG) {
                        @Override
                        public void call(String message) {
                            if(isPasswordEnable) {
                                String password = PreferenceUtils.getPref().getString(PreferenceKeys.KEY_USER_PASSWORD, "");
                                getViewState().onShowEnterPasswordDialog(password);
                            } else {
                                getViewState().onShowError(message);
                            }
                        }

                        @Override
                        public void unauthorized() {
                            coreServices.getUserService().cleanUser();
                            getViewState().onTokenExpired();
                        }
                    });
            subscriptions.add(disposable);
        }
    }

    public void verifyCode(String code) {
        Disposable disposable = coreServices
            .getApiService()
            .getApi()
            .verifyCode(new VerifyCodeRequest(phone, code))
            .retryWhen(this::retryAfterRefreshToken)
            .flatMap(verifyCodeResponse -> {
                TokenResponse refreshToken = verifyCodeResponse.getResult().getRefreshToken();
                PreferenceUtils.edit()
                    .putString(PreferenceKeys.KEY_SETTINGS_USER_REFRESH_TOKEN, refreshToken.getToken())
                    .putString(PreferenceKeys.KEY_SETTINGS_USER_REFRESH_TOKEN_EXPIRED_IN, refreshToken.getExpiredAt())
                    .putString(PreferenceKeys.KEY_SETTINGS_USER_REFRESH_TOKEN_TIMEZONE, refreshToken.getTimeZone())
                    .apply();
                return coreServices
                    .getApiService()
                    .getApi()
                    .login(new LoginRequest(refreshToken.getToken()));
            }, (verifyCodeResponse, loginResponse) -> {
                TokenResponse accessToken = loginResponse.getResult().getAccessToken();
                PreferenceUtils.edit()
                    .putString(PreferenceKeys.KEY_SETTINGS_USER_ACCESS_TOKEN, accessToken.getToken())
                    .putString(PreferenceKeys.KEY_SETTINGS_USER_ACCESS_TOKEN_EXPIRED_IN, accessToken.getExpiredAt())
                    .putString(PreferenceKeys.KEY_SETTINGS_USER_ACCESS_TOKEN_TIMEZONE, accessToken.getTimeZone())
                    .apply();
                return Observable.empty();
            })
            .flatMap(result -> updateToken())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(emptyObs -> getViewState().onShowSetPasswordDialog(),
                new ApiException(TAG) {
                    @Override
                    public void call(String message) {
                        getViewState().onShowError(message);
                    }

                    @Override
                    public void unauthorized() {
                        coreServices.getUserService().cleanUser();
                        getViewState().onTokenExpired();
                    }
                });
        subscriptions.add(disposable);
    }

    private Observable<BaseResponse<UpdateTokenResponse>> updateToken() {
        return coreServices.getApiService().getApi().initToken()
            .retryWhen(this::retryAfterRefreshToken)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    }

    public void setPassword(String password){
        PreferenceUtils.edit()
            .putString(PreferenceKeys.KEY_USER_PASSWORD, password)
            .apply();
        getViewState().onAuthSuccess();
    }

    public void startTimerCountdown() {
        stopTimer();

        timer = Observable.interval(0 , 1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(time -> {
                getViewState().timerUpdate(time);
            },
                throwable -> LogUtils.e(TAG, throwable.getMessage(), throwable));
        subscriptions.add(timer);
    }

    public void stopTimer() {
        if (timer != null && !timer.isDisposed()) {
            timer.dispose();
        }
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void onForgotPasswordClick() {
        getViewState().onPhoneInputEnable(true);
    }
}
