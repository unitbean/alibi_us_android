package com.ub.alibi.ui.history.presenters

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.history.views.CallView
import com.ub.alibi.utils.webrtc.rtcutils.OnCallEventListener

/**
 * Created by VOLixanov on 23.03.2018.
 */

@InjectViewState
class AllCallPresenter(val onCallEventListener: OnCallEventListener) : BasePresenter<CallView>() {

    init {
        coreServices.webRtcService
                .addCallEventListener(onCallEventListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        coreServices.webRtcService
                .removeCallEventListener(onCallEventListener)
    }

    fun startCall(dialerNumber: String) {
        coreServices.webRtcService.startCall(dialerNumber)
    }

    companion object {

        private const val TAG = "AllCallPresenter"
    }
}
