package com.ub.alibi.ui.contacts.presenters

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.contacts.models.ContactType
import com.ub.alibi.ui.contacts.models.HeaderModel
import com.ub.alibi.ui.contacts.views.ContactView
import com.ub.alibi.utils.LogUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList
import android.content.Context
import android.provider.ContactsContract
import com.ub.alibi.ui.contacts.models.ContactModel

private const val TAG = "ContactPresenter"

@InjectViewState
class ContactPresenter : BasePresenter<ContactView>() {

    val oldContacts = ArrayList<ContactType>()
    val contacts = ArrayList<ContactType>()

    /**
     * Получение списка контактов телефона
     * Сортировка их по алфавиту
     * Добавление к ним
     * @param query - введенный текст для поиска по имени
     * @param context - контекст для получения доступа к курсору
     */
    fun loadContacts(query: String, context: Context) {
        val subscription = Observable.just(context.contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"))
            .map {
                val list = ArrayList<ContactModel>()

                while (it.moveToNext()) {
                    val name = it.getString(it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                    val phoneNumber = it.getString(it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

                    if (query.isNotEmpty() && !name.contains(query, true)) {
                        continue
                    }

                    val existingContactOrNull = list.firstOrNull{it.name == name}

                    if(existingContactOrNull != null && existingContactOrNull.isNumberNotAdded(phoneNumber)) {
                        existingContactOrNull.numbers.add(phoneNumber)
                    } else if(existingContactOrNull == null) {
                        list.add(ContactModel(name, mutableListOf(phoneNumber)))
                    }
                }

                it.close()

                return@map list
            }
            .map {
                val list = ArrayList<ContactType>()

                var currentLetter = ""
                for (item in it) {
                    if (currentLetter != item.name.substring(0, 1)) {
                        currentLetter = item.name.substring(0, 1)
                        list.add(HeaderModel(currentLetter))
                    }

                    list.add(item)
                }

                return@map list
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( {
                contacts.clear()
                contacts.addAll(it)
                viewState.onContactsLoaded()
            },
                { LogUtils.e(TAG, it.message ?: "Undefined error", it) })
        subscriptions.add(subscription)
    }

    /**
     * Обновление старого листа новыми значениями после отображения последнего
     */
    fun updateOldContacts() {
        oldContacts.clear()
        oldContacts.addAll(contacts)
    }
}