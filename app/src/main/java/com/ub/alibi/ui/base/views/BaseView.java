package com.ub.alibi.ui.base.views;

import androidx.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by pozharov on 01.03.2018.
 */

public interface BaseView extends MvpView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onShowProgressBar(boolean isShow);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onShowError(String message);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onShowError(@StringRes int resId);
}
