package com.ub.alibi.ui.base.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.flurry.android.FlurryAgent;
import com.ub.alibi.R;
import com.ub.alibi.ui.base.activities.BaseActivity;
import com.ub.utils.base.MvpXFragment;

/**
 * Created by pozharov on 02.03.2018.
 */

public class BaseFragment extends MvpXFragment {

    BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof BaseActivity){
            mActivity = (BaseActivity)context;
        }
        FlurryAgent.logEvent(getClass().getSimpleName());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        if(mActivity != null && toolbar != null){
            mActivity.setSupportActionBar(toolbar);
        }
    }

    public void onShowProgressBar(boolean isShow) {
        if(mActivity != null) {
            mActivity.onShowProgressBar(isShow);
        }
    }

    public void onShowError(String message) {
        if(mActivity != null) {
            mActivity.onShowError(message);
        }
    }

    public void showBackArrowButton(){
        if(mActivity != null) {
            mActivity.showBackArrowButton();
        }
    }

    public void onShowError(@StringRes int resId) {
        if(mActivity != null) {
            mActivity.onShowError(resId);
        }
    }

    protected void setTitle(@StringRes int resId){
        if(mActivity != null){
            mActivity.setTitle(resId);
        }
    }

    protected void setSubtitle(String subtitle){
        if(mActivity != null){
            mActivity.setSubtitle(subtitle);
        }
    }

    protected BaseActivity getHostActivity() {
        return ((BaseActivity) getActivity());
    }
}
