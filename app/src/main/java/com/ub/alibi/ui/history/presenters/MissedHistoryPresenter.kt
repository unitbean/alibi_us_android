package com.ub.alibi.ui.history.presenters

import com.arellomobile.mvp.InjectViewState
import com.ub.alibi.ui.base.presenters.BasePresenter
import com.ub.alibi.ui.history.views.HistoryView

/**
 * Created by pozharov on 07.03.2018.
 */

@InjectViewState
class MissedHistoryPresenter() : BasePresenter<HistoryView>() {

    fun startCall(dialerNumber: String) {
        coreServices.webRtcService.startCall(dialerNumber)
    }

    companion object {

        private const val TAG = "MissedHistoryPresenter"
    }
}
