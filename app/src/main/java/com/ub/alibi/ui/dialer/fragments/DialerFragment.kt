package com.ub.alibi.ui.dialer.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.ub.alibi.R
import com.ub.alibi.di.services.api.ApiService
import com.ub.alibi.ui.base.fragments.BaseFragment
import com.ub.alibi.ui.dialer.presenters.DialerPresenter
import com.ub.alibi.ui.dialer.views.DialerView
import com.ub.alibi.ui.main.activities.MainActivity
import com.ub.alibi.ui.main.models.UserModel
import com.ub.alibi.utils.GlideApp
import com.ub.alibi.utils.Utils
import com.ub.alibi.utils.commons.OnBallanceUpdate
import com.ub.alibi.utils.webrtc.rtcutils.OnCallEventListener
import com.ub.utils.gone
import com.ub.utils.visible

/**
 * Created by pozharov on 02.03.2018.
 */

class DialerFragment : BaseFragment(), DialerView,
        View.OnClickListener,
        View.OnLongClickListener,
        OnCallEventListener,
        OnBallanceUpdate {

    @InjectPresenter lateinit var presenter: DialerPresenter

    private lateinit var tvInput: TextView
    private lateinit var btnCall: ImageButton
    private lateinit var btnSound: ImageButton
    private lateinit var ivAvatar: ImageView
    private lateinit var tvPhone: TextView
    private lateinit var tvBalance: TextView
    private lateinit var progressCall: ProgressBar

    @ProvidePresenter
    fun providePresenter(): DialerPresenter {
        return DialerPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_dialer, container, false)

        tvInput = view.findViewById(R.id.tv_input)
        ivAvatar = view.findViewById(R.id.iv_avatar)
        tvPhone = view.findViewById(R.id.tv_phone)
        tvBalance = view.findViewById(R.id.tv_balance)
        progressCall = view.findViewById(R.id.p_call)

        val ivDelete = view.findViewById<ImageView>(R.id.iv_delete)
        ivDelete.setOnClickListener(this)
        ivDelete.setOnLongClickListener(this)

        btnCall = view.findViewById(R.id.btn_call)
        btnCall.setOnClickListener(this)

        btnSound = view.findViewById(R.id.btn_sound)
        btnSound.setOnClickListener(this)

        view.findViewById<View>(R.id.tv_dialer_0).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_1).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_2).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_3).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_4).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_5).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_6).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_7).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_8).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_9).setOnClickListener(this)
        view.findViewById<View>(R.id.tv_dialer_0).setOnLongClickListener(this)

        return view
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.iv_delete -> presenter.onDialerDeleteClick()
            R.id.tv_dialer_0 -> presenter.onDialerClick("0")
            R.id.tv_dialer_1 -> presenter.onDialerClick("1")
            R.id.tv_dialer_2 -> presenter.onDialerClick("2")
            R.id.tv_dialer_3 -> presenter.onDialerClick("3")
            R.id.tv_dialer_4 -> presenter.onDialerClick("4")
            R.id.tv_dialer_5 -> presenter.onDialerClick("5")
            R.id.tv_dialer_6 -> presenter.onDialerClick("6")
            R.id.tv_dialer_7 -> presenter.onDialerClick("7")
            R.id.tv_dialer_8 -> presenter.onDialerClick("8")
            R.id.tv_dialer_9 -> presenter.onDialerClick("9")
            R.id.btn_sound -> (activity as MainActivity).openMelodyFragment()
            R.id.btn_call -> presenter.startCall()
        }
    }

    override fun onSetDialerInput(input: String) {
        tvInput.text = input
        btnCall.isActivated = input.isNotEmpty()
    }

    override fun onSetAlibiMelody(melody: String?) {
        btnSound.isActivated = melody != null

        val options = RequestOptions()
        options.centerCrop().transform(RoundedCorners(Utils.convertDpToPx(requireContext(), 1).toInt()))

        GlideApp.with(this)
            .load(melody)
            .placeholder(R.drawable.ic_sound_icon)
            .error(R.drawable.ic_sound_icon)
            //.apply(options)
            .transform(CircleCrop())
            .into(btnSound)

        GlideApp.with(this)
            .load(ApiService.MOCK_AVATAR)
            .transform(CircleCrop())
            .into(ivAvatar)

        ivAvatar.isActivated = true

        tvPhone.text = Utils.formatTelephoneNumber(presenter.user?.sipLogin ?: "")

        onBallanceUpdated(presenter.user ?: return)
    }

    override fun onStartCall(dialerInput: String) {
        presenter.invite(dialerInput)
        progressCall.visible
    }

    override fun onCallStarted() {
        progressCall.gone
    }

    override fun onCallCompleted(callTime: Long) {
        if (progressCall.visibility == View.VISIBLE) {
            progressCall.gone
        }
    }

    override fun onBallanceUpdated(user: UserModel) {
        presenter.user = user
        val remainingTime = user.remainingTime
        tvBalance.text = Utils.getString(R.string.activity_main_balance, Utils.formatRemainingMinutes(remainingTime, remainingTime))
    }

    override fun onLongClick(v: View): Boolean {
        when (v.id) {
            R.id.tv_dialer_0 -> {
                presenter.onDialerClick("+")
                return true
            }
            R.id.iv_delete -> {
                presenter.onDialerDeleteLongClick()
                return true
            }
        }
        return false
    }

    fun updateDialerInput(phoneNumber: String) {
        presenter.setPhoneNumber(phoneNumber)
    }

    companion object {

        const val TAG = "DialerFragment"

        @JvmStatic
        fun newInstance(): DialerFragment {
            return DialerFragment()
        }
    }
}
